-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: localhost    Database: f8db
-- ------------------------------------------------------
-- Server version	8.0.30

drop database if exists f8db;
create database f8db;

use f8db;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ask`
--

DROP TABLE IF EXISTS `ask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ask` (
  `AskID` int NOT NULL,
  `AskDetail` varchar(2000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `Dicuss_DicussID` int NOT NULL,
  PRIMARY KEY (`AskID`),
  KEY `fk_Ask_Dicuss1_idx` (`Dicuss_DicussID`),
  CONSTRAINT `fk_Ask_Dicuss1` FOREIGN KEY (`Dicuss_DicussID`) REFERENCES `dicuss` (`DicussID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ask`
--

LOCK TABLES `ask` WRITE;
/*!40000 ALTER TABLE `ask` DISABLE KEYS */;
/*!40000 ALTER TABLE `ask` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog`
--

DROP TABLE IF EXISTS `blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `blog` (
  `BlogID` int NOT NULL AUTO_INCREMENT,
  `BlogTitle` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `BlogImage` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `BlogDetail` longtext,
  `PostDate` datetime DEFAULT NULL,
  `User_UserID` int NOT NULL,
  `BlogStatus` int DEFAULT NULL,
  `TimeToRead` int DEFAULT NULL,
  `BlogTopic_BlogTopicID` int NOT NULL,
  `BlogTag_BlogTagID` int NOT NULL,
  PRIMARY KEY (`BlogID`),
  KEY `fk_Blog_User1_idx` (`User_UserID`),
  KEY `fk_Blog_BlogTopic1_idx` (`BlogTopic_BlogTopicID`),
  KEY `fk_Blog_BlogTag1_idx` (`BlogTag_BlogTagID`),
  CONSTRAINT `fk_Blog_BlogTag1` FOREIGN KEY (`BlogTag_BlogTagID`) REFERENCES `blogtag` (`BlogTagID`),
  CONSTRAINT `fk_Blog_BlogTopic1` FOREIGN KEY (`BlogTopic_BlogTopicID`) REFERENCES `blogtopic` (`BlogTopicID`),
  CONSTRAINT `fk_Blog_User1` FOREIGN KEY (`User_UserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog`
--

LOCK TABLES `blog` WRITE;
/*!40000 ALTER TABLE `blog` DISABLE KEYS */;
INSERT INTO `blog` VALUES (1,'CSS Selector','bloggingblog-concepts-ideas-white-worktable-260nw-1029506242.jpg','Coal is a valuable hard, black material extracted from mines. Wood that has been buried for a long time becomes coal due to a chemical change. Earthquakes cause vast forest areas to sink underground and contribute to such changes as a result of tremendous heat and pressure. Coal mines can be found in our country at  Dhanbad, Jharia, Giridih, Chaibasa, and other locations','2023-06-10 00:00:00',4,1,10,3,3),(2,'Tổng quan GIT','blog-post-image-guide.jpg','Coal is a valuable hard, black material extracted from mines. Wood that has been buried for a long time becomes coal due to a chemical change. Earthquakes cause vast forest areas to sink underground and contribute to such changes as a result of tremendous heat and pressure. Coal mines can be found in our country at  Dhanbad, Jharia, Giridih, Chaibasa, and other locations','2023-05-19 00:00:00',4,1,8,4,4),(3,'Theme cho VSCode','blogging-blog-word-coder-coding-260nw-520314613.jpg','Coal is a valuable hard, black material extracted from mines. Wood that has been buried for a long time becomes coal due to a chemical change. Earthquakes cause vast forest areas to sink underground and contribute to such changes as a result of tremendous heat and pressure. Coal mines can be found in our country at  Dhanbad, Jharia, Giridih, Chaibasa, and other locations','2023-03-10 00:00:00',4,1,9,4,7),(4,'Sinh viên tới CEO F8','images.jpg','Coal is a valuable hard, black material extracted from mines. Wood that has been buried for a long time becomes coal due to a chemical change. Earthquakes cause vast forest areas to sink underground and contribute to such changes as a result of tremendous heat and pressure. Coal mines can be found in our country at  Dhanbad, Jharia, Giridih, Chaibasa, and other locations','2023-05-18 00:00:00',4,1,11,4,4),(5,'Trick for BE','blogging-blog-word-coder-coding-260nw-520314613.jpg','Coal is a valuable hard, black material extracted from mines. Wood that has been buried for a long time becomes coal due to a chemical change. Earthquakes cause vast forest areas to sink underground and contribute to such changes as a result of tremendous heat and pressure. Coal mines can be found in our country at  Dhanbad, Jharia, Giridih, Chaibasa, and other locations','2022-10-12 00:00:00',4,1,10,2,6),(6,'Học viên Funix lạc đường tới F8','64448752c63c4.jpg','Mình đã tham gia khóa học lập trình 6 tháng ở funix và cũng có nhiều lý do khác dẫn đến quá hạn quá học và cũng đã hoàn thành 4 môn hiện đang cố nodejs để xong. Thực sự là ngoài hứa hẹn và tự bơi thì không có gì để bàn nhiều. Thậm chí để học xong chứng chỉ bên đó với người vừa đi làm vừa nonIT còn rất bất khả thi. Ban đầu mình cũng nghĩ tự học là tốt nhưng chính bản thân đi xin việc dơ chứng chỉ bên đó NTD nói không yên tâm với chất lượng. Cũng may là lạc sang f8 mà mình đã thuận lợi qua 4 chứng chỉ kia của funix. Tuy nhiên vì thời gian học ngắn nên chưa học kỹ hết các video bên F8. Bản thân mình thấy f8 dạy khá dễ hiểu và hiểu trước thì mới làm được. Đồng thời cộng đồng F8 chắc lạc không ít con đẻ của Funix.','2023-04-13 00:00:00',4,1,5,1,2),(7,'Deploy Spring Boot cùng SQL Server lên Azure','63f0e2755a6c0.jpg','Microsoft Azure là nền tảng tính toán đám mây được xây dựng bởi Microsoft dành cho xây dựng, kiểm thử, triển khai và quản lý các ứng dụng và dịch vụ thông qua mạng lưới trung tâm dữ liệu được quản lý bởi Microsoft. Nó cung cấp các phần mềm, nền tảng, và hệ thống cơ sở hạ tầng như các dịch vụ hỗ trợ nhiều ngôn ngữ lập trình, framework, công cụ khác nhau.','2023-04-14 00:00:00',4,1,6,2,6),(8,'So sánh JavaScript với những đối thủ khác','6305ca717d756.jpg','JavaScript và C C được compile (biên dịch) trước. Còn JavaScript được interpret (thông dịch) và đôi khi được biên dịch trong thời gian chạy bằng just-in-time (JIT) compiler. C là static typing. Còn JavaScript là dynamic typing. C yêu cầu lập trình viên phải cấp phát và lấy lại các khối bộ nhớ. JavaScript xử lý điều này tự động. Code C phải được biên dịch lại khi chuyển sang một bộ xử lý khác. JavaScript thì không cần thiết. C được thiết kế để hoạt động trực tiếp với bộ nhớ của máy tính thông qua các con trỏ. JavaScript thì không. C thường được sử dụng cho các ứng dụng nhúng vào máy tính và các ứng dụng đòi hỏi hiệu suất cao như hệ điều hành. Còn JavaScript chỉ được nhúng vào các trang web, nhưng nó đã tìm thấy vai trò mới trong các ứng dụng phía máy chủ được phát triển bởi Node.js. C cung cấp khả năng kiểm soát rõ ràng các luồng, trong khi JavaScript khuyến khích người dùng sắp xếp nhiều task bằng cách chia các tác vụ thành các hàm không đồng bộ và được gọi khi dữ liệu đã sẵn sàng.','2023-04-15 00:00:00',4,1,7,1,2),(9,'[Part 2] C#(.NET) - Tương tác với file Excel','6157e3742c6ba.jpg','Chào mọi người, trong một lần làm việc và được yêu cầu làm một tính năng X và sử dụng thư viện Microsoft.Office.Interop.Excel để thực hiện tương tác với file excel. Dưới đây là cách mình áp dụng thư viện Interop Excel vào để giảm tải thao tác trên phần mềm.','2023-04-16 00:00:00',4,1,8,2,7),(10,'Các nguồn tài nguyên hữu ích cho 1 front-end developer','613a1f36eed00.jpg','Visual studio code: là 1 trình soạn thảo code được nhiều người sử dụng nhất hiện nay với khả năng code được nhiều ngôn ngữ và rất nhiều extension hổ trợ cho việc code','2023-04-17 00:00:00',4,1,9,3,7),(11,'test2','613a1f36eed00.jpg','test','2023-04-17 00:00:00',4,0,1,1,2),(12,'test','613a1f36eed00.jpg','test','2023-04-17 00:00:00',4,3,1,1,2);
/*!40000 ALTER TABLE `blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blogcomment`
--

DROP TABLE IF EXISTS `blogcomment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `blogcomment` (
  `BlogCommentID` int NOT NULL AUTO_INCREMENT,
  `BlogID` int DEFAULT NULL,
  `UserID` int DEFAULT NULL,
  `level` int DEFAULT NULL,
  `origin_comment_id` int DEFAULT NULL,
  `reply_to_user` int DEFAULT NULL,
  `content` longtext,
  `publish` datetime DEFAULT NULL,
  PRIMARY KEY (`BlogCommentID`),
  KEY `fk_BlogComment_Blog1_idx` (`BlogID`),
  KEY `fk_BlogComment_User1_idx` (`UserID`),
  KEY `fk_BlogComment_User2_idx` (`reply_to_user`),
  CONSTRAINT `fk_BlogComment_Blog1` FOREIGN KEY (`BlogID`) REFERENCES `blog` (`BlogID`),
  CONSTRAINT `fk_BlogComment_User1` FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`),
  CONSTRAINT `fk_BlogComment_User2` FOREIGN KEY (`reply_to_user`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blogcomment`
--

LOCK TABLES `blogcomment` WRITE;
/*!40000 ALTER TABLE `blogcomment` DISABLE KEYS */;
INSERT INTO `blogcomment` VALUES (11,1,32,1,11,32,'aaaa','2023-06-25 00:00:00'),(12,1,23,2,11,32,'aaaaa','2023-06-25 00:00:00'),(13,1,35,2,11,23,'tttttt','2023-06-25 00:00:00'),(14,1,35,1,14,35,'aaaa','2023-06-25 00:00:00'),(15,1,35,2,14,35,'ttttt','2023-06-25 00:00:00'),(16,1,23,1,16,23,'ok hello','2023-06-25 00:00:00'),(17,1,23,2,16,23,'woew','2023-06-25 00:00:00'),(18,1,23,1,18,23,'hello guys','2023-06-25 00:00:00'),(19,1,23,1,19,23,'hello babe','2023-06-26 00:00:00'),(20,1,23,2,14,35,'ok','2023-06-26 00:00:00'),(21,1,23,2,11,35,'ddddddddddddddddddddddddddddddddđ','2023-06-26 00:00:00'),(22,1,23,1,22,23,'helloo gúy','2023-06-26 00:00:00'),(23,1,32,2,22,23,'welcome to F8 ','2023-06-26 00:00:00'),(24,1,32,2,22,23,'HEHE','2023-06-26 00:00:00'),(25,1,23,1,25,23,'Hellodat tien','2023-06-26 00:00:00'),(26,1,35,1,26,35,'đá','2023-06-26 00:00:00'),(27,1,35,1,27,35,'da','2023-06-26 00:00:00'),(28,1,35,2,27,35,'ok','2023-06-26 00:00:00'),(29,1,23,1,29,23,'hi','2023-06-26 00:00:00'),(30,1,23,2,29,23,'da','2023-06-26 00:00:00'),(31,1,23,2,27,35,'da','2023-06-26 00:00:00'),(32,1,23,1,32,23,'helllo broo','2023-06-27 10:16:24'),(33,1,23,1,33,23,'welcome to ll','2023-06-27 13:43:39'),(34,1,23,1,34,23,'welcome to llIII','2023-06-27 13:44:07'),(35,1,1,1,35,1,'helllo','2023-06-29 18:25:11'),(36,1,23,2,35,1,'ok','2023-06-30 00:00:00'),(37,1,23,2,35,23,'fun','2023-06-30 00:00:00'),(38,1,23,1,38,23,'<p><strong>dadadad</strong></p>','2023-06-30 09:12:55'),(39,1,23,1,39,23,'<p><em>da <img src=\"blog/623d4b2d95cec.png\" alt=\"\" width=\"180\" height=\"180\" /></em></p>','2023-06-30 09:38:02'),(44,1,23,1,44,23,'<p><span style=\"text-decoration: underline;\"><strong>Hello</strong></span></p>','2023-06-30 18:45:00'),(59,1,23,2,58,23,'ok','2023-06-30 00:00:00'),(60,1,23,2,58,23,'oke men','2023-06-30 00:00:00'),(61,1,23,2,39,23,'oke bro','2023-06-30 19:38:30'),(68,1,23,1,68,23,'<p>Heloo Men&nbsp;</p>\r\n<p><img src=\"blog/f8-icon.7ad2b161d5e80c87e516.png\" alt=\"\" width=\"123\" height=\"124\" /></p>','2023-06-30 20:00:56'),(69,1,23,2,68,23,'wow đẹp quá','2023-06-30 20:01:09'),(70,1,23,1,70,23,'<p>Hello</p>','2023-06-30 22:26:58'),(71,1,23,1,71,23,'<p><strong>Hellooo</strong></p>\r\n<p><strong><img src=\"blog/f8-icon.7ad2b161d5e80c87e516.png\" alt=\"\" width=\"89\" height=\"89\" /></strong></p>','2023-06-30 22:27:27'),(73,1,23,1,73,23,'<p>dấ</p>','2023-06-30 23:03:00'),(74,1,23,1,74,23,'<p><strong>đ&aacute;</strong></p>\r\n<p><strong><img src=\"blog/623d4b2d95cec.png\" alt=\"\" width=\"180\" height=\"180\" /></strong></p>\r\n<p><strong><img src=\"blog/318373525_886269375875027_5512792420077401664_n.jpg\" alt=\"\" width=\"283\" height=\"237\" /></strong></p>','2023-06-30 23:04:01'),(75,1,23,2,11,32,'ok mpo','2023-07-01 00:16:46'),(76,1,23,2,11,35,'ok','2023-07-01 00:24:51'),(77,1,23,2,14,35,'da','2023-07-01 00:44:02'),(78,2,23,2,73,23,'<p><strong>đ&aacute;</strong></p>','2023-07-01 01:17:28'),(79,2,23,2,73,23,'<p>oke</p>','2023-07-01 01:20:38'),(80,2,23,2,73,23,'<p>oke men</p>','2023-07-01 01:21:05'),(81,2,1,2,73,23,'<p>oke b</p>','2023-07-01 01:21:39'),(82,1,1,2,74,23,'<p>?</p>','2023-07-01 01:56:25'),(83,4,1,1,83,1,'<p><strong>đ&atilde; ty</strong></p>\r\n<p><strong><img src=\"blog/623d4b2d95cec.png\" alt=\"\" width=\"180\" height=\"180\" /></strong></p>','2023-07-01 02:13:22'),(84,3,23,1,84,23,'<p><span style=\"text-decoration: underline;\">EHello</span></p>','2023-07-01 23:25:17'),(85,3,36,2,84,23,'<p>ok</p>','2023-07-22 11:50:30');
/*!40000 ALTER TABLE `blogcomment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blogtag`
--

DROP TABLE IF EXISTS `blogtag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `blogtag` (
  `BlogTagID` int NOT NULL AUTO_INCREMENT,
  `BlogTagName` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`BlogTagID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blogtag`
--

LOCK TABLES `blogtag` WRITE;
/*!40000 ALTER TABLE `blogtag` DISABLE KEYS */;
INSERT INTO `blogtag` VALUES (1,'Front-end'),(2,'Javascript'),(3,'HTML CSS'),(4,'Other'),(5,'UI'),(6,'Backend'),(7,'VSCode');
/*!40000 ALTER TABLE `blogtag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blogtopic`
--

DROP TABLE IF EXISTS `blogtopic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `blogtopic` (
  `BlogTopicID` int NOT NULL AUTO_INCREMENT,
  `BlogTopicName` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`BlogTopicID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blogtopic`
--

LOCK TABLES `blogtopic` WRITE;
/*!40000 ALTER TABLE `blogtopic` DISABLE KEYS */;
INSERT INTO `blogtopic` VALUES (1,'Front-end / Mobile App'),(2,'Back-end / DevOps'),(3,'UI / UX / Design'),(4,'Others');
/*!40000 ALTER TABLE `blogtopic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `CategoryID` int NOT NULL,
  `Name` varchar(20) DEFAULT NULL,
  `Description` varchar(200) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`CategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'FRONT END',null,null,1),(2,'BACK END',null,null,1),(3,'BASIC',null,null,1);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course`
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `CourseID` int NOT NULL,
  `Name` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `Image` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `CourseInfo` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `Description` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `Status` int DEFAULT NULL,
  `User_UserID` int NOT NULL,
  `Category_CategoryID` int NOT NULL,
  `price` int NOT NULL,
  PRIMARY KEY (`CourseID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;




--
-- Table structure for table `courseenroll`
--

DROP TABLE IF EXISTS `courseenroll`;
CREATE TABLE `courseenroll` (
  `User_UserID` int NOT NULL,
  `Course_CourseID` int NOT NULL,
  `Status` int NOT NULL,
  `EnrollDate` date DEFAULT NULL,
  PRIMARY KEY (`User_UserID`,`Course_CourseID`),
  FOREIGN KEY (`User_UserID`)
  REFERENCES `f8db`.`user` (`UserID`),
  FOREIGN KEY (`Course_CourseID`)
  REFERENCES `f8db`.`course` (`CourseID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


--
-- Dumping data for table `courseenroll`
--

LOCK TABLES `courseenroll` WRITE;
/*!40000 ALTER TABLE `courseenroll` DISABLE KEYS */;
/*!40000 ALTER TABLE `courseenroll` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dicuss`
--

DROP TABLE IF EXISTS `dicuss`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dicuss` (
  `DicussID` int NOT NULL,
  `LessonDetail_LessonDetailID` int NOT NULL,
  PRIMARY KEY (`DicussID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dicuss`
--

LOCK TABLES `dicuss` WRITE;
/*!40000 ALTER TABLE `dicuss` DISABLE KEYS */;
/*!40000 ALTER TABLE `dicuss` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lesson`
--

DROP TABLE IF EXISTS `lesson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lesson` (
  `LessonID` int NOT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `Video` varchar(100) DEFAULT NULL,
  `Course_CourseID` int NOT NULL,
  PRIMARY KEY (`LessonID`),
  KEY `fk_Lesson_Course1_idx` (`Course_CourseID`),
  CONSTRAINT `fk_Lesson_Course1` FOREIGN KEY (`Course_CourseID`) REFERENCES `course` (`CourseID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lesson`
--

LOCK TABLES `lesson` WRITE;
/*!40000 ALTER TABLE `lesson` DISABLE KEYS */;
/*!40000 ALTER TABLE `lesson` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lessondetail`
--

DROP TABLE IF EXISTS `lessondetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lessondetail` (
  `LessonDetailID` int NOT NULL,
  `Title` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `Video` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `Note` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `Time` time DEFAULT NULL,
  `Lesson_LessonID` int NOT NULL,
  `Dicuss_DicussID` int NOT NULL,
  PRIMARY KEY (`LessonDetailID`),
  KEY `fk_LessonDetail_Lesson1_idx` (`Lesson_LessonID`),
  KEY `fk_LessonDetail_Dicuss1_idx` (`Dicuss_DicussID`),
  CONSTRAINT `fk_LessonDetail_Dicuss1` FOREIGN KEY (`Dicuss_DicussID`) REFERENCES `dicuss` (`DicussID`),
  CONSTRAINT `fk_LessonDetail_Lesson1` FOREIGN KEY (`Lesson_LessonID`) REFERENCES `lesson` (`LessonID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lessondetail`
--

LOCK TABLES `lessondetail` WRITE;
/*!40000 ALTER TABLE `lessondetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `lessondetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `likecomment`
--

DROP TABLE IF EXISTS `likecomment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `likecomment` (
  `User_UserID` int NOT NULL,
  `BlogComment_BlogCommentID` int NOT NULL,
  KEY `fk_LikeComment_User1_idx` (`User_UserID`),
  KEY `fk_LikeComment_BlogComment1_idx` (`BlogComment_BlogCommentID`),
  CONSTRAINT `fk_LikeComment_BlogComment1` FOREIGN KEY (`BlogComment_BlogCommentID`) REFERENCES `blogcomment` (`BlogCommentID`),
  CONSTRAINT `fk_LikeComment_User1` FOREIGN KEY (`User_UserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `likecomment`
--

LOCK TABLES `likecomment` WRITE;
/*!40000 ALTER TABLE `likecomment` DISABLE KEYS */;
INSERT INTO `likecomment` VALUES (1,11),(1,82),(2,82),(36,84);
/*!40000 ALTER TABLE `likecomment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reply`
--

DROP TABLE IF EXISTS `reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reply` (
  `ReplyID` int NOT NULL,
  `ReplyDetail` varchar(2000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `Ask_AskID` int NOT NULL,
  PRIMARY KEY (`ReplyID`),
  KEY `fk_Reply_Ask1_idx` (`Ask_AskID`),
  CONSTRAINT `fk_Reply_Ask1` FOREIGN KEY (`Ask_AskID`) REFERENCES `ask` (`AskID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reply`
--

LOCK TABLES `reply` WRITE;
/*!40000 ALTER TABLE `reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `saveblog`
--

DROP TABLE IF EXISTS `saveblog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `saveblog` (
  `User_UserID` int NOT NULL,
  `Blog_BlogID` int NOT NULL,
  `save_day` datetime DEFAULT NULL,
  KEY `fk_SaveBlog_User1_idx` (`User_UserID`),
  KEY `fk_SaveBlog_Blog1_idx` (`Blog_BlogID`),
  CONSTRAINT `fk_SaveBlog_Blog1` FOREIGN KEY (`Blog_BlogID`) REFERENCES `blog` (`BlogID`),
  CONSTRAINT `fk_SaveBlog_User1` FOREIGN KEY (`User_UserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `saveblog`
--

LOCK TABLES `saveblog` WRITE;
/*!40000 ALTER TABLE `saveblog` DISABLE KEYS */;
INSERT INTO `saveblog` VALUES (1,1,'2023-06-25 00:00:00'),(32,1,'2023-06-26 00:00:00'),(32,3,'2023-06-26 00:00:00'),(32,2,'2023-06-26 00:00:00'),(35,1,'2023-06-26 00:00:00'),(35,2,'2023-06-26 00:00:00'),(1,2,'2023-06-26 00:00:00'),(23,1,'2023-06-27 00:00:00'),(23,3,'2023-06-30 21:50:52'),(23,2,'2023-06-30 21:50:53'),(36,1,'2023-07-22 16:27:06');
/*!40000 ALTER TABLE `saveblog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `savelike`
--

DROP TABLE IF EXISTS `savelike`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `savelike` (
  `Blog_BlogID` int NOT NULL,
  `User_UserID` int NOT NULL,
  KEY `fk_SaveLike_Blog1_idx` (`Blog_BlogID`),
  KEY `fk_SaveLike_User1_idx` (`User_UserID`),
  CONSTRAINT `fk_SaveLike_Blog1` FOREIGN KEY (`Blog_BlogID`) REFERENCES `blog` (`BlogID`),
  CONSTRAINT `fk_SaveLike_User1` FOREIGN KEY (`User_UserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `savelike`
--

LOCK TABLES `savelike` WRITE;
/*!40000 ALTER TABLE `savelike` DISABLE KEYS */;
INSERT INTO `savelike` VALUES (1,30),(3,23),(2,23),(1,26),(4,23),(5,23),(1,1),(1,23);
/*!40000 ALTER TABLE `savelike` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `UserID` int NOT NULL AUTO_INCREMENT,
  `Email` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `Facebook` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `Github` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `Password` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `Phone` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `FullName` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `Image` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `Dob` date DEFAULT NULL,
  `Address` varchar(80) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `GmailID` varchar(80) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `FacebookID` varchar(80) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `GithubID` varchar(80) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `Status` int DEFAULT NULL,
  `UserRole_RoleID` int NOT NULL,
  `CodeVerify` varchar(15) DEFAULT NULL,
  `Bio` varchar(300) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `BackgroundImage` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  PRIMARY KEY (`UserID`),
  KEY `fk_User_UserRole1_idx` (`UserRole_RoleID`),
  CONSTRAINT `fk_User_UserRole1` FOREIGN KEY (`UserRole_RoleID`) REFERENCES `userrole` (`RoleID`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--
--
-- Table structure for table `userrole`
--

DROP TABLE IF EXISTS `userrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `userrole` (
  `RoleID` int NOT NULL AUTO_INCREMENT,
  `Name` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userrole`
--

LOCK TABLES `userrole` WRITE;
/*!40000 ALTER TABLE `userrole` DISABLE KEYS */;
INSERT INTO `userrole` VALUES (1,'Admin'),(2,'Customer'),(3,'Sales'),(4,'Expert'),(5,'Marketing');
/*!40000 ALTER TABLE `userrole` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-08-08 17:44:33

-- Create Table invoice(hoa don)
CREATE TABLE `invoice` (
  `InvoiceID` int NOT NULL,
  `User_UserID` int NOT NULL,
  `price` int NOT NULL,
  `CreateDate` date DEFAULT NULL,
  `Status` int NOT NULL,
  `Description` varchar(200) DEFAULT NULL,
  `Course_CourseID` int NOT NULL,
  PRIMARY KEY (`InvoiceID`, `User_UserID`), -- Composite primary key
  KEY `User_UserID_idx` (`User_UserID`),
  KEY `Course_CourseID_idx` (`Course_CourseID`),
FOREIGN KEY (`Course_CourseID`) REFERENCES `course` (`CourseID`),
FOREIGN KEY (`User_UserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- Add data for course
-- Free Course

-- free course
 INSERT INTO `course` VALUES (1,'Kiến Thức Nhập Môn IT','freec1.jpg','Để có cái nhìn tổng quan về ngành IT - Lập trình web các bạn nên xem các videos tại khóa này trước nhé.','Để theo ngành IT - Phần mềm cần rèn luyện những kỹ năng nào? ',1,1,2,0);
insert into course values (2, 'Lập trình C++ cơ bản, nâng cao', 'freec2.jpg', 'Để có cái nhìn tổng quan về lập trình C, các bạn nên xem các videos tại khóa này trước nhé.' ,'Khóa học lập trình C++ từ cơ bản tới nâng cao dành cho người mới bắt đầu. Mục tiêu của khóa học này nhằm giúp các bạn nắm được các khái niệm căn cơ của lập trình, giúp các bạn có nền tảng vững chắc để chinh phục con đường trở thành một lập trình viên.', 1, 1, 2,0);
insert into course values (3, 'HTML CSS từ Zero đến Hero', 'freec3.jpg', 'Để có cái nhìn tổng quan về HTML - CSS, các bạn nên xem các videos tại khóa này trước nhé.' ,'Khóa học lập trình C++ từ cơ bản tới nâng cao dành chos người mới bắt đầu. Mục tiêu của khóa học này nhằm giúp các bạn nắm được các khái niệm căn cơ của lập trình, giúp các bạn có nền tảng vững chắc để chinh phục con đường trở thành một lập trình viên.', 1,1, 1,0);
insert into course values (4, 'Lập Trình JavaScript Cơ Bản', 'freec4.jpg', 'Để có cái nhìn tổng quan về JavaScript, các bạn nên xem các videos tại khóa này trước nhé.' ,'Học Javascript cơ bản phù hợp cho người chưa từng học lập trình. Với hơn 100 bài học và có bài tập thực hành sau mỗi bài học.', 1, 1, 2,0);
insert into course values (5, 'Làm việc với Terminal & Ubuntu', 'freec5.jpg', 'Để có cái nhìn tổng quan về Terminal & Ubuntu, các bạn nên xem các videos tại khóa này trước nhé.' ,'Sở hữu một Terminal hiện đại, mạnh mẽ trong tùy biến và học cách làm việc với Ubuntu là một bước quan trọng trên con đường trở thành một Web Developer.', 1, 1, 2,0);
-- Pro Course
insert into course values (6, 'HTML CSS PRO từ Zero đến Hero', 'proc6.jpg', 'Khóa học này phù hợp với người hoàn toàn chưa biết về HTML CSS. Bạn chỉ cần biết sử dụng máy vi tính ở mức căn bản là có thể tham gia học.' ,'Với 400+ bài học, bài tập và thử thách, đây sẽ là khóa học đầy đủ và chi tiết nhất bạn có thể tìm kiếm được ở trên Internet.', 1, 3, 1,1000000);
insert into course values (7, 'Responsive Với Grid System', 'proc7.jpg', 'Khóa học này phù hợp với người hoàn toàn chưa biết về Responsive của hệ thống Grid Bootstrap 4. Bạn chỉ cần biết sử dụng máy vi tính ở mức căn bản là có thể tham gia học.' ,'Trong khóa này chúng ta sẽ học về cách xây dựng giao diện web responsive với Grid System, tương tự Bootstrap 4.', 1, 1, 1,800000);
insert into course values (8, 'Xây Dựng Website với ReactJS', 'proc8.jpg', 'Khóa học này phù hợp với người hoàn toàn chưa biết về ReactJS. Bạn chỉ cần biết sử dụng máy vi tính ở mức căn bản là có thể tham gia học.' ,'Khóa học ReactJS từ cơ bản tới nâng cao, kết quả của khóa học này là bạn có thể làm hầu hết các dự án thường gặp với ReactJS. Cuối khóa học này bạn sẽ sở hữu một dự án giống Tiktok.com, bạn có thể tự tin đi xin việc khi nắm chắc các kiến thức được chia sẻ trong khóa học này.', 1, 1, 1,900000);

-- Add account pass 1
insert into user values(1,'1@1.com',null,null,'C4CA4238A0B923820DCC509A6F75849B',null,'admin', 'anhdefault.jpg',null,null,NULL,null,null,1,1,null,null,null);
insert into user values(2,'2@2.com',null,null,'C4CA4238A0B923820DCC509A6F75849B',null,'customer', 'anhdefault.jpg',null,null,NULL,null,null,1,2,null,null,null);
insert into user values(3,'3@3.com',null,null,'C4CA4238A0B923820DCC509A6F75849B',null,'saler', 'anhdefault.jpg',null,null,NULL,null,null,1,3,null,null,null);
insert into user values(4,'4@4.com',null,null,'C4CA4238A0B923820DCC509A6F75849B',null,'expert', 'anhdefault.jpg',null,null,NULL,null,null,1,4,null,null,null);

-- Add table Blog Report
CREATE TABLE blogreport (
    User_UserID INT,
    Blog_BlogID INT,
    FOREIGN KEY (Blog_BlogID)
        REFERENCES f8db.blog (BlogID),
    FOREIGN KEY (User_UserID)
        REFERENCES f8db.user (UserID)
);

-- Add constraint 'Course'
ALTER TABLE `f8db`.`course` 
ADD INDEX `fk_userid_userr_idx` (`User_UserID` ASC) VISIBLE,
ADD INDEX `fk_category_courseee_idx` (`Category_CategoryID` ASC) VISIBLE;
;
ALTER TABLE `f8db`.`course` 
ADD CONSTRAINT `fk_userid_userr`
  FOREIGN KEY (`User_UserID`)
  REFERENCES `f8db`.`user` (`UserID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_category_courseee`
  FOREIGN KEY (`Category_CategoryID`)
  REFERENCES `f8db`.`category` (`CategoryID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  
  CREATE TABLE `newfeed` (
  `FeedID` int NOT NULL AUTO_INCREMENT,
  `FeedTitle` varchar(200) DEFAULT NULL,
  `Description` varchar(200) DEFAULT NULL,
  `Image` varchar(45) DEFAULT NULL,
  `FeedVideo` varchar(100) DEFAULT NULL,
  `Author` varchar(50) DEFAULT NULL,
  `CreateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`FeedID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `f8db`.`lesson` (`LessonID`, `Name`, `Video`, `Course_CourseID`) VALUES ('3', 'C#', 'https://www.youtube.com/watch?v=7GVFYt6_ZFM&list=PL08903FB7ACA1C2FB&ab_channel=kudvenkat', '1');
INSERT INTO `f8db`.`lesson` (`LessonID`, `Name`, `Video`, `Course_CourseID`) VALUES ('4', 'C++', 'https://youtu.be/7GVFYt6_ZFM?list=PL08903FB7ACA1C2FB', '2');
INSERT INTO `f8db`.`lesson` (`LessonID`, `Name`, `Video`, `Course_CourseID`) VALUES ('5', 'C', 'SWE201c_PEA_10diem', '3');
INSERT INTO `f8db`.`lesson` (`LessonID`, `Name`, `Video`, `Course_CourseID`) VALUES ('6', 'Java', 'Java', '4');
INSERT INTO `f8db`.`lesson` (`LessonID`, `Name`, `Video`, `Course_CourseID`) VALUES ('7', 'Python', 'video6', '5');
INSERT INTO `f8db`.`lesson` (`LessonID`, `Name`, `Video`, `Course_CourseID`) VALUES ('8', 'PHP', 'video 7', '6');
INSERT INTO `f8db`.`lesson` (`LessonID`, `Name`, `Video`, `Course_CourseID`) VALUES ('9', 'Reactjs', 'video8', '7');
INSERT INTO `f8db`.`lesson` (`LessonID`, `Name`, `Video`, `Course_CourseID`) VALUES ('10', 'NodeJS', 'video9', '8');

