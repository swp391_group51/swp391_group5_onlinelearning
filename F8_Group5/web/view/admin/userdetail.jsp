<%-- 
    Document   : newnlog
    Created on : May 24, 2023, 10:02:02 PM
    Author     : trank
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8" />
        <!-- favicon -->
        <link rel="shortcut icon" href="../../../assets/images/favicon.ico.png">
        <!-- Bootstrap -->
        <link href="../../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Select2 -->
        <link href="../../../assets/css/select2.min.css" rel="stylesheet" />
        <!-- Date picker -->
        <link rel="stylesheet" href="../../../assets/css/flatpickr.min.css">
        <link href="../../../assets/css/jquery.timepicker.min.css" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="../../../assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="../../../assets/css/remixicon.css" rel="stylesheet" type="text/css" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <!-- Css -->
        <link href="../../../assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />

    </head>

    <body>
        <%@include file="/view/common/header-back.jsp"%>
    </div>
</div><!--end col-->
</div><!--end row-->
</div><!--end container-->
</section><!--end section-->
<!-- End Hero -->
<div class="position-relative">
    <div class="shape overflow-hidden text-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<!-- End Hero -->
<style>
    .preview-box {
        width: 100%; /* Set the width of the preview box */
        height: auto; /* Let the height adjust based on the image */
        text-align: center; /* Center the image horizontally */
        overflow: hidden; /* Hide any overflowing parts of the image */
    }

    .preview-content {
        max-width: 100%; /* Set a maximum width to prevent the image from exceeding the box width */
        max-height: 100%; /* Set a maximum height to prevent the image from exceeding the box height */
    }
</style>
<!-- Start -->
<section class="section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="card border-0 shadow rounded overflow-hidden">
                    <ul class="nav nav-pills nav-justified flex-column flex-sm-row rounded-0 shadow overflow-hidden bg-light mb-0" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link rounded-0 active" id="clinic-booking" data-bs-toggle="pill"  role="tab" aria-controls="pills-clinic" aria-selected="false">
                                <div class="text-center pt-1 pb-1">
                                    <h4 class="title fw-normal mb-0">User Details</h4>
                                </div>
                            </a><!--end nav link-->
                        </li><!--end nav item-->


                    </ul>

                    <form action="blog" enctype="multipart/form-data" method="post">
                        <div class="tab-content p-4" id="pills-tabContent">
                            <div class="modal-body p-3 pt-4">
                                <div class="row">



                                    <div class="col-md-8 mt-4 mt-sm-0">
                                        <div class="ms-md-4">
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="mb-3">
                                                        <label class="form-label"></label>
                                                        <img src="assets/images/userpicture/${user.getImage()}" class="rounded-circle shadow-md avatar avatar-medium" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="mb-3">
                                                        <label class="form-label">Full Name:</label>
                                                        <input name="Name" id="name" type="text" class="form-control" value="${user.getFullName()}" readonly>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="mb-3">
                                                        <label class="form-label">Email:</label>
                                                        <input name="Email" id="email" type="text" class="form-control" value="${user.getEmail()}"  readonly="">
                                                    </div> 
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="mb-3">
                                                        <label class="form-label">Role:</label>
                                                        <input name="Email" id="email" type="text" class="form-control" value="${user.getUserRole().getName()}" placeholder="Chưa có" readonly="">
                                                    </div> 
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="mb-3">
                                                        <label class="form-label">Bio:</label>
                                                        <input name="Email" id="email" type="text" class="form-control" value="${user.getBio()}" placeholder="Empty" readonly="">
                                                    </div> 
                                                </div> 
                                                <div class="col-md-12">
                                                    <div class="mb-3">
                                                        <label class="form-label">Status:</label>
                                                        <input name="Email" id="email" type="text" class="form-control" value="${user.getStatus() == 1 ? "Active":"Banned"}" placeholder="Chưa liên kết" readonly="">
                                                    </div> 
                                                </div> 

                                            </div>
                                        </div>
                                    </div><!--end col-->
                                </div><!--end row-->
                            </div>
                        </div>
                    </form>




                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->
</section><!--end section-->
<!-- End -->




<%@include file="/view/common/footer.jsp"%>

<!-- javascript -->
<script src="../../../assets/js/jquery.min.js"></script>
<script src="../../../assets/js/bootstrap.bundle.min.js"></script>

<script src="../../../assets/js/select2.min.js"></script>
<script src="../../../assets/js/select2.init.js"></script>

<script src="../../../assets/js/flatpickr.min.js"></script>
<script src="../../../assets/js/flatpickr.init.js"></script>

<script src="../../../assets/js/jquery.timepicker.min.js"></script> 
<script src="../../../assets/js/timepicker.init.js"></script>

<script src="../../../assets/js/feather.min.js"></script>

<script src="../../../assets/js/app.js"></script>



</body>
</html>
