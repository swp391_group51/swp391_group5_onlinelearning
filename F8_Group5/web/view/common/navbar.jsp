<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : navbar
    Created on : May 19, 2023, 9:47:55 AM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- Nav Bar -->
<head>
    <!-- Icons -->
    <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css" />
    <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
    <style>
        .col-xl-2 {
            flex: 0 0 auto;
            width: 140px;
        }
    </style>
</head>
<ul class="list-unstyled sidebar-nav mb-0">
    <br>
    <c:if test="${expert!=null}">
        <li class="navbar-item"><a href="blog" class="navbar-link"><i class="ri-chat-1-line align-middle navbar-icon"></i><i style="color: blue">Viết Blog</i></a></li>
                </c:if>
                <c:if test="${expert!=null}">
        <li class="navbar-item"><a href="myblog" class="navbar-link"><i class="ri-chat-1-line align-middle navbar-icon"></i><i style="color: blue">Blog của tôi</i></a></li>
                </c:if> 
    <li class="navbar-item"><a href="home" class="navbar-link"><i class="ri-home-8-line align-middle navbar-icon" style="color: #000"></i>Home</a></li>
    <li class="navbar-item"><a href="programTraining" class="navbar-link"><i class="ri-calendar-check-line align-middle navbar-icon"></i>Lộ trình</a></li>
    <li class="navbar-item"><a href="coursePublic" class="navbar-link"><i class="ri-projector-line align-middle navbar-icon"></i>Học</a></li>
    <li class="navbar-item"><a href="bloglist" class="navbar-link"><i class="ri-pages-line align-middle navbar-icon"></i>Blog</a></li>
    <li class="navbar-item">
        <a href="newfeedmanager" target="_blank" class="navbar-link">
            <i class="ri-volume-up-line align-middle navbar-icon"></i>
            New Feed
        </a>
    </li>
    <li class="navbar-item">
        <c:if test = "${admin != null}">
            <a href="addnewfeed?AdminID=${admin.getUserID()}" target="_blank" class="navbar-link">
            <i class="ri-add-line align-middle navbar-icon"></i>
            Add New Feed
        </a>
        </c:if>
    </li>
</ul>
<!-- End Nav Bar -->