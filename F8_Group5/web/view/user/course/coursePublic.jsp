<%-- 
    Document   : coursePublic
    Created on : Jun 21, 2023, 5:19:20 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="vi">
    <head>
        <meta charset="UTF-8">
        <title>Danh sách các khóa học lập trình tại F8 | by F8</title>
        <!-- F8logo -->
        <!-- favicon -->
        <link rel="shortcut icon" href="assets/images/logoF8.png">
        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <!-- Css -->
        <link href="assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />
        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- SLIDER -->
        <link rel="stylesheet" href="assets/css/tiny-slider.css"/>
        <!-- Icons -->
        <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">

        <style>
            .tns-outer [aria-controls], .tns-outer[data-action]{
                cursor: var;
                background: black;
                width: 50px;
            }

            .tns-nav{
                text-align: center;
                margin-top: 10px;
                margin-right: 1070px;
            }

            .row {
                --bs-gutter-x: 1.5rem;
                --bs-gutter-y: 0;
                display: flex;
                flex-wrap: wrap;
                margin-top: calc(var(--bs-gutter-y) * -1);
                margin-right: calc(var(--bs-gutter-x) * -.5);
                margin-left: calc(var(--bs-gutter-x) * -.5);
                width: 1350px;
            }

            .container-right {
                width: 100%;
                overflow: hidden; /* Clear floats */
            }

            .content-right {
                float: right;
                margin-right: 25px;
            }
        </style>

    </head>

    <body>
        <%@include file="/view/common/header.jsp"%>
        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div>
        <!-- End Loader -->

        <!-- start section -->
        <!-- all fixed but no database added yet -->
        <section class="bg-dashboard">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-2 col-lg-2 col-md-2">
                        <div class="rounded overflow-hidden sticky-bar">
                            <%@include file="/view/common/navbar.jsp"%>
                        </div>
                    </div>

                    <div class="col-lg-10 mt-4">
                        <strong style="font-size: 25px">Khóa học</strong>
                        <p>Các khóa học được thiết kế phù hợp cho cả người mới, nhiều khóa học miễn phí, chất lượng, nội dung dễ hiểu.</p>
                        <br><br>
                        <div class="row">
                            <!-- show khoa hoc pro --> 
                            <br>
                            <div class="row">
                                <strong><a style="color: #000 ; font-size: 27px">Khóa học Pro</a><span class="badge bg-danger rounded-pill ms-1"><strong>New</strong></span></strong>
                                <div class="row row-cols-md-2 row-cols-lg-4">
                                    <!-- Courses -->
                                    <c:choose>
                                        <c:when test="${empty listProCourse}">
                                            <div class="col-12 mt-5 text-center">
                                                <span style="display: block; color: red">There are no courses available.</span>
                                            </div>
                                        </c:when>
                                        <c:otherwise>
                                            <c:forEach items="${listProCourse}" var="p">
                                                <div class="col-3 mt-4">
                                                    <div class="card team border-0 rounded overflow-hidden">
                                                        <div class="team-person position-relative overflow-hidden"> 
                                                            <a href="viewCourse?CourseID=${p.getCourseID()}">
                                                                <img src="assets/images/course/${p.getImage()}" class="img-fluid" alt="" style="width: 350px; height: 180px">
                                                            </a>
                                                        </div>
                                                        <div>
                                                            <br/>
                                                            <a href="viewCourse?CourseID=${p.getCourseID()}" class="title text-dark h6 d-block mb-0">${p.getName()}</a>
                                                            <small class="text-muted speciality">
                                                                <p style="color: orangered; font-size: 15px" class="price">${p.getPrice()}</p>
                                                            </small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </c:forEach>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                            <!-- end show khoa hoc pro -->


                            <!-- show khoa hoc mien phi -->
                            <br>
                            <div class="row">
                                <div class="col-6">
                                    <strong><a style="color: #000 ; font-size: 27px">Khóa học miễn phí</a></strong>
                                </div>
                                <div class="row row-cols-md-2 row-cols-lg-4">
                                    <!-- Courses -->
                                    <c:choose>
                                        <c:when test="${empty listFreeCourse}">
                                            <div class="col-12 mt-5 text-center">
                                                <span style="display: block; color: red">There are no courses available.</span>
                                            </div>
                                        </c:when>
                                        <c:otherwise>
                                            <c:forEach items="${listFreeCourse}" var="f">
                                                <div class="col-3 mt-4">
                                                    <div class="card team border-0 rounded overflow-hidden">
                                                        <div class="team-person position-relative overflow-hidden">
                                                            <a href="viewCourse?CourseID=${f.getCourseID()}">
                                                                <img src="assets/images/course/${f.getImage()}" class="img-fluid" alt="" style="width: 350px; height: 180px">
                                                            </a>
                                                        </div>
                                                        <div>
                                                            <br/>
                                                            <a href="viewCourse?CourseID=${f.getCourseID()}" class="title text-dark h6 d-block mb-0">${f.getName()}</a>
                                                            <small class="text-muted speciality">
                                                                <p style="color: orangered; font-size: 15px" class="price">${f.getPrice()}</p>
                                                            </small>
                                                        </div>
                                                    </div>
                                                </div>
                                                <c:if test="${empty f}">
                                                    <p>No course found</p>
                                                </c:if>
                                            </c:forEach>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                            <!-- end show khao hoc mien phi -->
                        </div>
                        <br><br><br><!-- tam thoi -->

                        <div class="row">
                            <div class="col-4">
                                <br><br><br><br>
                                <strong><a style="color: #000 ; font-size: 27px">Bạn đang tìm kiếm lộ trình học cho người mới?</a></strong>
                                <p>Các khóa học được thiết kế phù hợp cho người mới, lộ trình học rõ ràng, nội dung dễ hiểu.</p>
                                <a href="programTraining" class="btn btn-pills btn-outline-dark"> Xem lộ trình </a>
                            </div>

                            <div class="col-4">
                            </div>

                            <div class="col-4">
                                <div class="container-right">
                                    <div class="content-right">
                                        <div class="card team border-0 rounded overflow-hidden">
                                            <div class="team-person position-relative overflow-hidden">
                                                <img style="height: 400px; width: 600px;" src="assets/images/course/intro.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end show khao hoc mien phi -->
                </div>
            </div>
        </div>
    </section>
    <!-- End section -->

    <!-- JavaScript -->
    <!--convert price to currency-->
    <script>
        // Lấy tất cả các ô trong bảng có class "price"
        var priceValue = document.querySelectorAll('.price');

        priceValue.forEach(cell => {
            const price = parseInt(cell.textContent);

            //neu price = 0 thi hien thi Free
            if (price === 0) {
                cell.textContent = 'Free';
            } else {
                const formattedPrice = price.toLocaleString('vi-VN') + ' VND';
                cell.textContent = formattedPrice;
            }
        });
    </script>
    <!-- Home -->
    <!-- javascript -->
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <!-- SLIDER -->
    <script src="assets/js/tiny-slider.js"></script>
    <script src="assets/js/tiny-slider-init.js"></script>
    <!-- Chart -->
    <script src="assets/js/apexcharts.min.js"></script>
    <script src="assets/js/areachart.init.js"></script>
    <!-- Icons -->
    <script src="assets/js/feather.min.js"></script>
    <!-- Main Js -->
    <script src="assets/js/app.js"></script>

    <!-- javascript -->
    <script src="https://kit.fontawesome.com/4ec0e85a31.js" crossorigin="anonymous"></script>
    <script src="../../../assets/js/bootstrap.bundle.min.js"></script>
    <!-- SLIDER -->
    <script src="../../../assets/js/tiny-slider.js"></script>
    <script src="../../../assets/js/tiny-slider-init.js"></script>
    <!-- Icons -->
    <script src="../../../assets/js/feather.min.js"></script>
    <!-- Main Js -->
    <script src="../../../assets/js/app.js"></script>

    <br><br>
    <!-- footer -->
    <%@include file="/view/common/footer.jsp"%>
    <!-- end footer -->
</body>
</html>
