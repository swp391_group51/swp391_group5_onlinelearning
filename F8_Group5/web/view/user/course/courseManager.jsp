<%-- 
    Document   : courseManagement
    Created on : Jun 10, 2023, 8:49:26 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="vi">

    <head>
        <meta charset="utf-8" />
        <title>Quản lý danh sách khóa học</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
        <meta name="keywords" content="Appointment, Booking, System, Dashboard, Health" />
        <meta name="author" content="Shreethemes" />
        <meta name="email" content="support@shreethemes.in" />
        <meta name="website" content="index.html" />
        <meta name="Version" content="v1.2.0" />
        <!-- favicon -->
        <link rel="shortcut icon" href="assets/images/logoF8.png">
        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- simplebar -->
        <link href="assets/css/simplebar.css" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <!-- Css -->
        <link href="assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />

        <!--refresh page if delete successfully-->
        <script>
            function confirmDelete(id) {
                var confirm = window.confirm("Chắc chắn muốn xóa khóa học này?");
                if (confirm) {
                    window.location.href = "deleteCourse?CourseID=" + id;
                    alert("Xoa thanh cong");
                }
            }
        </script>

    </head>

    <body>
        <%@include file="/view/common/header-back.jsp"%>
        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div>
        <!-- Loader -->

        <div class="page-wrapper doctris-theme">
            <!-- Start Page Content -->
            <main class="page-content bg-light">
                <div class="container-fluid">
                    <div class="layout-specing">
                        <div class="row">
                            <div class="d-md-flex justify-content-between">
                                <h4 style="color: orangered; font-family: inherit">Quản lý danh sách khóa học</h4>
                                <form class="form-inline" method="post" action="courseManager">
                                    <div class="form-group">
                                        <input type="text" placeholder="Search courses..." class="form-control" name="search"/>
                                        <input type="submit" class="btn btn-primary form-control" value="Search" />
                                    </div>
                                </form>
                                <c:choose>
                                    <c:when test="${not empty listCourse}">
                                        <h7>
                                            <a href="addCourse" class="btn btn-primary">Thêm khóa học mới</a>
                                        </h7>
                                    </c:when>
                                    <c:when test="${empty listCourse}">
                                        <h7>
                                            <a href="addCourse" class="btn btn-primary" hidden="">Thêm khóa học mới</a>
                                        </h7>
                                    </c:when>
                                </c:choose>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 mt-3">
                                <c:if test="${search != null && search != ''}">
                                    <p style="font-size: 20px">Search result for: ${search}</p>
                                </c:if>
                            </div>
                            <div class="col-12 mt-4">
                                <c:choose>
                                    <c:when test="${empty listCourse}">
                                        <span style="display: block; text-align: center; margin-top: 300px; font-size: 25px">
                                            No course existed, create a new one <a href="/F8_Group6/addCourse">here</a>.
                                        </span>
                                    </c:when>
                                    <c:otherwise>
                                        <div class="table-responsive shadow rounded">                             
                                            <table class="table table-center bg-white mb-0">
                                                <thead>
                                                    <tr>
                                                        <th scope="col" class="border-bottom text-center" style="width: 300px;">Course Name</th>   
                                                        <th scope="col" class="border-bottom text-center" style="width: 300px;">Course Image</th>

                                                        <th scope="col" class="border-bottom text-center" style="width: 200px;">Category</th>
                                                        <th scope="col" class="border-bottom text-center" style="width: 200px;">Public Status</th>
                                                        <th scope="col" class="border-bottom text-center" style="width: 200px;">Fee Course</th>
                                                        <th scope="col" class="border-bottom text-center" style="width: 300px;">Admin Created</th>
                                                        <th scope="col" class="border-bottom text-center" style="width: 300px;">Actions</th>
                                                    </tr>
                                                </thead>                                     
                                                <tbody>
                                                    <c:forEach items="${listCourse}" var="c">
                                                        <tr>
                                                            <td scope="row" class="text-center"><a href="viewCourse?CourseID=${c.getCourseID()}">${c.getName()}</a></td>
                                                            <td scope="row" class="text-center"><img src="assets/images/course/${c.getImage()}" alt="${c.getName()}" style="width: 200px; height: 110px"/></td>
                                                            <td scope="row" class="text-center" style="font-weight: normal">

                                                                <c:forEach items="${listCategory}" var="category">
                                                                    <c:if test="${category.getCategoryID() == c.getCategory_CategoryID()}">
                                                                        ${category.getName()}
                                                                    </c:if>
                                                                </c:forEach>

                                                            </td>
                                                            <td scope="row" class="text-center" style="font-weight: normal">
                                                                <c:if test="${c.getStatus() == 1}">
                                                                    <span style="color: green">Show</span>
                                                                </c:if>
                                                                <c:if test="${c.getStatus() == 0}">
                                                                    <span style="color: red">Hidden</span>
                                                                </c:if>
                                                            </td>
                                                            <td scope="row" class="text-center priceCell" style="font-weight: normal">
                                                                ${c.getPrice()}
                                                            </td>
                                                            <td scope="row" class="text-center" style="font-weight: normal">
                                                                <c:forEach items="${listUser}" var="user">
                                                                    <c:if test="${user.getUserID() == c.getUser_UserID()}">
                                                                        <p>${user.getFullName()}</p>
                                                                    </c:if>
                                                                </c:forEach>
                                                            </td>
                                                            <td scope="row" class="text-center" style="font-weight: normal">
                                                                <a href="updateCourse?CourseID=${c.getCourseID()}" class="btn btn-primary">Edit</a>
                                                                <a class="btn btn-danger" onclick="confirmDelete('${c.getCourseID()}')">Delete</a>
                                                            </td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                            <div class="mt-4">
                                <ul class="pagination justify-content-center mb-0 list-unstyled">
                                    <c:choose>
                                        <c:when test="${totalPage > 1}">
                                            <c:forEach begin="1" end="${totalPage}" var="i">
                                                <c:set var="activeClass" value="${(i == currentPage) ? 'active' : ''}" />
                                                <li class="page-item ${activeClass}">
                                                    <a class="page-link" href="courseManager?page=${i}">${i}</a>
                                                </li>
                                            </c:forEach>
                                        </c:when>
                                    </c:choose>
                                </ul><!--end pagination-->

                            </div>
                        </div>
                    </div>
                </div><!--end container-->

                <!-- Footer Start -->
                <%@include file="/view/common/footer.jsp"%>
                <!-- End -->
            </main>
            <!--End page-content" -->
        </div>
        <!-- page-wrapper -->

        <!-- java script -->
        <script src="../../../assets/js/bootstrap.bundle.min.js"></script>
        <!-- simplebar -->
        <script src="../../../assets/js/simplebar.min.js"></script>
        <!-- Icons -->
        <script src="../../../assets/js/feather.min.js"></script>
        <!-- Main Js -->
        <script src="../../../assets/js/app.js"></script>

        <!-- tham khao chat gpt -->

        <!--convert price to currency-->
        <script>
                                                                    // Lấy tất cả các ô trong bảng có class "priceCell"
                                                                    var priceCells = document.querySelectorAll('.priceCell');

                                                                    priceCells.forEach(cell => {
                                                                        const price = parseInt(cell.textContent);

                                                                        //neu price = 0 thi hien thi Free
                                                                        if (price === 0) {
                                                                            cell.textContent = 'Free';
                                                                        } else {
                                                                            const formattedPrice = price.toLocaleString('vi-VN') + ' VND';
                                                                            cell.textContent = formattedPrice;
                                                                        }
                                                                    });
        </script>


    </body>
</html>