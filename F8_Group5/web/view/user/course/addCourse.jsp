<%-- 
    Document   : addCourse
    Created on : Jun 10, 2023, 9:21:06 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="vi">

    <head>
        <meta charset="utf-8" />
        <title>F8 - Học lập trình để đi làm</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
        <meta name="keywords" content="Appointment, Booking, System, Dashboard, Health" />
        <meta name="author" content="Shreethemes" />
        <meta name="email" content="support@shreethemes.in" />
        <meta name="website" content="index.html" />
        <meta name="Version" content="v1.2.0" />
        <!-- F8logo -->
        <link rel="shortcut icon" href="assets/images/logoF8.png">
        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- simplebar -->
        <link href="assets/css/simplebar.css" rel="stylesheet" type="text/css" />
        <!-- Select2 -->
        <link href="assets/css/select2.min.css" rel="stylesheet" />
        <!-- Icons -->
        <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <!-- Css -->
        <link href="assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />
        <style>
            .preview-box {
                width: 100%; /* Set the width of the preview box */
                height: 100%; /* Let the height adjust based on the image */
                text-align: center; /* Center the image horizontally */
                overflow: hidden; /* Hide any overflowing parts of the image */
            }

        </style>

        <script>
            const handleChange = () => {
                const fileUploader = document.querySelector('#input-file');
                const getFile = fileUploader.files;
                if (getFile.length !== 0) {
                    const uploadedFile = getFile[0];
                    readFile(uploadedFile);
                }
            };

            const readFile = (uploadedFile) => {
                if (uploadedFile) {
                    const reader = new FileReader();
                    reader.onload = () => {
                        const parent = document.querySelector('.preview-box');
                        const img = document.createElement('img');
                        img.src = reader.result;
                        img.classList.add('preview-content');
                        img.onload = () => {
                            const parentWidth = parent.offsetWidth;
                            const imgWidth = img.width;
                            const imgHeight = img.height;

                            const scaleFactor = parentWidth / imgWidth;
                            const newWidth = imgWidth * scaleFactor;
                            const newHeight = imgHeight * scaleFactor;

                            img.style.width = `${newWidth}px`;
                            img.style.height = `${newHeight}px`;
                        };
                        parent.innerHTML = "";
                        parent.appendChild(img);
                    };
                    reader.readAsDataURL(uploadedFile);
                }
            };
        </script>
    </head>

    <body>
        <%@include file="/view/common/header-back.jsp"%>
        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div>
        <!-- Loader -->

        <div class="page-wrapper doctris-theme">
            <!-- Start Page Content -->
            <main class="page-content bg-light">
                <div class="container-fluid">
                    <div class="layout-specing">
                        <div class="d-md-flex justify-content-between">
                            <h4 style="color: orangered ; font-family: inherit">Thêm khóa học mới</h5>
                                <h7 class="mb-0">
                                    <a href="courseManager" style="color: darkblue">Quản lý danh sách khóa học</a>
                                </h7>
                        </div>

                        <div class="row">
                            <div class="offset-3 col-lg-6 mt-4">
                                <div class="card border-0 p-4 rounded shadow">
                                    <form action="addCourse" enctype="multipart/form-data" method="POST" class="mt-4">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p>${u.UserID}</p>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="mb-3">
                                                    <label class="form-label">Course Name</label>
                                                    <input name="Name" type="text" class="form-control" placeholder="Course Name" required="">
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="mb-3">
                                                    <label class="form-label">Course Image</label>
                                                    <div class="preview-box d-block justify-content-center rounded shadow overflow-hidden bg-light p-1"></div>
                                                    <input type="file" id="input-file" name="image" accept="image/*" onchange="handleChange()" hidden />
                                                    <label class="btn-upload btn btn-primary mt-4" for="input-file">Upload Image</label>
                                                </div>
                                            </div><!--end col-->

                                            <div class="col-md-12">
                                                <div class="mb-3">
                                                    <label class="form-label">Course Information</label>
                                                    <textarea name="CourseInfo" rows="4" class="form-control" placeholder="Course Information" required=""></textarea>
                                                </div>
                                            </div><!--end col-->

                                            <div class="col-md-12">
                                                <div class="mb-3">
                                                    <label class="form-label">Description</label>
                                                    <textarea name="Description" rows="4" class="form-control" placeholder="Description" required=""></textarea>
                                                </div>
                                            </div><!--end col-->

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="form-label">Public Status</label>
                                                    <select name="Status" class="form-control">
                                                        <option style="color: green;" value="1" >Show</option>
                                                        <option style="color: red;" value="0" >Hide</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="form-label">Price Status</label>
                                                    <select id="price-type" name="price-type" class="form-control">
                                                        <option value="free">Free</option>
                                                        <option value="pro">Pro</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="form-label">Category</label>
                                                    <select name="Category_CategoryID" class="form-control" required="">
                                                        <c:forEach items="${listCategory}" var="listCat">
                                                            <option value="${listCat.getCategoryID()}">${listCat.getName()}</option>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                            </div><!--end col-->

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="form-label">Price</label>
                                                    <div id="price-container">
                                                        <input type="number" id="Price" name="Price" class="form-control" disabled="" value="0"  required="">
                                                        <span id="price-error" style="color: red;"></span>
                                                    </div>
                                                </div>
                                                <c:forEach items="${listUser}" var="user">
                                                    <c:if test="${user.getUserID() == admin.getUserID()}">
                                                        <input type="text" name="User_UserID" hidden value="${admin.getUserID()}" placeholder="Admin Created" class="form-control" readonly="">
                                                    </c:if>
                                                </c:forEach>    
                                            </div><!--end col-->
                                            <p style="color: green">${messSuccess}</p> 
                                            <p style="color: red">${mess}</p>
                                            <button type="submit" class="btn btn-primary">Thêm khóa học mới</button>
                                        </div>
                                    </form>
                                </div><!--end col-->
                            </div><!--end row-->
                        </div>
                    </div><!--end container-->

                    <!-- Footer Start -->
                    <%@include file="/view/common/footer.jsp"%>
                    <!-- End -->
                    <!-- End -->
            </main>
            <!--End page-content" -->
        </div>
        <!-- page-wrapper -->
        <!-- javascript -->
        <script>
            const priceTypeSelect = document.getElementById('price-type');
            const priceInput = document.getElementById('Price');
            const priceError = document.getElementById('price-error');


            priceTypeSelect.addEventListener('change', function () {
                if (priceTypeSelect.value === 'free') {
                    priceInput.disabled = true;
                    priceInput.value = 0; // Clear the value if it was entered previously
                } else {
                    priceInput.value = 1;
                    priceInput.disabled = false;
                }
            });

            priceInput.addEventListener('input', function () {

                if (priceTypeSelect.value === 'pro') {

                    const inputText = priceInput.value.replace(/[^0-9]/g, ''); // Remove non-numeric characters
                    priceInput.value = inputText; // Update input value

                    const priceValue = parseFloat(priceInput.value);
                    if (priceValue < 0) {
                        priceError.textContent = 'Price must be greater than 0';
                        priceInput.value = 1;
                    } else if (priceValue > 10000000) {
                        priceError.textContent = 'Price must be smaller than 10000000';
                        priceInput.value = 9999999;
                    } else if (priceValue === 0) {
                        priceTypeSelect.value = 'free';
                        priceInput.disabled = true;
                        priceInput.value = 0;
                    } else {
                        priceError.textContent = '';
                    }
                }
            });
        </script>
        <script src="../../../assets/js/jquery.min.js"></script>
        <script src="../../../assets/js/bootstrap.bundle.min.js"></script>
        <!-- simplebar -->
        <script src="../../../assets/js/simplebar.min.js"></script>
        <!-- Select2 -->
        <script src="../../../assets/js/select2.min.js"></script>
        <script src="../../../assets/js/select2.init.js"></script>
        <!-- Icons -->
        <script src="../../../assets/js/feather.min.js"></script>
        <!-- Main Js -->
        <script src="../../../assets/js/app.js"></script>

    </body>

</html>
