<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="vi">
    <head>
        <meta charset="UTF-8">
        <title>F8 - Học Lập Trình Để Đi Làm | F8 trên Youtube | F8 trên Facebook</title>
        <!-- F8logo -->
        <!-- favicon -->
        <link rel="shortcut icon" href="assets/images/logoF8.png">
        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <!-- Css -->
        <link href="assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />
        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- SLIDER -->
        <link rel="stylesheet" href="assets/css/tiny-slider.css"/>
        <!-- Icons -->
        <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
    </head>
    <body>
        <%@include file="/view/common/header-back.jsp"%>
        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div>
        <!-- End Loader -->

        <!-- start section -->

        <section class="bg-dashboard">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-1 col-lg-1 col-md-1">
                        <div class="rounded overflow-hidden sticky-bar">
                            <%@include file="/view/common/navbar.jsp"%>
                        </div>
                    </div>
                    <div class="col-xs-7 col-lg-7 col-md-7">
                        <div class="row">
                            <section>
                                <h2>${c.getName()}</h2>
                                <p class="text-muted mt-2s">${c.getDescription()}</p>
                            </section>
                        </div>
                    </div>
                    <div class="col-xs-4 col-lg-4 col-md-4">
                        <c:choose>
                            <c:when test="${c.getPrice() == 0}">
                                <form action="enrollCourse" method="post">
                                    <img src="./assets/images/course/${c.getImage()}" width="400px" height="250px" alt="${c.getName()}"/>
                                    <p style="color: orangered; font-size: 20px; margin-top: 10px; font-weight: 500" class="price amount">${c.getPrice()}</p>
                                    <input type="hidden" name="CourseID" value="${c.getCourseID()}" />
                                    <input type="submit" value="Đăng kí ngay" class="btn btn-muted rounded-pill" />
                                </form>
                            </c:when>
                            <c:otherwise>
                                <form action="payment" method="POST">
                                    <img src="./assets/images/course/${c.getImage()}" width="400px" height="250px" alt="${c.getName()}"/>
                                    <p style="color: orangered; font-size: 20px; margin-top: 10px; font-weight: 500" class="price amount">${c.getPrice()}</p>
                                    <input type="hidden" name="CourseID" value="${c.getCourseID()}" />
                                    <input type="submit" value="Mua ngay" class="btn btn-muted rounded-pill" />
                                </form>
                            </c:otherwise>
                        </c:choose>
                        <input type="text" value="${UserID}" hidden/>

                    </div>
                </div>
            </div>
        </section>
        <script>
// Lấy tất cả các ô trong bảng có class "price"
            var priceValue = document.querySelectorAll('.price');

            priceValue.forEach(cell => {
                const price = parseInt(cell.textContent);

//neu price = 0 thi hien thi Free
                if (price === 0) {
                    cell.textContent = 'Free';
                } else {
                    const formattedPrice = price.toLocaleString('vi-VN') + ' VND';
                    cell.textContent = formattedPrice;
                }
            });
        </script>
    </body>
</html>
