
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>F8 - Học lập trình để đi làm</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
        <meta name="keywords" content="Appointment, Booking, System, Dashboard, Health" />
        <meta name="author" content="Shreethemes" />
        <meta name="email" content="support@shreethemes.in" />
        <meta name="website" content="index.html" />
        <meta name="Version" content="v1.2.0" />
        <!-- F8logo -->
        <link rel="shortcut icon" href="assets/images/logoF8.png">
        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- simplebar -->
        <link href="assets/css/simplebar.css" rel="stylesheet" type="text/css" />
        <!-- Select2 -->
        <link href="assets/css/select2.min.css" rel="stylesheet" />
        <!-- Icons -->
        <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <!-- Css -->
        <link href="assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />

        <style>
            .list-inline-item:not(:last-child){
                margin-right:.5rem;
                margin-left: 280px;
            }

            .subcribe-form input{
                padding:12px 20px;
                width:470px;
                color:#343a40 !important;
                border:none;
                outline:none !important;
                padding-right:160px;
                padding-left:20px;
                background-color:rgba(255,255,255,0.8)
            }
        </style>
    </head>
    <body>
        <%@include file="/view/common/header-back.jsp"%>
        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div>
        <!-- Loader -->

        <div class="page-wrapper doctris-theme">
            <!-- Start Page Content -->
            <main class="page-content bg-light">
                <div class="container-fluid">
                    <div class="layout-specing">
                        <div class="d-md-flex justify-content-between">
                            <h4 style="color: orangered; font-family: inherit;">Thêm bài học</h4>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 mt-4">
                                <div class="card border-0 p-4 rounded shadow">
                                    <form action="addlesson" method="POST" enctype="multipart/form-data" class="mt-4">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="mb-3">
                                                    <label class="form-label">Lesson Name</label>
                                                    <input name="Name" type="text" class="form-control" placeholder="Tên bài học" required>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="mb-3">
                                                    <label class="form-label">Lesson Video</label>
                                                    <input id="videoInput" name="Video" type="file" accept="video/mp4">
                                                </div>
                                            </div><!--end col-->
                                            <div class="col-md-8">
                                                <div id="VideoPreview"></div>
                                                <div><p id="fileSizeText"></p></div>
                                            </div>
                                            <div class="col-md-12 mt-3">
                                                <div class="mb-3">
                                                    <label class="form-label">Course Name</label>
                                                    <select name="CourseID" class="form-select">
                                                        <c:forEach items="${course.getAllCourse()}" var="c">
                                                            <option value="${c.getCourseID()}">${c.getName()}</option>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-12 mt-3">
                                                <button type="submit" class="btn btn-primary">Thêm bài học</button>
                                            </div>
                                        </div>
                                    </form>
                                </div><!--end card-->
                            </div><!--end col-->
                        </div><!--end row-->
                    </div><!--end layout-specing-->


                    <!-- Footer Start -->
                    <%@include file="/view/common/footer.jsp"%>
                    <!-- End -->
                    <!-- End -->
            </main>
            <!--End page-content" -->
        </div>


        <!-- page-wrapper -->
        <!-- javascript -->
        <script src="../../../assets/js/jquery.min.js"></script>
        <script src="../../../assets/js/bootstrap.bundle.min.js"></script>
        <!-- simplebar -->
        <script src="../../../assets/js/simplebar.min.js"></script>
        <!-- Select2 -->
        <script src="../../../assets/js/select2.min.js"></script>
        <script src="../../../assets/js/select2.init.js"></script>
        <!-- Icons -->
        <script src="../../../assets/js/feather.min.js"></script>
        <!-- Main Js -->
        <script src="../../../assets/js/app.js"></script>
        <script>
            document.getElementById('videoInput').addEventListener('change', function (event) {
                const videoPreview = document.getElementById('VideoPreview');
                const fileSizeText = document.getElementById('fileSizeText'); // Element to display file size
                videoPreview.innerHTML = ''; // Clear previous video preview, if any

                const file = event.target.files[0];
                if (file) {
                    if (file.size <= 1000000000) { // Limit file size to 10 MB
                        const video = document.createElement('video');
                        video.src = URL.createObjectURL(file);
                        video.controls = true; // Display video controls for playback
                        video.style.maxWidth = '100%'; // Set maximum width for the video
                        videoPreview.appendChild(video);
                        // Display file size in bytes
                        const fileSize = file.size;
                        fileSizeText.textContent = 'Video File Size: ' + fileSize + ' bytes';
                        fileSizeText.style.color = 'black'; // Reset color to default

                    } else {
                        // Display error message if the file size is too large
                        fileSizeText.textContent = 'Error: File size exceeds 10,000,000 bytes (10 MB).';
                        fileSizeText.style.color = 'red';
                    }
                }
            });

        </script>
    </body>

</html>

</body>

</html>
