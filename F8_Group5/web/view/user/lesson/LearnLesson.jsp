<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Bài học của tôi</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
        <meta name="keywords" content="Appointment, Booking, System, Dashboard, Health" />
        <meta name="author" content="Shreethemes" />
        <meta name="email" content="support@shreethemes.in" />
        <meta name="website" content="https://shreethemes.in" />
        <meta name="Version" content="v1.2.0" />
        <!-- f8logo -->
        <link rel="shortcut icon" href="assets/images/logoF8.png">
        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <!-- Css -->
        <link href="assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" /><!-- Your head content here -->
    </head>
    <body>
        <style>
            .fixed-container {
                position: relative;
            }

            .video-lesson-container {
                position: absolute;
                top: 0;
                left: 0;
                width: 80%;
                height: 200%;
            }

            .lesson-list {
                position: absolute;
                top: 0;
                right: 0;
                width: 20%;
                height: 100%;
                background-color: #f1f1f1;
                padding: 10px;
            }
            .video-container video {
                border: 1px solid #ccc;
                border-radius: 5px;
                box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.2);
            }

            .lesson-list h4 {
                margin-top: 0;
                margin-bottom: 10px;
            }

            .lesson-list table {
                width: 100%;
                border-collapse: collapse;
            }

            .lesson-list a {
                display: block;
                padding: 5px 0;
                color: #333;
                text-decoration: none;
                transition: color 0.3s ease-in-out;
            }

            .lesson-list a:hover {
                color: orangered;
            }
        </style>
        <%@include file="/view/common/header-back.jsp"%>
        <div class="page-wrapper doctris-theme">
            <!-- Start Page Content -->
            <main class="page-content bg-light">
                <div class="container-fluid">
                    <div class="layout-specing">
                        <div class="fixed-container">
                            <div class="video-lesson-container">
                                <div class="video-container" id="videoContainer" style="display: none; text-align: center;">
                                    <iframe id="videoFrame" width="100%" height="400px" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="text-container" id="textContainer" style="text-align: center; padding: 10px; border: 1px solid #ccc; background-color: #f5f5f5;">
                                    <iframe id="textFrame" width="100%" height="400px" frameborder="0"></iframe>
                                </div>
                            </div>
                            <div class="lesson-list">
                                <h4 style="color: orangered; font-family: inherit">Bài Học</h4>
                                <table>
                                    <c:forEach items="${lessonlist}" var="lesson">
                                        <tr>
                                            <td>
                                                <a href="#" onclick="showContent('${lesson.getVideo()}', '${lesson.getLessonID()}')">
                                                    ${lesson.getName()}
                                                </a>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </div>
                        </div>

                        <script>
                            function showContent(content, lessonid) {
                                var textFrame = document.getElementById('textFrame');
                                var videoContainer = document.getElementById('videoContainer');
                                var textContainer = document.getElementById('textContainer');
                                var videoFrame = document.getElementById('videoFrame');

                                if (content.includes("youtube.com")) {
                                    // Display video
                                    var videoId = extractVideoId(content);
                                    var youtubeEmbedUrl = "https://www.youtube.com/embed/" + videoId;
                                    videoFrame.src = youtubeEmbedUrl;
                                    videoContainer.style.display = 'block';
                                    textContainer.style.display = 'none';
                                } else {
                                    // Display text content
                                    textFrame.src = 'ReadTextFile?LessonID=' + lessonid;
                                    textFrame.style.display = 'block';
                                    textContainer.style.display = 'block';
                                    videoContainer.style.display = 'none';
                                }
                            }

                            // Function to extract video ID from YouTube link
                            function extractVideoId(videoLink) {
                                var match = videoLink.match(/v=([^&]+)/);
                                if (match && match[1]) {
                                    return match[1];
                                }
                                return null;
                            }
                        </script>



                    </div>
                </div>
                <!--end container-->
                <!-- Footer Start -->
                <%@include file="/view/common/footer.jsp"%>
                <!-- End -->
            </main>
            <!--End page-content" -->
        </div>


        <!-- page-wrapper -->

        <!-- java script -->
        <script src="../../../assets/js/bootstrap.bundle.min.js"></script>
        <!-- simplebar -->
        <script src="../../../assets/js/simplebar.min.js"></script>
        <!-- Icons -->
        <script src="../../../assets/js/feather.min.js"></script>
        <!-- Main Js -->
        <script src="../../../assets/js/app.js"></script>
    </body>
</html>
