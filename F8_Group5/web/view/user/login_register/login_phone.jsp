<%-- 
    Document   : verify
    Created on : May 26, 2023, 1:28:25 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri= "http://java.sun.com/jsp/jstl/core" prefix = "c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Đăng nhập vào F8 - Học lập trình để đi làm</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
        <meta name="keywords" content="Appointment, Booking, System, Dashboard, Health" />
        <meta name="author" content="Shreethemes" />
        <meta name="email" content="support@shreethemes.in" />
        <meta name="website" content="https://shreethemes.in" />
        <meta name="Version" content="v1.2.0" />
        <!-- favicon -->
        <link rel="shortcut icon" href="assets/images/logoF8.png">
        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <!-- Css -->
        <link href="assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />
        <style>
            #number,
            #verificationcode {

            }

            #recaptcha-container {
                margin-bottom: 10px;
                transform:scale(1);
                transform-origin:0 0;

            }

            #send,
            #verify {
                width: 100%;
                height: 48px;
                outline: none;
            }

            .error-message {
                display: none;
                color: red;
                font-size: 14px;
                margin-top: auto;
                margin-bottom: 10px;
                text-align: center;
            }

        </style>
    <body>

        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div>
        <!-- Loader -->

        <div class="back-to-home rounded d-none d-sm-block">
            <a href="home" class="btn btn-icon btn-primary"><i data-feather="home" class="icons"></i></a>
        </div>

        <!-- Hero Start -->
        <section class="bg-home d-flex bg-light align-items-center" style="background: url('assets/images/bg/f8_bg.png') center;">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-5 col-md-8">

                        <div class="card login-page bg-white shadow mt-4 rounded border-0">
                            <div class="card-body">
                                <img style="border-radius: 25%;" src="assets/images/logoF8.png" height="50" class="mx-auto d-block" alt="">
                                <h4 class="text-center">Đăng nhập vào F8</h4>  
                                <form action="login-phone" method="post" class="login-form mt-4">
                                    <div class="row">

                                        <div class="col-lg-12">
                                            <div class="d-flex justify-content-between">
                                                <div class="mb-3">
                                                    <label class="form-label mb-0">Số điện thoại <span class="text-danger">*</span></label> 
                                                </div>
                                                <a href="login" class="text-dark h6 mb-0">Đăng nhập với Email</a>
                                            </div>
                                        </div>
                                        <div class="col-lg-8" style="padding-right: 0px">
                                            <div class="mb-3">
                                                <input style="height:40.1px" id="number" type="text" class="form-control" placeholder="Số điện thoại" 
                                                       name="phone" ${success == 'Success' ? 'readonly':''} 
                                                       autofocus="" value="${phone}">
                                            </div>
                                        </div>

                                        <div class="col-lg-4 mb-0" style="padding-left: 0px">
                                            <div class="d-grid">
                                                <button type="submit" class="btn btn-warning">Kiểm tra</button>
                                            </div>
                                        </div>

                                        <div class="col-lg-8" style="padding-right: 0px">
                                            <div class="mb-3" style="display: flex">
                                                <input style="height:40.1px" id="verificationcode" type="text" 
                                                       class="form-control" placeholder="Nhập mã xác nhận">
                                            </div>
                                        </div>

                                        <div class="col-lg-4 mb-0" style="padding-left: 0px">
                                            <div class="d-grid">
                                                <input type="button" class="btn btn-danger" value="Gửi mã" ${success != 'Success' ? 'disabled':''} onClick="phoneAuth()">
                                            </div>
                                        </div>


                                        <div style="display: none" id="recaptcha-container"></div>

                                        <c:if test="${fail != null || failempty != null || failformat != null}">
                                            <div class="col-lg-12">
                                                <div class="mb-3 text-center">
                                                    <span style="color:red; ">${fail}</span>
                                                    <span style="color:red; ">${failempty}</span>
                                                    <span style="color:red; ">${failformat}</span>
                                                </div>
                                            </div>
                                        </c:if>

                                        <div class="col-lg-12 error-message">Mã xác nhận không hợp lệ. Vui lòng thử lại</div>

                                        <div class="col-lg-12 mb-0">
                                            <div class="d-grid">
                                                <input type="button" class="btn btn-primary" ${success != 'Success' ? 'disabled':''} 
                                                       value="Đăng Nhập" onClick="codeverify()">
                                            </div>
                                        </div>      

                                        <div class="col-lg-12 mt-3 text-center">
                                            <h6 class="text-muted">Hoặc</h6>
                                        </div><!--end col-->

                                        <div class="col-6 mt-3">
                                            <div class="d-grid">
                                                <a href="https://www.facebook.com/dialog/oauth?client_id=846580123743218&redirect_uri=http://localhost:9999/F8_Group6/login-facebook" 
                                                   class="btn btn-soft-primary"><i class="uil uil-facebook"></i> Facebook</a>
                                            </div>
                                        </div><!--end col-->

                                        <div class="col-6 mt-3">
                                            <div class="d-grid">
                                                <a href="https://accounts.google.com/o/oauth2/auth?scope=email%20profile&&redirect_uri=http://localhost:9999/F8_Group6/loginwithgoogle&response_type=code&client_id=474696467188-912h3an6csb4sen30chpstfou05t4o77.apps.googleusercontent.com&approval_prompt=force" 
                                                   class="btn btn-soft-primary"><i class="uil uil-google"></i> Google</a>
                                            </div>
                                        </div><!--end col-->


                                        <div class="col-lg-12 mt-3 text-center">
                                            <h6 class="d-grid"><a href="https://github.com/login/oauth/authorize?client_id=7546e46ad9badbdb19fe&redirect_uri=http://localhost:9999/F8_Group6/login-github" 
                                                            class="btn btn-soft-primary"
                                                            style="width: 189.5px; margin: 0 auto;"><i class="uil uil-github"></i> Github</a></h6>
                                        </div><!--end col-->               


                                        <div class="col-12 text-center">
                                            <p class="mb-0 mt-3"><small class="text-dark me-2">Bạn chưa có tài khoản ?</small> <a href="register-phone" class="text-dark fw-bold">Đăng Ký</a></p>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div><!---->
                    </div> <!--end col-->
                </div><!--end row-->
            </div> <!--end container-->
        </section><!--end section-->
        <!-- Hero End -->
        <script src="https://www.gstatic.com/firebasejs/9.12.1/firebase-app-compat.js"></script>
        <script src="https://www.gstatic.com/firebasejs/9.12.1/firebase-auth-compat.js"></script>
        <script>
                                                           // For Firebase JS SDK v7.20.0 and later, measurementId is optional
                                                           const firebaseConfig = {
                                                               apiKey: "AIzaSyBzAuMGn2_37BRbH8lyoyinmjRZk_Qd_OM",
                                                               authDomain: "f8-group-6a17b.firebaseapp.com",
                                                               projectId: "f8-group-6a17b",
                                                               storageBucket: "f8-group-6a17b.appspot.com",
                                                               messagingSenderId: "79227539105",
                                                               appId: "1:79227539105:web:77ff03234cc602c4aeaae0",
                                                               measurementId: "G-01257MMZKN"
                                                           };
                                                           firebase.initializeApp(firebaseConfig);
                                                           render();
                                                           function render() {
                                                               window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
                                                               recaptchaVerifier.render();
                                                           }

                                                           // function for send message
                                                           function phoneAuth() {
                                                               var number = document.getElementById('number').value;
                                                               number = convertPhone(number);
                                                               document.getElementById('recaptcha-container').style.display = 'block';
                                                               firebase.auth().signInWithPhoneNumber(number, window.recaptchaVerifier).then(function (confirmationResult) {
                                                                   window.confirmationResult = confirmationResult;
                                                                   coderesult = confirmationResult;

                                                               }).catch(function (error) {
                                                                   alert(error.message);
                                                               });
                                                           }
                                                           function convertPhone(phone) {
                                                               if (!phone.startsWith("+84")) {
                                                                   phone = phone.replace('0', "+84");
                                                                   console.log(phone + "new ");
                                                               }
                                                               return phone;
                                                           }
                                                           // function for code verify
                                                           function codeverify() {
                                                               var code = document.getElementById('verificationcode').value;
                                                               coderesult.confirm(code).then(function () {
                                                                   window.location.href = "succesful-phone?phone=${phone}";
                                                                   document.getElementsByClassName('error-message')[0].style.display = 'none';
                                                               }).catch(function () {

                                                                   document.getElementsByClassName('error-message')[0].style.display = 'block';
                                                               })
                                                           }
        </script>
        <!-- javascript -->
        <script src="assets/js/bootstrap.bundle.min.js"></script>
        <!-- Icons -->
        <script src="assets/js/feather.min.js"></script>
        <!-- Main Js -->
        <script src="assets/js/app.js"></script>

    </body>
</html>
