<%-- 
    Document   : courseManagement
    Created on : Jun 10, 2023, 8:49:26 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="vi">

    <head>
        <meta charset="utf-8" />
        <title>Quản lý lịch sử hóa đơn</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
        <meta name="keywords" content="Appointment, Booking, System, Dashboard, Health" />
        <meta name="author" content="Shreethemes" />
        <meta name="email" content="support@shreethemes.in" />
        <meta name="website" content="index.html" />
        <meta name="Version" content="v1.2.0" />
        <!-- favicon -->
        <link rel="shortcut icon" href="assets/images/logoF8.png">
        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- simplebar -->
        <link href="assets/css/simplebar.css" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <!-- Css -->
        <link href="assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />


        <script>
            function openBlankTab() {
                var url = "invoiceManager/exportExcel";

                window.open(url, ".blank");
            }
        </script>

    </head>

    <body>
        <%@include file="/view/common/header-back.jsp"%>
        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div>
        <!-- Loader -->

        <div class="page-wrapper doctris-theme">
            <!-- Start Page Content -->
            <main class="page-content bg-light">
                <div class="container-fluid">
                    <div class="layout-specing">
                        <div class="row">
                            <div class="d-md-flex justify-content-between">
                                <h4 style="color: orangered; font-family: inherit">Quản lý lịch sử hóa đơn</h4>
                                <c:if test="${not empty listInvoice}">
                                    <button class="btn btn-primary" onclick="openBlankTab()">Export to Excel</button>
                                </c:if>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 mt-4">
                                <div class="table-responsive shadow rounded">
                                    <table class="table table-center bg-white mb-0">
                                        <thead>
                                            <tr>
                                                <th scope="col" class="border-bottom text-center" style="width: 100px;">Invoice ID</th>   
                                                <th scope="col" class="border-bottom text-center" style="width: 200px;">User Name</th>
                                                <th scope="col" class="border-bottom text-center" style="width: 200px;">Invoice Amount</th>
                                                <th scope="col" class="border-bottom text-center" style="width: 200px;">Purchase Date</th>
                                                <th scope="col" class="border-bottom text-center" style="width: 100px;">Status</th>
                                                <th scope="col" class="border-bottom text-center" style="width: 400px;">Description</th>
                                                <th scope="col" class="border-bottom text-center" style="width: 300px;">Course Name</th>
                                            </tr>
                                        </thead>    
                                        <tbody>
                                            <c:forEach items="${listInvoice}" var="i">
                                                <tr>
                                                    <td class="text-center">${i.getInvoice_ID()}</td>
                                                    <td class="text-center">
                                                        <c:forEach items="${listUser}" var="u">
                                                            <c:if test="${u.getUserID() == i.getUser_UserID()}">
                                                                ${u.getFullName()}
                                                            </c:if>
                                                        </c:forEach>
                                                    </td>
                                                    <td class="text-center priceCell">${i.getPrice()}</td>
                                                    <td class="text-center">${i.getCreateDate()}</td>
                                                    <th class="text-center">
                                                        <c:if test="${i.getStatus() == 1}">
                                                            <span style="color: green">Success</span>
                                                        </c:if>
                                                        </td>
                                                    <td class="text-center">${i.getDescription()}</td>
                                                    <td class="text-center">
                                                        <c:forEach items="${listCourse}" var="c">
                                                            <c:if test="${c.getCourseID() == i.getCourse_CourseID()}">
                                                                ${c.getName()}
                                                            </c:if>
                                                        </c:forEach>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="mt-4">
                                <ul class="pagination justify-content-center mb-0 list-unstyled">
                                    <c:choose>
                                        <c:when test="${totalPage > 1}">
                                            <c:forEach begin="1" end="${totalPage}" var="i">
                                                <c:set var="activeClass" value="${(i == currentPage) ? 'active' : ''}" />
                                                <li class="page-item ${activeClass}">
                                                    <a class="page-link" href="invoiceManager?page=${i}">${i}</a>
                                                </li>
                                            </c:forEach>
                                        </c:when>
                                    </c:choose>
                                </ul><!--end pagination-->
                            </div>
                        </div>
                    </div>
                </div><!--end container-->

                <!-- Footer Start -->
                <%@include file="/view/common/footer.jsp"%>
                <!-- End -->
            </main>
            <!--End page-content" -->
        </div>
        <!-- page-wrapper -->

        <!-- java script -->
        <script src="../../../assets/js/bootstrap.bundle.min.js"></script>
        <!-- simplebar -->
        <script src="../../../assets/js/simplebar.min.js"></script>
        <!-- Icons -->
        <script src="../../../assets/js/feather.min.js"></script>
        <!-- Main Js -->
        <script src="../../../assets/js/app.js"></script>

        <!-- tham khao chat gpt -->

        <!--convert price to currency-->
        <script>
                                        // Lấy tất cả các ô trong bảng có class "priceCell"
                                        var priceCells = document.querySelectorAll('.priceCell');

                                        priceCells.forEach(cell => {
                                            const price = parseInt(cell.textContent);

                                            //neu price = 0 thi hien thi Free
                                            if (price === 0) {
                                                cell.textContent = 'Free';
                                            } else {
                                                const formattedPrice = price.toLocaleString('vi-VN') + ' VND';
                                                cell.textContent = formattedPrice;
                                            }
                                        });
        </script>


    </body>
</html>