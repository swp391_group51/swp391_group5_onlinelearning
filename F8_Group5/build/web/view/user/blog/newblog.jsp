<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8" />
        <!-- favicon -->
        <link rel="shortcut icon" href="../../../assets/images/favicon.ico.png">
        <!-- Bootstrap -->
        <link href="../../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Select2 -->
        <link href="../../../assets/css/select2.min.css" rel="stylesheet" />
        <!-- Date picker -->
        <link rel="stylesheet" href="../../../assets/css/flatpickr.min.css">
        <link href="../../../assets/css/jquery.timepicker.min.css" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="../../../assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="../../../assets/css/remixicon.css" rel="stylesheet" type="text/css" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <!-- Css -->
        <link href="../../../assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />

    </head>

    <body>
        <%@include file="/view/common/header-back.jsp"%>


        <!--         Start Hero 
                <section class="bg-half-170 d-table w-100 bg-light">
                    <div class="container">
                        <div class="row mt-5 justify-content-center">
                            <div class="col-12">
                                <div class="section-title text-center">
                                    <h3 class="sub-title mb-4">Viết Blog</h3>
                                    <nav aria-label="breadcrumb" class="d-inline-block mt-3">
                                        <ul class="breadcrumb bg-transparent mb-0 py-1">
                                            <li class="breadcrumb-item"><a href="home">Trang chủ</a></li>
        
                                        </ul>
                                    </nav>
                                </div>
                            </div>end col
                        </div>end row
                    </div>end container
                </section>end section
                 Start Hero -->
        <section class="bg-half-170 d-table w-100" style="background: url('assets/images/bg/blogbg.jpg') center center;">
            <div class="bg-overlay bg-overlay-dark"></div>
            <div class="container">
                <div class="row mt-5 justify-content-center">
                    <div class="col-12">
                        <div class="section-title text-center">
                            <h1 class="sub-title mb-4 text-white title-dark">BLOG CỦA TÔI</h1>


                            <nav aria-label="breadcrumb" class="d-inline-block mt-3">
                                <ul class="breadcrumb bg-light rounded mb-0 py-1 px-2">
                                    <li class="breadcrumb-item"><a href="home">Trang chủ</a></li>

                                </ul>
                            </nav>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </section><!--end section-->
        <!-- End Hero -->
        <div class="position-relative">
            <div class="shape overflow-hidden text-white">
                <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
                </svg>
            </div>
        </div>
        <!-- End Hero -->
        <style>
            .preview-box {
                width: 100%; /* Set the width of the preview box */
                height: auto; /* Let the height adjust based on the image */
                text-align: center; /* Center the image horizontally */
                overflow: hidden; /* Hide any overflowing parts of the image */
            }

            .preview-content {
                max-width: 100%; /* Set a maximum width to prevent the image from exceeding the box width */
                max-height: 100%; /* Set a maximum height to prevent the image from exceeding the box height */
            }
        </style>
        <!-- Start -->
        <section class="section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="card border-0 shadow rounded overflow-hidden">
                            <ul class="nav nav-pills nav-justified flex-column flex-sm-row rounded-0 shadow overflow-hidden bg-light mb-0" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link rounded-0 active" id="clinic-booking" data-bs-toggle="pill"  role="tab" aria-controls="pills-clinic" aria-selected="false">
                                        <div class="text-center pt-1 pb-1">
                                            <h4 class="title fw-normal mb-0">Nội dung Blog</h4>
                                        </div>
                                    </a><!--end nav link-->
                                </li><!--end nav item-->


                            </ul>

                            <form action="blog" enctype="multipart/form-data" method="post">
                                <div class="tab-content p-4" id="pills-tabContent">
                                    <div class="modal-body p-3 pt-4">
                                        <div class="row">
                                            <c:if test="${Blog != null}">
                                                <img width="300px" height="400px" src="assets/images/blog/${Blog.getBlogImage()}">
                                            </c:if>

                                            <div class="col-md-4">
                                                <div class="d-grid">
                                                    <p class="text-muted">Tải ảnh blog của bạn ở đây, hãy nhấn vào nút "Tải ảnh lên"</p>
                                                    <div class="preview-box d-block justify-content-center rounded shadow overflow-hidden bg-light p-1"></div>
                                                    <input type="file" id="input-file" name="image" accept="image/*" onchange="handleChange()" hidden />
                                                    <label class="btn-upload btn btn-primary mt-4" for="input-file">Tải ảnh lên</label>
                                                </div>
                                            </div><!--end col-->

                                            <div class="col-md-8 mt-4 mt-sm-0">
                                                <div class="ms-md-4">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="mb-3">
                                                                <label class="form-label">Tiêu đề Blog <span class="text-danger">*</span></label>
                                                                <input name="title" id="title" type="text" class="form-control" placeholder="Tiêu đề :" value="${Blog.getBlogTitle()}">
                                                            </div>
                                                        </div><!--end col-->

                                                        <div class="col-md-6">
                                                            <div class="mb-3">
                                                                <label class="form-label"> Thời gian : </label>
                                                                <input name="date" type="date" class="form-control" id="date" value="${Blog.getPostDate()}" >
                                                            </div>
                                                        </div><!--end col-->

                                                        <div class="col-md-6">
                                                            <div class="mb-3">
                                                                <label class="form-label"> Thời gian đọc (phút) : </label>
                                                                <input name="time" type="number" class="form-control" id="time" value="${Blog.getTimeToRead()}" >
                                                            </div>
                                                        </div><!--end col-->
                                                        <div class="col-md-6">
                                                            <div class="mb-3">
                                                                <label class="form-label"> Topic : </label>
                                                                <select class="form-control" name="id_tag">
                                                                    <c:forEach var="t" items="${tags}">
                                                                        <option value="${t.getBlogTagID()}" ${Blog.getBlogTagID() == t.getBlogTagID() ? 'selected':''} >${t.getBlogTagName()}</option>
                                                                    </c:forEach>

                                                                </select>
                                                            </div>
                                                        </div><!--end col-->

                                                        <div class="col-md-6">
                                                            <div class="mb-3">
                                                                <label class="form-label"> Tag : </label>
                                                                <select class="form-control" name="id_topic">
                                                                    <c:forEach var="t" items="${topics}">
                                                                        <option value="${t.getBlogTopicID()}" ${Blog.getBlogTopicID() == t.getBlogTopicID() ? 'selected':''}>${t.getBlogTopicName()}</option>
                                                                    </c:forEach>

                                                                </select>
                                                            </div>
                                                        </div><!--end col-->

                                                        <div class="col-lg-12">
                                                            <div class="mb-3">
                                                                <label class="form-label">Nội dung <span class="text-danger">*</span></label>
                                                                <textarea name="detail" id="detail" rows="4" class="form-control" placeholder="Nội dung Blog :" >${Blog.getBlogDetail()}</textarea>
                                                            </div>
                                                        </div><!--end col-->

                                                        <div class="col-lg-12 text-end">
                                                            <c:if test="${Blog != null}">
                                                                <input type="hidden" name="blogid" value="${Blog.getBlogID()}">
                                                            </c:if>
                                                            


                                                            <button type="submit" class="btn btn-primary" name="action" value="save">Lưu</button>
                                                            <button type="submit" class="btn btn-primary" name="action" value="publish">Xuất bản</button>



                                                        </div><!--end col-->
                                                        <div class="text-center">
                                                            <div style="color: green">${updateMessage}</div>
                                                            <div style="color: green">${insertMessage}</div>
                                                            <div style="color: red">${errorUpload}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!--end col-->
                                        </div><!--end row-->
                                    </div>
                                </div>
                            </form>




                        </div><!--end col-->
                    </div><!--end row-->
                </div><!--end container-->
        </section><!--end section-->
        <!-- End -->
        <script>
            const handleChange = () => {
                const fileUploader = document.querySelector('#input-file');
                const getFile = fileUploader.files;
                if (getFile.length !== 0) {
                    const uploadedFile = getFile[0];
                    readFile(uploadedFile);
                }
            };

            const readFile = (uploadedFile) => {
                if (uploadedFile) {
                    const reader = new FileReader();
                    reader.onload = () => {
                        const parent = document.querySelector('.preview-box');
                        const img = document.createElement('img');
                        img.src = reader.result;
                        img.classList.add('preview-content');
                        img.onload = () => {
                            const parentWidth = parent.offsetWidth;
                            const imgWidth = img.width;
                            const imgHeight = img.height;

                            const scaleFactor = parentWidth / imgWidth;
                            const newWidth = imgWidth * scaleFactor;
                            const newHeight = imgHeight * scaleFactor;

                            img.style.width = `${newWidth}px`;
                            img.style.height = `${newHeight}px`;
                        };
                        parent.innerHTML = "";
                        parent.appendChild(img);
                    };
                    reader.readAsDataURL(uploadedFile);
                }
            };
        </script>



        <%@include file="/view/common/footer.jsp"%>

        <!-- javascript -->
        <script src="../../../assets/js/jquery.min.js"></script>
        <script src="../../../assets/js/bootstrap.bundle.min.js"></script>

        <script src="../../../assets/js/select2.min.js"></script>
        <script src="../../../assets/js/select2.init.js"></script>

        <script src="../../../assets/js/flatpickr.min.js"></script>
        <script src="../../../assets/js/flatpickr.init.js"></script>

        <script src="../../../assets/js/jquery.timepicker.min.js"></script> 
        <script src="../../../assets/js/timepicker.init.js"></script>

        <script src="../../../assets/js/feather.min.js"></script>

        <script src="../../../assets/js/app.js"></script>



    </body>
</html>
