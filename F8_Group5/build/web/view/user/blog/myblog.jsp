
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <!-- favicon -->
        <link rel="shortcut icon" href="../../../assets/images/favicon.ico.png">
        <!-- Bootstrap -->
        <link href="../../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="../../../assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="../../../assets/css/remixicon.css" rel="stylesheet" type="text/css" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <!-- Css -->
        <link href="../../../assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />
    </head>

    <body>


        <%@include file="/view/common/header-back.jsp"%>

        <!-- Start Hero -->
        <section class="bg-half-170 d-table w-100" style="background: url('assets/images/bg/blogbg.jpg') center center;">
            <div class="bg-overlay bg-overlay-dark"></div>
            <div class="container">
                <div class="row mt-5 justify-content-center">
                    <div class="col-12">
                        <div class="section-title text-center">
                            <h1 class="sub-title mb-4 text-white title-dark">BLOG CỦA TÔI</h1>


                            <nav aria-label="breadcrumb" class="d-inline-block mt-3">
                                <ul class="breadcrumb bg-light rounded mb-0 py-1 px-2">
                                    <li class="breadcrumb-item">
                                        <a class=" ${statusValue==0 ? 'active' : ''}" href="myblog?status=0" value="save">Bản nháp</a>
                                    </li>
                                    <li class="breadcrumb-item " >
                                        <a class=" ${statusValue==1 ? 'active' : ''}" href="myblog?status=1" value="publish">Đã xuất bản</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </section><!--end section-->
        <!-- End Hero -->

        <!-- Start -->
        <section class="section">
            <div class="container">
                <div class="row">
                    <c:forEach items="${bList}" var="o">
                        <div class="col-lg-4 col-md-6 col-12 mb-4 pb-2">
                            <div class="card blog blog-primary border-0 shadow rounded overflow-hidden">
                                <div >
                                    <img src="assets/images/blog/${o.getBlogImage()}" class="img-fluid" style="height:200px;width:400px;" alt="">
                                </div>

                                <div class="card-body p-4">
                                    <div style="float: right">
                                        <i class="uil uil-calendar-alt text-dark h6 me-1"></i>${o.getPostDate()}
                                    </div>
                                    <div style="float: left">
                                        <i class="uil uil-clock text-dark h6 me-1"></i>${o.getTimeToRead()} min read<br>

                                    </div>
                                    <div style="clear:both" class="mt-5">
                                        <a  href="blogview?id=${o.getBlogID()}" class="text-dark title h5">${o.getBlogTitle()}</a>
                                    </div>


                                </div>
                            </div>
                        </div><!--end col-->
                    </c:forEach>

                </div><!--end row-->


            </div><!--end container-->
        </section><!--end section-->
        <!-- End -->

        <%@include file="/view/common/footer.jsp"%>

        <!-- javascript -->
        <script src="../../../assets/js/bootstrap.bundle.min.js"></script>
        <!-- Icons -->
        <script src="../../../assets/js/feather.min.js"></script>
        <!-- Main Js -->
        <script src="../../../assets/js/app.js"></script>

    </body>

</html>