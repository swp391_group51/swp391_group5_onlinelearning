<%-- 
    Document   : updateCourse
    Created on : Jun 10, 2023, 9:57:23 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>Cập nhật thông tin khóa học</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
        <meta name="keywords" content="Appointment, Booking, System, Dashboard, Health" />
        <meta name="author" content="Shreethemes" />
        <meta name="email" content="support@shreethemes.in" />
        <meta name="website" content="index.html" />
        <meta name="Version" content="v1.2.0" />
        <!-- F8logo -->
        <link rel="shortcut icon" href="assets/images/logoF8.png">
        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- simplebar -->
        <link href="assets/css/simplebar.css" rel="stylesheet" type="text/css" />
        <!-- Select2 -->
        <link href="assets/css/select2.min.css" rel="stylesheet" />
        <!-- Icons -->
        <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <!-- Css -->
        <link href="assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />
        <style>
            .preview-box {
                width: 100%; /* Set the width of the preview box */
                height: auto; /* Let the height adjust based on the image */
                text-align: center; /* Center the image horizontally */
                overflow: hidden; /* Hide any overflowing parts of the image */
            }
        </style>
        <script>
            //not working
            var oldImagePreview = document.getElementById('old-image-preview');
            var newImagePreview = document.getElementById('new-image-preview');

            // Display the old image preview initially
            oldImagePreview.style.display = 'block';

            function handleChange() {
                var input = document.getElementById('input-file');

                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        newImagePreview.src = e.target.result;
                        newImagePreview.style.display = 'block';
                        oldImagePreview.style.display = 'none';
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }
        </script>
    </head>

    <body>
        <%@include file="/view/common/header-back.jsp"%>
        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div>
        <!-- Loader -->

        <div class="page-wrapper doctris-theme">
            <!-- Start Page Content -->
            <main class="page-content bg-light">
                <div class="container-fluid">
                    <div class="layout-specing">
                        <div class="d-md-flex justify-content-between">
                            <h4 style="color: orangered ; font-family: inherit">Cập nhật thông tin khóa học</h5>
                                <h7 class="mb-0">
                                    <a href="courseManager" style="color: darkblue">Quản lý danh sách khóa học</a>
                                </h7>
                        </div>
                        <div class="row">
                            <div class="offset-lg-3 col-lg-6 mt-4">
                                <div class="card border-0 p-4 rounded shadow">
                                    <form action="updateCourse" enctype="multipart/form-data"  method="POST" class="mt-4">
                                        <div class="row">


                                            <div class="col-md-12">
                                                <div class="mb-3">
                                                    <label class="form-label">Course Name</label>
                                                    <input type="text" name="name" value="${course.getName()}" class="form-control" placeholder="Course Name" required="">
                                                    <input type="text" name="id" value="${course.getCourseID()}" class="form-control" hidden="">
                                                </div>
                                            </div><!--end col-->

                                            <div class="col-md-12">
                                                <div class="mb-3">
                                                    <img src="assets/images/course/${course.getImage()}" id="old-image-preview" style="max-width: 100%; max-height: 200px;">
                                                    <div class="preview-box d-block justify-content-center rounded shadow overflow-hidden bg-light p-1">
                                                        <img id="new-image-preview" src="assets/images/course/${course.getImage()}" alt="Course Image Preview" style="max-width: 100%; max-height: 200px; display: none;">
                                                    </div>
                                                    <input type="file" id="input-file" name="image" accept="image/*" onchange="handleChange()" hidden />
                                                    <label class="btn-upload btn btn-primary mt-4" for="input-file">Upload Image</label>
                                                </div>
                                            </div><!--end col-->

                                            <div class="col-md-12">
                                                <div class="mb-3">
                                                    <label class="form-label">Course Information</label>
                                                    <textarea name="info" rows="4" class="form-control" placeholder="Course Information" required="">${course.getCourseInfo()}</textarea>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="mb-3">
                                                    <label class="form-label">Description</label>
                                                    <textarea name="des" rows="4" class="form-control" placeholder="Description" required="">${course.getDescription()}</textarea>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="form-label">Public Status</label>
                                                    <select name="Status" class="form-control">
                                                        <option style="color: green;" value="1" ${course.getStatus() == 1 ? 'selected' : ''}>Show</option>
                                                        <option style="color: red;" value="0" ${course.getStatus() == 0 ? 'selected' : ''}>Hide</option>
                                                    </select>
                                                </div>
                                            </div>



                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="form-label">Price Status</label>
                                                    <select id="price-type" name="price-type" class="form-control">
                                                        <option value="free" ${course.getPrice() == 0 ? 'selected' : ''}>Free</option>
                                                        <option value="paid" ${course.getPrice() > 0 ? 'selected' : ''}>Paid</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="form-label">Category</label>
                                                    <select name="cid" class="form-control" required="">
                                                        <c:forEach items="${listCategory}" var="listCat">
                                                            <option value="${listCat.getCategoryID()}" ${listCat.getCategoryID() == course.getCategory_CategoryID() ? 'selected' : ''}>${listCat.getName()}</option>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="form-label">Price</label>
                                                    <div id="price-container">
                                                        <c:choose>
                                                            <c:when test="${course.getPrice() == 0}">
                                                                <input type="number" id="Price" name="Price" class="form-control" disabled="" value="0"  required="">
                                                            </c:when>
                                                            <c:otherwise>
                                                                <input type="number" id="Price" name="Price" class="form-control" value="${course.getPrice()}"  required="">
                                                            </c:otherwise>
                                                        </c:choose>
                                                        <span id="price-error" style="color: red;"></span>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-md-6">${user.getFullName()}
                                                <div class="mb-3">
                                                    <c:forEach items="${listUser}" var="user">
                                                        <c:if test="${user.getUserID() == course.getUser_UserID()}">
                                                            <input type="text" name="uid" hidden value="${course.getUser_UserID()}" placeholder="Admin Created" class="form-control" readonly="">
                                                        </c:if>
                                                    </c:forEach>     
                                                </div>
                                            </div><!--end col-->
                                            <p style="color: green">${param.messSuccess}</p>
                                            <button type="submit" class="btn btn-outline-success">Cập nhật</button>
                                        </div>
                                    </form>
                                </div><!--end col-->
                            </div><!--end row-->
                        </div>
                    </div><!--end container-->
                    <!-- End -->
                    <!-- Footer Start -->
                    <%@include file="/view/common/footer.jsp"%>
                    <!-- End -->
            </main>
            <!--End page-content" -->
        </div>
        <!-- page-wrapper -->

        <!-- javascript -->
        <script>
            const priceTypeSelect = document.getElementById('price-type');
            const priceInput = document.getElementById('Price');
            const priceError = document.getElementById('price-error');


            priceTypeSelect.addEventListener('change', function () {
                if (priceTypeSelect.value === 'free') {
                    priceInput.disabled = true;
                    priceInput.value = 0; // Clear the value if it was entered previously
                } else {
                    priceInput.value = 1;
                    priceInput.disabled = false;
                }
            });

            priceInput.addEventListener('input', function () {

                if (priceTypeSelect.value === 'paid') {

                    const inputText = priceInput.value.replace(/[^0-9]/g, ''); 
                    priceInput.value = inputText; // Update input value

                    const priceValue = parseFloat(priceInput.value);
                    if (priceValue < 0) {
                        priceError.textContent = 'Price must be greater than 0';
                        priceInput.value = 1;
                    } else if (priceValue > 10000000) {
                        priceError.textContent = 'Price must be smaller than 10000000';
                        priceInput.value = 9999999;
                    } else if (priceValue === 0) {
                        priceTypeSelect.value = 'free';
                        priceInput.disabled = true;
                        priceInput.value = 0;
                    } else {
                        priceError.textContent = '';
                    }
                }
            });
        </script>
        <script src="../../../assets/js/jquery.min.js"></script>
        <script src="../../../assets/js/bootstrap.bundle.min.js"></script>
        <!-- simplebar -->
        <script src="../../../assets/js/simplebar.min.js"></script>
        <!-- Select2 -->
        <script src="../../../assets/js/select2.min.js"></script>
        <script src="../../../assets/js/select2.init.js"></script>
        <!-- Icons -->
        <script src="../../../assets/js/feather.min.js"></script>
        <!-- Main Js -->
        <script src="../../../assets/js/app.js"></script>

    </body>
</html>