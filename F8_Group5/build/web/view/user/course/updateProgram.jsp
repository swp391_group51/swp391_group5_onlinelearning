<%-- 
    Document   : addCourse
    Created on : Jun 10, 2023, 9:21:06 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>F8 - Học lập trình để đi làm</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
        <meta name="keywords" content="Appointment, Booking, System, Dashboard, Health" />
        <meta name="author" content="Shreethemes" />
        <meta name="email" content="support@shreethemes.in" />
        <meta name="website" content="index.html" />
        <meta name="Version" content="v1.2.0" />
        <!-- F8logo -->
        <link rel="shortcut icon" href="assets/images/logoF8.png">
        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- simplebar -->
        <link href="assets/css/simplebar.css" rel="stylesheet" type="text/css" />
        <!-- Select2 -->
        <link href="assets/css/select2.min.css" rel="stylesheet" />
        <!-- Icons -->
        <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css" rel="stylesheet">
        <!-- Css -->
        <link href="assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />

        <style>
            .list-inline-item:not(:last-child) {
                margin-right: .5rem;
                margin-left: 280px;
            }

            .subcribe-form input {
                padding: 12px 20px;
                width: 470px;
                color: #343a40 !important;
                border: none;
                outline: none !important;
                padding-right: 160px;
                padding-left: 20px;
                background-color: rgba(255, 255, 255, 0.8)
            }
        </style>
    </head>

    <body>
        <script>
            document.getElementById('imageInput').addEventListener('change', function (event) {
                const imagePreview = document.getElementById('imagePreview');
                imagePreview.innerHTML = ''; // Clear previous image preview, if any

                const file = event.target.files[0];

                if (file) {
                    const reader = new FileReader();

                    reader.onload = function (e) {
                        const img = document.createElement('img');
                        img.src = e.target.result;
                        img.classList.add('img-fluid'); // Add Bootstrap class for responsive images
                        img.style.maxWidth = '100%'; // Set maximum width for the image
                        imagePreview.appendChild(img);
                    };

                    reader.readAsDataURL(file);
                }
            });
        </script>
        <%@include file="/view/common/header-back.jsp" %>
        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div>
        <!-- Loader -->

        <div class="page-wrapper doctris-theme">
            <!-- Start Page Content -->
            <main class="page-content bg-light">
                <div class="container-fluid">
                    <div class="layout-specing">
                        <div class="d-md-flex justify-content-between">
                            <h4 style="color: orangered ; font-family: inherit">Thay đổi lộ trình </h4>
                            <h7 class="mb-0">
                                <a href="trainingManager" style="color: darkblue">Quản lý danh sách lộ trình</a>
                            </h7>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 mt-4">
                                <div class="card border-0 p-4 rounded shadow">
                                    <form action="updateprogram?CateID=${cate.getCategoryID()}" method="POST"
                                          enctype="multipart/form-data" class="mt-4">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="mb-3">
                                                    <label class="form-label">Program Name</label>
                                                    <input name="Name" type="text" value="${cate.getName()}"
                                                           class="form-control" placeholder="Program Name"
                                                           required="">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="mb-3">
                                                    <label class="form-label">Program Image</label>
                                                    <input id="imageInput" name="Image" type="file"
                                                           accept="image/png, image/jpeg">
                                                </div>
                                            </div><!--end col-->
                                            <div class="col-md-8">
                                                <div id="imagePreview"></div>
                                                <div>
                                                    <p id="fileSizeText"></p>
                                                </div>
                                                <div id="errorText" style="color: red;"></div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="mb-3">
                                                    <label class="form-label">Description</label>
                                                    <textarea name="Description" rows="4" class="form-control"
                                                              placeholder="Description"
                                                              required="">${cate.getDescription()}</textarea>
                                                </div>
                                            </div><!--end col-->
                                            <button id = "Updatebutton" type="submit" class="btn btn-primary" disabled>Thay đổi lộ
                                                trình</button>
                                        </div>
                                    </form>
                                </div><!--end col-->
                            </div><!--end row-->
                        </div>
                    </div><!--end container-->

                    <!-- Footer Start -->
                    <%@include file="/view/common/footer.jsp" %>
                    <!-- End -->
                    <!-- End -->
            </main>
            <!--End page-content" -->
        </div>


        <!-- page-wrapper -->
        <!-- javascript -->
        <script src="../../../assets/js/jquery.min.js"></script>
        <script src="../../../assets/js/bootstrap.bundle.min.js"></script>
        <!-- simplebar -->
        <script src="../../../assets/js/simplebar.min.js"></script>
        <!-- Select2 -->
        <script src="../../../assets/js/select2.min.js"></script>
        <script src="../../../assets/js/select2.init.js"></script>
        <!-- Icons -->
        <script src="../../../assets/js/feather.min.js"></script>
        <!-- Main Js -->
        <script src="../../../assets/js/app.js"></script>
        <script>
            const imageInput = document.getElementById('imageInput');
            const updateButton = document.getElementById('Updatebutton');
            const errorText = document.getElementById('errorText');
            const fileSizeText = document.getElementById('fileSizeText');
            const imagePreview = document.getElementById('imagePreview');

            imageInput.addEventListener('change', function (event) {
                imagePreview.innerHTML = ''; // Clear previous image preview, if any
                fileSizeText.textContent = '';
                errorText.textContent = ''; // Clear previous error message

                const file = event.target.files[0];

                if (file) {
                    if (file.size <= 30720 && (file.type === 'image/png' || file.type === 'image/jpeg')) {
                        const reader = new FileReader();

                        reader.onload = function (e) {
                            const img = document.createElement('img');
                            img.src = e.target.result;
                            img.classList.add('img-fluid');
                            img.style.maxWidth = '100%';
                            imagePreview.appendChild(img);

                            // Display file size in bytes
                            const fileSize = file.size;
                            fileSizeText.textContent = 'Image Memory: ' + fileSize + ' bytes';

                            // Enable the update button
                            updateButton.disabled = false;
                        };

                        reader.readAsDataURL(file);
                    } else {
                        // Disable the update button
                        updateButton.disabled = true;

                        if (file.size > 30720) {
                            fileSizeText.textContent = 'Error: File size exceeds 30,720 bytes (30 KB).';
                        }
                        if (file.type !== 'image/png' && file.type !== 'image/jpeg') {
                            errorText.textContent = 'Error: Selected file is not a valid image (PNG or JPEG only).';
                        }
                    }
                } else {
                    // No file selected, disable the update button
                    updateButton.disabled = true;
                }
            });
        </script>
    </body>

</html>

</body>

</html>
