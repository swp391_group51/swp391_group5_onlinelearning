<%-- 
    Document   : lesson
    Created on : May 19, 2023, 10:06:03 AM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quản lý bài học</title>
    </head>
    <body>
        <%@include file="/view/common/header-back.jsp"%>
        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div>
        <!-- Loader -->

        <div class="page-wrapper doctris-theme">
            <!-- Start Page Content -->
            <main class="page-content bg-light">
                <div class="container-fluid">
                    <div class="layout-specing">
                        <div class="d-md-flex justify-content-between">
                            <h4 style="color: orangered ; font-family: inherit">Quản lý danh sách bài học</h4>
                            <h7>
                                <a href="addlesson" class="btn btn-primary">Thêm bài học mới</a>
                            </h7>
                        </div>
                        <div class="row">
                            <div class="col-12 mt-4">
                                <div class="table-responsive shadow rounded">                             
                                    <table class="table table-center bg-white mb-0">
                                        <thead>
                                            <tr>
                                                <th class="border-bottom p-4" style="width: 150px; border-right: 1px dashed red;">Lesson ID</th>
                                                <th class="border-bottom p-4" style="width: 300px; border-right: 1px dashed #00cccc;">Lesson Name</th>
                                                <th class="border-bottom p-4" style="width: 300px; border-right: 1px dashed red;">Lesson Video</th>
                                                <th class="border-bottom p-4" style="width: 150px; border-right: 1px dashed #00cccc;">Course Name</th>
                                                <th class="border-bottom p-4" style="width: 50px;">Actions</th>
                                            </tr>
                                        </thead>                                     
                                        <tbody>
                                            <c:forEach items="${listLesson}" var="lesson">
                                                <tr>
                                                    <td class="border-bottom p-4" style="width: 150px; border-right: 1px dashed red;">${lesson.getLessonID()}</td>
                                                    <td class="border-bottom p-4" style="width: 300px; border-right: 1px dashed #00cccc;">${lesson.getName()}</td>
                                                    <td class="border-bottom p-4" style="width: 300px; border-right: 1px dashed red;">
                                                        <c:choose>
                                                            <c:when test="${lesson.getVideo().contains('youtube.com')}">
                                                                <a href="${lesson.getVideo()}" target="_blank">${lesson.getVideo()}</a>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <a href="ReadTextFile?LessonID=${lesson.getLessonID()}">${lesson.getVideo()}</a>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </td>
                                                    <td class="border-bottom p-4" style="width: 150px; border-right: 1px dashed #00cccc;">${course.getCourseByID(lesson.getCourseID()).getName()}</td>
                                                    <td class="border-bottom p-4" style="width: 50px">
                                                        <a href="updatelesson?LessonID=${lesson.getLessonID()}" class="btn btn-icon btn-pills btn-soft-primary"><i class="uil uil-edit"></i></a>
                                                        <form action="deletelesson?LessonID=${lesson.getLessonID()}&CourseID=${lesson.getCourseID()}" method="post">
                                                            <button type="submit" class="btn btn-icon btn-pills btn-soft-danger"><i class="uil uil-trash-alt"></i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            </c:forEach>

                                        </tbody>
                                    </table>
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="mt-4">
                        <ul class="pagination justify-content-center mb-0 list-unstyled">
                            <c:choose>
                                <c:when test="${totalPage > 1}">
                                    <c:forEach begin="1" end="${totalPage}" var="i">
                                        <c:set var="activeClass" value="${(i == currentPage) ? 'active' : ''}" />
                                        <li class="page-item ${activeClass}">
                                            <a class="page-link" href="lessonManager?page=${i}">${i}</a>
                                        </li>
                                    </c:forEach>
                                </c:when>
                            </c:choose>
                        </ul><!--end pagination-->
                    </div>
                </div><!--end container-->


                <!-- Footer Start -->
                <%@include file="/view/common/footer.jsp"%>
                <!-- End -->
            </main>
            <!--End page-content" -->
        </div>
        <!-- page-wrapper -->
        <style>
            /* Style for table rows */
            tbody tr {
                background-color: #f8f9fa; /* Light gray background */
            }

            /* Add space between table cells */
            tbody td {
                padding: 10px;
            }

            /* Style for delete button */
            .btn-soft-danger {
                background-color: #f44336; /* Red color for danger buttons */
                color: white;
                border: none;
                border-radius: 4px;
                padding: 6px 12px;
                cursor: pointer;
            }

            /* Hover effect for buttons */
            .btn-soft-danger:hover {
                background-color: #d32f2f; /* Darker red color on hover */
            }
        </style>

        <!-- java script -->
        <script src="../../../assets/js/bootstrap.bundle.min.js"></script>
        <!-- simplebar -->
        <script src="../../../assets/js/simplebar.min.js"></script>
        <!-- Icons -->
        <script src="../../../assets/js/feather.min.js"></script>
        <!-- Main Js -->
        <script src="../../../assets/js/app.js"></script>
    </body>
</html>
