<%-- 
    Document   : userManager
    Created on : Jun 19, 2023, 11:36:03 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!Doctype html>
<html lang="en" dir="ltr">

    <head>
        <meta charset="utf-8" />
        <title>Doctris - Doctor Appointment Booking System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 5 Landing Page Template" />
        <meta name="keywords" content="Appointment, Booking, System, Dashboard, Health" />
        <meta name="author" content="Shreethemes" />
        <meta name="email" content="support@shreethemes.in" />
        <meta name="website" content="https://shreethemes.in" />
        <meta name="Version" content="v1.4.0" />
        <!-- favicon -->
        <link rel="shortcut icon" href="../../assets/images/favicon.ico">

        <!-- Css -->
        <link href="../../assets/libs/simplebar/simplebar.min.css" rel="stylesheet">
        <!-- Bootstrap Css -->
        <link href="../../assets/css/bootstrap.min.css" class="theme-opt" rel="stylesheet" type="text/css" />
        <!-- Icons Css -->
        <link href="../../assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/libs/remixicon/fonts/remixicon.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/libs/@iconscout/unicons/css/line.css" type="text/css" rel="stylesheet" />
        <!-- Style Css-->
        <link href="../../assets/css/style.min.css" class="theme-opt" rel="stylesheet" type="text/css" />

    </head>

    <body>
        <%@include file="/view/common/header.jsp"%>

        <div class="page-wrapper doctris-theme">
            <!-- Start Page Content -->
            <main class="page-content bg-light">
                <div class="container-fluid">
                    <div class="layout-specing">
                        <!-- title -->
                        <div class="d-md-flex justify-content-between">
                            <h5 class="mb-0">User Manage</h5>

                            <nav aria-label="breadcrumb" class="d-inline-block mt-4 mt-sm-0">
                                <ul class="breadcrumb bg-transparent rounded mb-0 p-0">
                                    <li class="breadcrumb-item"><a href="#">Manage</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Manage</li>
                                </ul>
                            </nav>
                        </div>
                        <!-- title -->

                        <div class="card rounded border-0 mt-4 overflow-hidden">
                            <div class="row g-1">
                                <!-- Nav bar -->
                                <div class="col-xl-2 col-lg-3 col-md-4 col-12">
                                    <div class="card rounded-0 border-0 shadow p-4">
                                        <a href="javascript:void(0)" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#compose-mail"><i class="uil uil-plus me-1"></i> Compose</a>

                                        <ul class="nav nav-pills nav-link-soft nav-justified flex-column mt-4 mb-0" id="pills-tab" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link rounded active" id="inbox-tab" data-bs-toggle="pill" href="#inbox" role="tab" aria-controls="inbox" aria-selected="false">
                                                    <div class="text-start px-3">
                                                        <span class="mb-0"><i class="uil uil-inbox h5 align-middle me-2 mb-0"></i> Inbox</span>
                                                    </div>
                                                </a><!--end nav link-->
                                            </li><!--end nav item-->

                                            <li class="nav-item mt-2">
                                                <a class="nav-link rounded" id="starred-tab" data-bs-toggle="pill" href="#starred" role="tab" aria-controls="starred" aria-selected="false">
                                                    <div class="text-start px-3">
                                                        <span class="mb-0"><i class="uil uil-envelope-star h5 align-middle me-2 mb-0"></i> Starred</span>
                                                    </div>
                                                </a><!--end nav link-->
                                            </li><!--end nav item-->

                                            <li class="nav-item mt-2">
                                                <a class="nav-link rounded" id="spam-tab" data-bs-toggle="pill" href="#spam" role="tab" aria-controls="spam" aria-selected="false">
                                                    <div class="text-start px-3">
                                                        <span class="mb-0"><i class="uil uil-envelope-times h5 align-middle me-2 mb-0"></i> Spam</span>
                                                    </div>
                                                </a><!--end nav link-->
                                            </li><!--end nav item-->

                                            <li class="nav-item mt-2">
                                                <a class="nav-link rounded" id="send-tab" data-bs-toggle="pill" href="#sent" role="tab" aria-controls="sent" aria-selected="false">
                                                    <div class="text-start px-3">
                                                        <span class="mb-0"><i class="uil uil-envelope-upload h5 align-middle me-2 mb-0"></i> Sent</span>
                                                    </div>
                                                </a><!--end nav link-->
                                            </li><!--end nav item-->

                                            <li class="nav-item mt-2">
                                                <a class="nav-link rounded" id="drafts-tab" data-bs-toggle="pill" href="#drafts" role="tab" aria-controls="drafts" aria-selected="false">
                                                    <div class="text-start px-3">
                                                        <span class="mb-0"><i class="uil uil-envelope-edit h5 align-middle me-2 mb-0"></i> Drafts</span>
                                                    </div>
                                                </a><!--end nav link-->
                                            </li><!--end nav item-->

                                            <li class="nav-item mt-2">
                                                <a class="nav-link rounded" id="delete-tab" data-bs-toggle="pill" href="#delete" role="tab" aria-controls="delete" aria-selected="false">
                                                    <div class="text-start px-3">
                                                        <span class="mb-0"><i class="uil uil-trash h5 align-middle me-2 mb-0"></i> Delete</span>
                                                    </div>
                                                </a><!--end nav link-->
                                            </li><!--end nav item-->

                                            <li class="nav-item mt-2">
                                                <a class="nav-link rounded" id="notes-tab" data-bs-toggle="pill" href="#notes" role="tab" aria-controls="notes" aria-selected="false">
                                                    <div class="text-start px-3">
                                                        <span class="mb-0"><i class="uil uil-notes h5 align-middle me-2 mb-0"></i> Notes</span>
                                                    </div>
                                                </a><!--end nav link-->
                                            </li><!--end nav item-->
                                        </ul><!--end nav pills-->
                                    </div>
                                </div><!--end col-->
                                <!-- Nav bar -->
                                <!-- Table -->
                                <div class="col-xl-10 col-lg-9 col-md-8 col-12 mt-4 mt-sm-0">
                                    <div class="tab-content rounded-0 shadow" id="pills-tabContent">
                                        <div class="tab-pane fade bg-white show active" id="inbox" role="tabpanel" aria-labelledby="inbox-tab">
                                            <div class="table-responsive bg-white shadow rounded">
                                                <table class="table mb-0 table-center">
                                                    <thead>
                                                        <tr>
                                                            <th class="border-bottom p-3" style="min-width: 15px;" scope="col">
                                                                <div class="form-check">
                                                                    <input class="form-check-input check" type="checkbox" value="" id="checkAll">
                                                                    <label class="form-check-label fw-normal" for="checkAll"></label>
                                                                </div>
                                                            </th>
                                                            <th class="border-bottom p-3" style="min-width: 15px;" scope="col">
                                                                <a href="#" class="text-muted"><i class="uil uil-redo"></i></a>
                                                            </th>
                                                            <th class="border-bottom p-3" style="min-width: 150px;" scope="col"></th>
                                                            <th class="border-bottom p-3" style="min-width: 750px;" scope="col"></th>
                                                            <th class="border-bottom p-3 text-end" style="min-width: 150px;" scope="col">
                                                                <a href="#" class="btn btn-icon btn-sm btn-pills btn-soft-light"><i class="uil uil-angle-left fs-5"></i></a>
                                                                <a href="#" class="btn btn-icon btn-sm btn-pills btn-soft-light"><i class="uil uil-angle-right fs-5"></i></a>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th class="p-3">
                                                                <div class="form-check">
                                                                    <input class="form-check-input check" type="checkbox" value="" id="checkbox1">
                                                                    <label class="form-check-label" for="checkbox1"></label>
                                                                </div>
                                                            </th>
                                                            <th class="p-3"><i class="uil uil-star text-muted"></i></th>
                                                            <td class="p-3">Amy Lucier</td>
                                                            <td class="p-3"><p class="text-muted mb-0">In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do.</p></td>
                                                            <td class="text-end class p-3"><small>17 Jan 2021</small></td>
                                                        </tr>
                                                        <tr>
                                                            <th class="p-3">
                                                                <div class="form-check">
                                                                    <input class="form-check-input check" type="checkbox" value="" id="checkbox2">
                                                                    <label class="form-check-label" for="checkbox2"></label>
                                                                </div>
                                                            </th>
                                                            <th class="p-3"><i class="uil uil-star text-muted"></i></th>
                                                            <td class="p-3">Tameika Whittle</td>
                                                            <td class="p-3"><p class="text-muted mb-0">The wise man therefore always holds in these matters to this principle of selection.</p></td>
                                                            <td class="text-end class p-3"><small>27 Jan 2021</small></td>
                                                        </tr>
                                                        <tr>
                                                            <th class="p-3">
                                                                <div class="form-check">
                                                                    <input class="form-check-input check" type="checkbox" value="" id="checkbox3">
                                                                    <label class="form-check-label" for="checkbox3"></label>
                                                                </div>
                                                            </th>
                                                            <th class="p-3"><i class="uil uil-star text-muted"></i></th>
                                                            <td class="p-3">Barbara Bayne</td>
                                                            <td class="p-3"><p class="text-muted mb-0">We denounce with righteous</p></td>
                                                            <td class="text-end class p-3"><small>21 Feb 2021</small></td>
                                                        </tr>
                                                        <tr>
                                                            <th class="p-3">
                                                                <div class="form-check">
                                                                    <input class="form-check-input check" type="checkbox" value="" id="checkbox4">
                                                                    <label class="form-check-label" for="checkbox4"></label>
                                                                </div>
                                                            </th>
                                                            <th class="p-3"><i class="uil uil-star text-muted"></i></th>
                                                            <td class="p-3">Nita Griffin</td>
                                                            <td class="p-3"><p class="text-muted mb-0">In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do.</p></td>
                                                            <td class="text-end class p-3"><small>17 Jan 2021</small></td>
                                                        </tr>
                                                        <tr>
                                                            <th class="p-3">
                                                                <div class="form-check">
                                                                    <input class="form-check-input check" type="checkbox" value="" id="checkbox5">
                                                                    <label class="form-check-label" for="checkbox5"></label>
                                                                </div>
                                                            </th>
                                                            <th class="p-3"><i class="uil uil-star text-muted"></i></th>
                                                            <td class="p-3">Marc Flythe</td>
                                                            <td class="p-3"><p class="text-muted mb-0">The wise man therefore always holds in these matters to this principle of selection.</p></td>
                                                            <td class="text-end class p-3"><small>27 Jan 2021</small></td>
                                                        </tr>
                                                        <tr>
                                                            <th class="p-3">
                                                                <div class="form-check">
                                                                    <input class="form-check-input check" type="checkbox" value="" id="checkbox6">
                                                                    <label class="form-check-label" for="checkbox6"></label>
                                                                </div>
                                                            </th>
                                                            <th class="p-3"><i class="uil uil-star text-muted"></i></th>
                                                            <td class="p-3">Elaine Hannah</td>
                                                            <td class="p-3"><p class="text-muted mb-0">pain avoided.</p></td>
                                                            <td class="text-end class p-3"><small>21 Feb 2021</small></td>
                                                        </tr>
                                                        <tr>
                                                            <th class="p-3">
                                                                <div class="form-check">
                                                                    <input class="form-check-input check" type="checkbox" value="" id="checkbox7">
                                                                    <label class="form-check-label" for="checkbox7"></label>
                                                                </div>
                                                            </th>
                                                            <th class="p-3"><i class="uil uil-star text-muted"></i></th>
                                                            <td class="p-3">Abraham Edwards</td>
                                                            <td class="p-3"><p class="text-muted mb-0">We denounce with righteous</p></td>
                                                            <td class="text-end class p-3"><small>21 Feb 2021</small></td>
                                                        </tr>
                                                        <tr>
                                                            <th class="p-3">
                                                                <div class="form-check">
                                                                    <input class="form-check-input check" type="checkbox" value="" id="checkbox8">
                                                                    <label class="form-check-label" for="checkbox8"></label>
                                                                </div>
                                                            </th>
                                                            <th class="p-3"><i class="uil uil-star text-muted"></i></th>
                                                            <td class="p-3">Allie Smith</td>
                                                            <td class="p-3"><p class="text-muted mb-0">In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do.</p></td>
                                                            <td class="text-end class p-3"><small>17 Jan 2021</small></td>
                                                        </tr>
                                                        <tr>
                                                            <th class="p-3">
                                                                <div class="form-check">
                                                                    <input class="form-check-input check" type="checkbox" value="" id="checkbox9">
                                                                    <label class="form-check-label" for="checkbox9"></label>
                                                                </div>
                                                            </th>
                                                            <th class="p-3"><i class="uil uil-star text-muted"></i></th>
                                                            <td class="p-3">Eleanor Cisco</td>
                                                            <td class="p-3"><p class="text-muted mb-0">The wise man therefore always holds in these matters to this principle of selection.</p></td>
                                                            <td class="text-end class p-3"><small>27 Jan 2021</small></td>
                                                        </tr>
                                                        <tr>
                                                            <th class="p-3">
                                                                <div class="form-check">
                                                                    <input class="form-check-input check" type="checkbox" value="" id="checkbox10">
                                                                    <label class="form-check-label" for="checkbox10"></label>
                                                                </div>
                                                            </th>
                                                            <th class="p-3"><i class="uil uil-star text-muted"></i></th>
                                                            <td class="p-3">Troy Turk</td>
                                                            <td class="p-3"><p class="text-muted mb-0">pain avoided.</p></td>
                                                            <td class="text-end class p-3"><small>21 Feb 2021</small></td>
                                                        </tr>
                                                        <tr>
                                                            <th class="p-3">
                                                                <div class="form-check">
                                                                    <input class="form-check-input check" type="checkbox" value="" id="checkbox11">
                                                                    <label class="form-check-label" for="checkbox11"></label>
                                                                </div>
                                                            </th>
                                                            <th class="p-3"><i class="uil uil-star text-muted"></i></th>
                                                            <td class="p-3">Tyron Elliott</td>
                                                            <td class="p-3"><p class="text-muted mb-0">We denounce with righteous</p></td>
                                                            <td class="text-end class p-3"><small>21 Feb 2021</small></td>
                                                        </tr>
                                                        <tr>
                                                            <th class="p-3">
                                                                <div class="form-check">
                                                                    <input class="form-check-input check" type="checkbox" value="" id="checkbox12">
                                                                    <label class="form-check-label" for="checkbox12"></label>
                                                                </div>
                                                            </th>
                                                            <th class="p-3"><i class="uil uil-star text-muted"></i></th>
                                                            <td class="p-3">Betty Cook</td>
                                                            <td class="p-3"><p class="text-muted mb-0">We denounce with righteous</p></td>
                                                            <td class="text-end class p-3"><small>21 Feb 2021</small></td>
                                                        </tr>
                                                        <tr>
                                                            <th class="p-3">
                                                                <div class="form-check">
                                                                    <input class="form-check-input check" type="checkbox" value="" id="checkbox13">
                                                                    <label class="form-check-label" for="checkbox13"></label>
                                                                </div>
                                                            </th>
                                                            <th class="p-3"><i class="uil uil-star text-muted"></i></th>
                                                            <td class="p-3">Doris Morrison</td>
                                                            <td class="p-3"><p class="text-muted mb-0">In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do.</p></td>
                                                            <td class="text-end class p-3"><small>17 Jan 2021</small></td>
                                                        </tr>
                                                        <tr>
                                                            <th class="p-3">
                                                                <div class="form-check">
                                                                    <input class="form-check-input check" type="checkbox" value="" id="checkbox14">
                                                                    <label class="form-check-label" for="checkbox14"></label>
                                                                </div>
                                                            </th>
                                                            <th class="p-3"><i class="uil uil-star text-muted"></i></th>
                                                            <td class="p-3">Chad Potter</td>
                                                            <td class="p-3"><p class="text-muted mb-0">The wise man therefore always holds in these matters to this principle of selection.</p></td>
                                                            <td class="text-end class p-3"><small>27 Jan 2021</small></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <p class="text-end text-muted mb-0 py-2 mx-2">See more 14 of 35 PAGING</p>
                                        </div><!--end teb pane-->
                                    </div>
                                </div>
                                <!-- Table -->
                            </div><!--end row-->
                        </div>
                    </div>
                </div><!--end container-->
                <!-- Footer Start -->
                <%@include file="/view/common/footer.jsp"%>
                <!-- End -->
            </main>
            <!--End page-content" -->
        </div>
        <!-- page-wrapper -->        

        <!-- javascript -->
        <script src="../assets/libs/simplebar/simplebar.min.js"></script>
        <script src="../assets/libs/feather-icons/feather.min.js"></script>
        <script src="../assets/js/ckeditor.js"></script>
        <!-- Main Js -->
        <!-- JAVASCRIPT -->
        <script src="../assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="../assets/js/plugins.init.js"></script>
        <script src="../assets/js/app.js"></script>
        
        <!-- javascript -->
        <script src="../../../assets/libs/simplebar/simplebar.min.js"></script>
        <script src="../../../assets/libs/feather-icons/feather.min.js"></script>
        <!-- Main Js -->
        <!-- JAVASCRIPT -->
        <script src="../../assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="../../assets/js/plugins.init.js"></script>
        <script src="../../assets/js/app.js"></script>
        
        <script>
            ClassicEditor
                    .create(document.querySelector('#editor'))
                    .catch(error => {
                        console.error(error);
                    });
        </script>
    </body>
</html>
