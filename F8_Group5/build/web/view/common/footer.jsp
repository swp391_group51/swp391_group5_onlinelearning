<%-- 
    Document   : footer
    Created on : May 19, 2023, 9:47:09 AM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- Footer -->
<style>
    .bg-footer{
        background-color: #001123 !important;
    }
</style>
<!-- Footer -->
<footer class="bg-footer">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-lg-4 mb-0 mb-md-4 pb-0 pb-md-2">
                <a href="home" class="logo-footer">
                    <img src="assets/images/logoF8.png" style="border-radius: 20%" height="50" width="50" alt="F8 logo">
                    <strong style="color: #ffffff"><strong style="font-size: 18px">Học lập trình để đi làm</strong></strong>
                </a>
                <p class="mt-4 me-xl-5">Điện thoại: 0246.329.1102<br/>
                    Email: contact@fullstack.edu.vn<br/>
                    Địa chỉ: Số 26 Dương Đình Nghệ, Phường Yên Hòa, Quận Cầu Giấy, TP. Hà Nội</p>
            </div>
            <div class="col-xl-4 col-lg-4 mb-0 mb-md-4 pb-0 pb-md-2">
                <h5 class="text-light title-dark footer-head">CÔNG TY CỔ PHẦN CÔNG NGHỆ GIÁO DỤC F8</h5>
                <ul class="list-unstyled footer-list mt-4">
                    <a>Mã số thuế: 0109922901<br>
                        Ngày thành lập: 04/03/2022<br>
                        Lĩnh vực: Công nghệ, giáo dục, lập trình.<br>
                        F8 xây dựng và phát triển những sản phẩm mang lại giá trị cho cộng đồng.</a>
                </ul>
            </div><!-- comment -->
            <div class="col-xl-4 col-lg-4 mb-0 mb-md-4 pb-0 pb-md-2">
                <h5 class="text-light title-dark footer-head">Liên hệ</h5>
                <ul class="list-unstyled footer-list mt-4">

                    <li class="d-flex align-items-center">
                        <i data-feather="phone" class="fea icon-sm text-foot align-middle"></i>
                        <a href="tel:+152534-468-854" class="text-foot ms-2">Tel: (028)22200219</a>
                    </li>

                    <li class="d-flex align-items-center">
                        <i data-feather="phone" class="fea icon-sm text-foot align-middle"></i>
                        <a href="tel:+152534-468-854" class="text-foot ms-2">Hotline: 1900 6333 96</a>
                    </li>

                    <li class="d-flex align-items-center">
                        <i data-feather="mail" class="fea icon-sm text-foot align-middle"></i>
                        <a href="contact@fullstack.edu.vn" class="text-foot ms-2">contact@fullstack.edu.vn</a>
                    </li>
                </ul>
                <ul class="list-unstyled social-icon footer-social mb-0 mt-4 list-inline d-flex">
                    <a href="#" class="btn btn-icon btn-pills btn-dark"><i data-feather="facebook" class="fea icon-sm"></i></a>
                    <a href="#" class="btn btn-icon btn-pills btn-dark"><i data-feather="instagram" class="fea icon-sm"></i></a>
                    <a href="#" class="btn btn-icon btn-pills btn-dark"><i data-feather="twitter" class="fea icon-sm"></i></a>
                    <a href="#" class="btn btn-icon btn-pills btn-dark"><i data-feather="linkedin" class="fea icon-sm"></i></a>
                    <a href="#" class="btn btn-icon btn-pills btn-dark"><i data-feather="github" class="fea icon-sm"></i></a>
                    <a href="/F8_Group6/payment?CourseID=1" style="font-size: 1px">Link to payment test</a>
                </ul>
            </div>
        </div>
    </div><!-- end div -->
    <div class="container mt-5">
        <div class="pt-4 footer-bar">
            <div class="row align-items-center">
                <div class="col-sm-6"></div>
            </div>
        </div>
    </div>
</footer><!-- End Footer -->
<!-- back to top -->
<!-- end back to top -->
<!-- Java Script -->
<!-- Footer -->
<!-- Icons -->
<script src="assets/js/feather.min.js"></script>
<!-- Main Js -->
<script src="assets/js/app.js"></script>