<%-- 
    Document   : blog
    Created on : May 19, 2023, 10:05:52 AM
    Author     : ADMIN
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>
            Danh sách bài viết về lĩnh vực IT / CNTT / Phần mềm / lập trình tại F8
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
        <meta name="keywords" content="Appointment, Booking, System, Dashboard, Health" />
        <meta name="author" content="Shreethemes" />
        <meta name="email" content="support@shreethemes.in" />
        <meta name="website" content="https://shreethemes.in" />
        <meta name="Version" content="v1.2.0" />
        <!-- favicon -->
        <link rel="shortcut icon" href="assets/images/logoF8.png">
        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/remixicon.css" rel="stylesheet" type="text/css" />
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <!-- SLIDER -->
        <link rel="stylesheet" href="assets/css/tiny-slider.css"/>
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <!-- Css -->
        <link href="assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />
        <style>
            .blog-detail-container {
                max-height: 100px; /* Đặt chiều cao tối đa của BlogDetail */
                overflow: hidden; /* Ẩn nội dung vượt quá */
            }
            .copy-success-alert {
                position: fixed;
                top: 80px;
                left: 50%;
                transform: translate(-50%, -50%);
                margin-top: 30px;
                padding: 8px 60px;
                color: whitesmoke;
                font-weight: 400;
                font-family: var(--bs-font-sans-serif);
                border-radius: 3px;
                opacity: 0;
                transition: opacity 0.3s;
            }
            .copy-success-alert.show {
                opacity: 1;
            }
            #searchContent{
                border: none;
                outline: none;
            }

        </style>
    </head>
    <body>
        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div>
        <!-- Loader -->
        <%@include file="/view/common/header.jsp"%>

        <!-- End Hero -->

        <!-- Start -->
        <section class="bg-dashboard">         
            <div class="container-fluid">            
                <div class="row">
                    <div class="col-xl-2 col-lg-2 col-md-3 col-6">
                        <div class="rounded overflow-hidden sticky-bar">
                            <%@include file="/view/common/navbar.jsp"%>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-8 col-md-6 mt-4 mt-sm-0">

                        <strong>
                            Kết quả tìm kiếm cho: ${name}
                            <!--<input type="text" id="searchContent" onkeyup="searchValue(this.value)"/>-->
                        </strong>
                        <br><br><br>


                        <div class="mb-4" style="border-bottom: 1px solid rgba(0,0,0,.079);padding-bottom: 5px;">
                            <button id="BlogBorder"  onclick="openCity('Course', 'Blog', 'BlogBorder', 'CourseBorder')" style="border: none;background:  white;font-size: 20px;${page eq 'course' ? 'border-bottom:2px solid black':''}">Khóa học</button>
                            <button id="CourseBorder"   onclick="openCity('Blog', 'Course', 'CourseBorder', 'BlogBorder')" style="border: none;background:  white;font-size: 20px;${page eq 'blog' ? 'border-bottom:2px solid black':''}">Bài viết</button>

                        </div>

                        <div id="Blog" class="w3-container-fluid city"  style="${page eq 'blog' ? 'display:block':'display:none'}">
                            <c:forEach var="b" items="${listBlog}">
                                <div class=" rounded mb-4" style="width: 70%;border-bottom: 1px solid rgba(0,0,0,.079)">


                                    <div class="row">
                                        <div class="col-lg-12" style="height: 50%;">
                                            <a href="blogview?id=${b.getBlogID()}">
                                                <img style="width: 100%;height: 200px;" class="rounded" src="http://localhost:9999/F8_Group6/assets/images/blog/${b.getBlogImage()}"/>    
                                            </a>
                                            <div class="mt-2"><a href="blogview?id=${b.getBlogID()}" class="text-dark title h5" style="font-size: 23px; font-weight: bold">${b.getBlogTitle()}</a></div>

                                            <div class="mt-2"><a style="color: black;text-decoration: none;font-size: 18px;font-weight: 500" href="blogview?id=${b.getBlogID()}">Đọc thêm ...</a></div>
                                            <div class="mb-4 mt-2">
                                                <i class="mdi mdi-heart-outline me-1" style="color: #666; font-size: large"></i>${b.getLikes()}
                                                <span style="color:#666;float: right;font-weight: 500">${b.getComments()} bình luận</span>
                                            </div>

                                        </div>

                                    </div>

                                </div>
                            </c:forEach>
                            <div class="row text-center mt-2 mb-3">
                                <div class="col-12">
                                    <ul class="pagination justify-content-center mb-0 list-unstyled">
                                        <c:forEach begin="1" end="${totalPageBlog}" var="i">
                                            <li class="page-item ${indexBlog == i ? 'active':''}" ><a class="page-link" href="searchPage?indexBlog=${i}&page=blog&name=${name}">${i}</a></li>
                                            </c:forEach>   
                                    </ul><!--end pagination-->
                                </div><!--end col-->
                            </div>
                        </div>

                        <div id="Course" class="w3-container-fluid" style="${page eq 'course' ? 'display:block':'display:none'}">
                            <c:forEach var="c" items="${listCourse}">
                                <div class=" rounded mb-4" style="border-bottom: 1px solid rgba(0,0,0,.079)">
                                    <div class="row">
                                        <div class="col-lg-12" style="height: 50%;display: flex">
                                            <a href="viewCourse?CourseID=${c.getCourseID()}">
                                                <img style="width: 300px;height: 200px;" class="rounded" src="http://localhost:9999/F8_Group6/assets/images/course/${c.getImage()}"/>    
                                            </a>
                                            <div style="margin-left: 20px">
                                                <div style="font-weight: 500;font-size: 20px"><a href="viewCourse?CourseID=${c.getCourseID()}">${c.getName()}</a></div>
                                                <div style="color: #757575;">${c.getCourseInfo()}</div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </c:forEach>
                            <div class="row text-center mt-2 mb-3">
                                <div class="col-12">
                                    <ul class="pagination justify-content-center mb-0 list-unstyled">
                                        <c:choose>
                                            <c:when test="${indexCourse > 1}">
                                                <li class="page-item"><a class="page-link" href="searchPage?indexCourse=${indexBlog - 1}&page=course&name=${name}" aria-label="Previous">< Trang trước</a></li>
                                                </c:when>
                                                <c:otherwise>
                                                <li class="page-item disabled"><a class="page-link" href="#" aria-label="Previous">< Trang trước</a></li>
                                                </c:otherwise>
                                            </c:choose>
                                            <c:forEach begin="1" end="${totalPageCourse}" var="i">
                                            <li class="page-item ${indexCourse == i ? 'active':''}"><a class="page-link" href="searchPage?indexCourse=${i}&page=course&name=${name}">${i}</a></li>
                                            </c:forEach>   
                                            <c:choose>
                                                <c:when test="${indexCourse < totalPageCourse}">
                                                <li class="page-item"><a class="page-link" href="searchPage?indexCourse=${indexBlog + 1}&page=course&name=${name}" aria-label="Next">Trang sau ></a></li>
                                                </c:when>
                                                <c:otherwise>
                                                <li class="page-item disabled"><a class="page-link" href="#" aria-label="Next">Trang sau ></a></li>
                                                </c:otherwise>
                                            </c:choose>
                                    </ul><!--end pagination-->
                                </div><!--end col-->
                            </div>
                        </div>



                        <script>
                            function openCity(cityName, non, border, noneBorder) {
                                var i;
                                var x = document.getElementById(cityName);
                                x.style.display = 'block';
                                document.getElementById(non).style.display = 'none';
                                document.getElementById(cityName).style.display = "block";
                                document.getElementById(border).style.setProperty("border-bottom", "2px solid black");

                                document.getElementById(noneBorder).style.setProperty("border-bottom", "none");

                            }
                            function searchValue(value) {
                                var url = "searchPage?name=" + encodeURIComponent(value);
                                window.location.href = url;

                            }
                        </script>
                    </div><!--end row-->
                    <div class="col-xl-3 col-lg-2 col-md-3 col-6" style="margin-top: 120px; margin-left: 30px">
                        <div class="sticky-bar rounded overflow-hidden">
                            <%@include file="/view/common/sidebar-advertise.jsp"%>
                        </div>
                    </div>
                </div><!--end container-->
            </div>
        </section>
        <!-- Offcanvas End -->

        <!-- javascript -->
        <script src="https://kit.fontawesome.com/4ec0e85a31.js" crossorigin="anonymous"></script>
        <script src="assets/js/bootstrap.bundle.min.js"></script>
        <!-- SLIDER -->
        <script src="assets/js/tiny-slider.js"></script>
        <script src="assets/js/tiny-slider-init.js"></script>
        <!-- Icons -->
        <script src="assets/js/feather.min.js"></script>
        <!-- Main Js -->
        <script src="assets/js/app.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.8/clipboard.min.js"></script>
        <script>
                            // Xu ly do dai cua blog details               
                            const detailContainers = document.querySelectorAll('.blog-detail-container');
                            detailContainers.forEach((container) => {
                                const detailText = container.querySelector('p').textContent;
                                const maxLength = 100; // Độ dài tối đa của đoạn văn bản
                                if (detailText.length > maxLength) {
                                    const truncatedText = detailText.substring(0, maxLength) + '...';
                                    container.querySelector('p').textContent = truncatedText;
                                }
                            });
                            function deleteBlog(event, blogID) {
                                event.preventDefault(); // Ngăn chặn hành vi mặc định của thẻ <a>

                                // Hiển thị toast thông báo
                                toastr.success('Bài viết đã được xóa thành công!');

                                // Thực hiện chuyển hướng tới đường dẫn
                                setTimeout(function () {
                                    window.location.href = `save-blog-action?id=${blogID}&action=unsave`;
                                }, 2000); // Chuyển hướng sau 2 giây (có thể điều chỉnh thời gian)
                            }

//                                                                    function sendData(action, id) {
//                                                                        var xhr = new XMLHttpRequest();
//                                                                        var url = "save-blog-action";
//
//
//                                                                        url += "?action=" + encodeURIComponent(action) + "&id=" + encodeURIComponent(id);
//
//                                                                        xhr.open("GET", url, true);
//
//                                                                        xhr.onreadystatechange = function () {
//                                                                            if (xhr.readyState === 4 && xhr.status === 200) {
//                                                                                if (action === 'unsave')
//                                                                                    document.getElementById("bookMarkSave").style.color = "white";
//                                                                                else
//                                                                                    document.getElementById("bookMarkNotSave").style.color = "yellow";
//                                                                            }
//                                                                        };
//
//                                                                        xhr.send();
//                                                                    }
//                                                                    ;

        </script>
    </body>
    <%@include file="/view/common/footer.jsp"%>
</html>
