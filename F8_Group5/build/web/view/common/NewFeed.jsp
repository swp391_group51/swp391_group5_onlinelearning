<%-- 
    Document   : NewFeed
    Created on : 22-08-2023, 16:17:34
    Author     : Hanh
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>News Feed</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 0;
            }
            header {
                background-color: #000;
                color: #ffffff;
                text-align: center;
                padding: 1rem;
            }
            .news-item {
                border: 1px solid #ccc;
                margin: 1rem;
                padding: 1rem;
            }
            .news-title {
                font-size: 1.2rem;
                margin-bottom: 0.5rem;
            }
            .news-description {
                font-size: 1rem;
                margin-bottom: 0.5rem;
            }
            .news-video {
                max-width: 100%;
            }
            .news-author {
                font-style: italic;
                color: #777;
            }
        </style>
    </head>
    <body>
        <header>
            <h1>News Feed</h1>
        </header>
        <div class="news-container">
            <c:forEach items="${feedlist}" var="list">
                <div class="col-md-6">
                    <div class="news-item">
                        <h2 class="news-title">${list.getFeedTitle()}</h2>
                        <p class="news-description">${list.getDescription()}</p>
                        <a href="${list.getFeedVideo()}" target="_blank">
                            <img class="news-video" src="assets/images/NewFeed/${list.getImage()}" alt="Thumbnail" width="50%" height="auto">
                        </a>
                        <p class="news-author">Đăng bởi: ${list.getAuthor()}</p>
                        <p>${list.getDay()} ngày trước</p>
                    </div>
                </div>
            </c:forEach>
        </div>
    </body>

</html>

