/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.payment;

import dao.CourseDAO;
import dao.InvoiceDAO;
import dto.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author pmt
 */
public class PaymentReturnServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PaymentReturnServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PaymentReturnServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the
    // + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            User u = null;
            int userID = -1;
            String enrollmentStatusMessage;

            if (request.getSession().getAttribute("customer") != null) {
                u = (User) request.getSession().getAttribute("customer");
                userID = u.getUserID();
            } else if (request.getSession().getAttribute("admin") != null
                    || request.getSession().getAttribute("expert") != null
                    || request.getSession().getAttribute("sales") != null) {
                // Redirect admin, expert, and sales roles to home page
                response.sendRedirect("home");
                return;
            }
            if (u != null) {

                CourseDAO courseDAO = new CourseDAO();
                PrintWriter out = response.getWriter();

                InvoiceDAO invoiceDAO = new InvoiceDAO();

                // parameter
                String invoiceID = request.getParameter("vnp_TransactionNo");
                String amount = request.getParameter("vnp_Amount");// output from vnp
                String price = amount.substring(0, amount.length() - 2);
                String description = request.getParameter("vnp_OrderInfo");
                String status;
                if (request.getParameter("vnp_ResponseCode").equals("00")) {
                    status = "1";
                } else {
                    status = "0";
                }

                // hard part
                Pattern courseIDPattern = Pattern.compile("Course ID: (\\d+)");// get courseID

                Matcher courseIDMatcher = courseIDPattern.matcher(description);

                String courseID = null;
                if (courseIDMatcher.find()) {
                    courseID = courseIDMatcher.group(1);
                }

                Map fields = new HashMap();
                for (Enumeration params = request.getParameterNames(); params.hasMoreElements();) {
                    String fieldName = (String) params.nextElement();
                    String fieldValue = request.getParameter(fieldName);
                    if ((fieldValue != null) && (fieldValue.length() > 0)) {
                        fields.put(fieldName, fieldValue);
                    }
                }

                String vnp_SecureHash = request.getParameter("vnp_SecureHash");
                if (fields.containsKey("vnp_SecureHashType")) {
                    fields.remove("vnp_SecureHashType");
                }
                if (fields.containsKey("vnp_SecureHash")) {
                    fields.remove("vnp_SecureHash");
                }
                String signValue = Config.hashAllFields(fields);

                if (signValue.equals(vnp_SecureHash)) {
                    if ("00".equals(request.getParameter("vnp_ResponseCode"))) {
                        out.print("GD Thanh cong");
                    } else {
                        out.print("GD Khong thanh cong");
                    }

                } else {
                    out.print("Chu ky khong hop le");
                }
                System.out.println("invoiceID" + invoiceID);
                System.out.println("price: " + price);
                System.out.println("status: " + status);
                System.out.println("des: " + description);
                if (status.equals("1")) {
                    courseDAO.addCourseEnroll(userID, courseID, 1);
                    invoiceDAO.addInvoice(invoiceID, userID, price, status, description, courseID);
                }
            }
            enrollmentStatusMessage = "Enroll successfully";
            request.getSession().setAttribute("enrollmentStatusMessage", enrollmentStatusMessage);
            request.getRequestDispatcher("view/user/payment/thanks.jsp").forward(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(PaymentReturnServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
