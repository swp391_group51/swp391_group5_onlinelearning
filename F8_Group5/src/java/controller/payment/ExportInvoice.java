/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.payment;

import java.io.*;
import dao.InvoiceDAO;
import dto.Invoice;
import dto.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author pmt
 */
public class ExportInvoice extends HttpServlet {

    User u = null;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        if (request.getSession().getAttribute("sales") != null) {
                u = (User) request.getSession().getAttribute("sales");
            } else if (request.getSession().getAttribute("customer") != null
                    || request.getSession().getAttribute("expert") != null
                    || request.getSession().getAttribute("admin") != null) {
                // Redirect admin, expert, and sales roles to home page
                response.sendRedirect("home");
                return;
            }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

        if (u != null) {

            try {
                InvoiceDAO invoiceDAO = new InvoiceDAO();
                List<Invoice> listInvoice = invoiceDAO.getListInvoice();

                // Get the user's home directory
                String userHome = System.getProperty("user.home");
                // Specify the "Downloads" directory path
                String downloadsDirectory = userHome + File.separator + "Downloads";

                // Create a file object with the desired file name
                String fileName = "invoice_export.xlsx";

                XSSFWorkbook workbook = new XSSFWorkbook();
                Sheet sheet = workbook.createSheet("Invoice Data");

                // Create a header row
                Row headerRow = sheet.createRow(0);
                headerRow.createCell(0).setCellValue("Invoice ID");
                headerRow.createCell(1).setCellValue("User ID");
                headerRow.createCell(2).setCellValue("Price");
                headerRow.createCell(3).setCellValue("Purchase Date");
                headerRow.createCell(4).setCellValue("Status");
                headerRow.createCell(5).setCellValue("Description");
                headerRow.createCell(6).setCellValue("Course ID");

                // Create a cell style for date format
                CellStyle dateCellStyle = workbook.createCellStyle();
                dateCellStyle.setDataFormat(workbook.getCreationHelper().createDataFormat().getFormat("yyyy-MM-dd"));

                // Populate Excel cells with invoice data
                int rowNumber = 1;
                for (Invoice invoice : listInvoice) {
                    Row row = sheet.createRow(rowNumber++);
                    row.createCell(0).setCellValue(invoice.getInvoice_ID());
                    row.createCell(1).setCellValue(invoice.getUser_UserID());
                    row.createCell(2).setCellValue(invoice.getPrice());
                    //Create and format the Purchase Date type
                    Cell dateCell = row.createCell(3);
                    dateCell.setCellValue(invoice.getCreateDate()); // Replace with your actual date value
                    dateCell.setCellStyle(dateCellStyle);

                    row.createCell(4).setCellValue(invoice.getStatus());
                    row.createCell(5).setCellValue(invoice.getDescription());
                    row.createCell(6).setCellValue(invoice.getCourse_CourseID());
                }

                File file = new File(downloadsDirectory + File.separator + fileName);
                System.out.println("File will be saved to: " + file.getAbsolutePath());

                try ( FileOutputStream outputStream = new FileOutputStream(file)) {
                    workbook.write(outputStream);
                }
                // Close the workbook
                workbook.close();

                System.out.println("Excel file created successfully!");
            } catch (Exception e) {
                // Handle exceptions
                e.printStackTrace();
            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");

        try ( PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>HTML Page</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Hello, this is an HTML page from the servlet!</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
