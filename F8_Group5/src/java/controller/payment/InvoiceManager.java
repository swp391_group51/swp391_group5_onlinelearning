/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.payment;

import dao.CourseDAO;
import dao.InvoiceDAO;
import dao.UserDAO;
import dto.Course;
import dto.Invoice;
import dto.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pmt
 */
public class InvoiceManager extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet InvoiceManager</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet InvoiceManager at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {

            User u = null;

            if (request.getSession().getAttribute("sales") != null) {
                u = (User) request.getSession().getAttribute("sales");
            } else if (request.getSession().getAttribute("customer") != null
                    || request.getSession().getAttribute("expert") != null
                    || request.getSession().getAttribute("admin") != null) {
                // Redirect admin, expert, and sales roles to home page
                response.sendRedirect("home");
                return;
            }

            if (u != null) {
                InvoiceDAO invoiceDAO = new InvoiceDAO();
                List<Invoice> listInvoice = invoiceDAO.getListInvoice();

                UserDAO userDAO = new UserDAO();
                List<User> listUser = userDAO.getAllUser();

                CourseDAO courseDAO = new CourseDAO();
                List<Course> listCourse = courseDAO.getAllCourse();

                int index = 1;
                int totalPage = 0;  // Initialize totalPage to 0 initially
                int itemPerPage = 3;

                if (!listInvoice.isEmpty()) {
                    totalPage = listInvoice.size() % itemPerPage == 0 ? (listInvoice.size() / itemPerPage) : ((listInvoice.size() / itemPerPage) + 1);
                }

                if (request.getParameter("page") != null) {
                    index = Integer.parseInt(request.getParameter("page"));
                }

                if (index < 1) {
                    index = 1;
                } else if (totalPage > 0 && index > totalPage) {
                    index = totalPage;
                }

                int startIndex = (index - 1) * itemPerPage;
                int endIndex = Math.min(startIndex + itemPerPage, listInvoice.size());
                List<Invoice> paginatedList = listInvoice.subList(startIndex, endIndex);

                request.setAttribute("totalPage", totalPage);
                request.setAttribute("index", index);
                request.setAttribute("listInvoice", paginatedList);
                request.setAttribute("listUser", listUser);
                request.setAttribute("listCourse", listCourse);

                request.getRequestDispatcher("/view/user/payment/invoiceManager.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("/view/user/login_register/login.jsp").forward(request, response);
            }
        } catch (SQLException ex) {
            Logger.getLogger(InvoiceManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {

            User u = null;

            if (request.getSession().getAttribute("sales") != null) {
                u = (User) request.getSession().getAttribute("sales");
            }

            if (u != null) {
                InvoiceDAO invoiceDAO = new InvoiceDAO();
                List<Invoice> listInvoice = invoiceDAO.getListInvoice();

                UserDAO userDAO = new UserDAO();
                List<User> listUser = userDAO.getAllUser();

                CourseDAO courseDAO = new CourseDAO();
                List<Course> listCourse = courseDAO.getAllCourse();

                int index = 1;
                int totalPage = 0;  // Initialize totalPage to 0 initially

                if (!listInvoice.isEmpty()) {
                    totalPage = listInvoice.size() % 10 == 0 ? (listInvoice.size() / 10) : ((listInvoice.size() / 10) + 1);
                }

                if (request.getParameter("page") != null) {
                    index = Integer.parseInt(request.getParameter("page"));
                }

                if (index < 1) {
                    index = 1;
                } else if (totalPage > 0 && index > totalPage) {
                    index = totalPage;
                }

                int startIndex = (index - 1) * 10;
                int endIndex = Math.min(startIndex + 10, listInvoice.size());
                List<Invoice> paginatedList = listInvoice.subList(startIndex, endIndex);

                request.setAttribute("totalPage", totalPage);
                request.setAttribute("index", index);
                request.setAttribute("listInvoice", paginatedList);
                request.setAttribute("listUser", listUser);
                request.setAttribute("listCourse", listCourse);

                request.getRequestDispatcher("/view/user/payment/invoiceManager.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("/view/user/login_register/login.jsp").forward(request, response);
            }
        } catch (SQLException ex) {
            Logger.getLogger(InvoiceManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
