/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.blog;

import dao.BlogDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import dto.User;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ADMIN
 */
public class Blog extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {

        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            BlogDAO dao = new BlogDAO();
//            ArrayList<dto.Blog> blogA = dao.getAllBlogg();
            int id = 0;
            try {

                id = Integer.parseInt(request.getParameter("id"));
            } catch (Exception e) {
            }
            String type = request.getParameter("type");
//            if (type != null && type.equals("0")) {
//                dao.delBlog(id);
//            } else 
            if (type != null && type.equals("1")) {
//                ArrayList<dto.Blog> blogList = dao.getAllBlogg();
//                for (dto.Blog b : blogList) {
//                    if (b.getBlogID() == id) {
//                        request.setAttribute("Blog", b);
//                    }
//                }
                request.setAttribute("Blog", new BlogDAO().getListBlogByID(id));
                request.setAttribute("id_blog", Integer.parseInt(request.getParameter("id")));

            }

//            request.setAttribute("bList", blogA);
            request.setAttribute("tags", dao.getListBlogTag());
            request.setAttribute("topics", dao.getListBlogTopic());
            request.getRequestDispatcher("view/user/blog/newblog.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(Blog.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String uploadFolder = request.getServletContext().getRealPath("assets/images/blog");
            Path uploadPath = Paths.get(uploadFolder);

            if (!Files.exists(uploadPath)) {
                Files.createDirectory(uploadPath); // neu chua ton tai foder postimg thi tao
            }
            Part imagePart = request.getPart("image"); // tra ve doi tuong file 'image'
            String imageFilename = Paths.get(imagePart.getSubmittedFileName()).getFileName().toString(); // lay ra ten cua file moi chon
            if (!imageFilename.isEmpty()) {
                imagePart.write(Paths.get(uploadPath.toString(), imageFilename).toString()); //save to foder 
            }
            BlogDAO dao = new BlogDAO();
            String action = request.getParameter("action");
            if (action != null && !action.isEmpty()) {
                String title = request.getParameter("title");
                String date = request.getParameter("date");
                String time = request.getParameter("time");
                String detail = request.getParameter("detail");
                int id_topic = Integer.parseInt(request.getParameter("id_topic"));
                int id_tag = Integer.parseInt(request.getParameter("id_tag"));
                String status = "";
                if (action.equals("save")) {
                    status = "0";
                } else if (action.equals("publish")) {
                    status = "1";
                }
                System.out.println(status);
                System.out.println(imageFilename);
                HttpSession session = request.getSession();
                User a = (User) session.getAttribute("expert");
                int uid = a.getUserID();
                String id = "";
                if (request.getParameter("blogid") != null) {
                    id = request.getParameter("blogid");
                }
                String[] avatar = imageFilename.split("\\.");

                request.setAttribute("tags", dao.getListBlogTag());

                request.setAttribute("topics", dao.getListBlogTopic());
                System.out.println(id);
                try {
                    if (id.isEmpty()) {
                        if (imageFilename != null && !imageFilename.equals("") && !avatar[1].equals("jpg") && !avatar[1].equals("png")) {
                            System.out.println("2");
                            request.setAttribute("errorUpload", "Ảnh chỉ được cho phép trong định dạng jpg, png");
                            request.getRequestDispatcher("view/user/blog/newblog.jsp").forward(request, response);
                        }
                        dao.insertBlog(title, imageFilename, date, time, detail, uid, status, id_tag, id_topic);
                        String insertMessage = (status == "1" ? "Release successully" : "Save to papar successfully");
                        request.setAttribute("insertMessage", insertMessage);
                        System.out.println(title + " " + imageFilename + " " + date + " " + time + " " + detail + " " + status + " " + id + " " + id_tag + " " + id_topic);
                    } else {
                        int ID = 0;
                        try {
                            ID = Integer.parseInt(id);
                        } catch (Exception e) {
                        }
                        

                        if (imageFilename != null && !imageFilename.equals("") && !avatar[1].equals("jpg") && !avatar[1].equals("png") ) {
                            request.setAttribute("Blog", new BlogDAO().getListBlogByID(ID));
                            request.setAttribute("errorUpload", "Ảnh chỉ được cho phép trong định dạng jpg, png");
                            request.getRequestDispatcher("view/user/blog/newblog.jsp").forward(request, response);
                        }
                        if (imageFilename.isEmpty()) {
                            imageFilename = dao.getListBlogByID(ID).getBlogImage();
                        }
                        System.out.println(status);
                        request.setAttribute("updateMessage", "Update successfully!!!");
                        dao.updateBlog(title, imageFilename, date, time, detail, status, ID, id_tag, id_topic);
                        request.setAttribute("Blog", new BlogDAO().getListBlogByID(ID));

                    }

                    request.setAttribute("tags", dao.getListBlogTag());
                    request.setAttribute("topics", dao.getListBlogTopic());
                } catch (SQLException ex) {
                    Logger.getLogger(Blog.class.getName()).log(Level.SEVERE, null, ex);
                }

                request.getRequestDispatcher("view/user/blog/newblog.jsp").forward(request, response);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Blog.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
