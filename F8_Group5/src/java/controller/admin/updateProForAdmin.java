/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin;

import controller.course.UpdateCourse;
import dao.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import dto.User;
import jakarta.servlet.http.Part;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 *
 * @author admin
 */
public class updateProForAdmin extends HttpServlet {

    private UserDAO userDao = new UserDAO();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdateProfile</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UpdateProfile at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        try {
            String UserID = request.getParameter("id");
            UserDAO userDAO = new UserDAO();
            User user = userDAO.getUserbyUserID(UserID);
            request.setAttribute("user", user);
            request.getRequestDispatcher("/view/admin/updateProAdmin.jsp").forward(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(updateProForAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            User u = null;
            //set session cho tung doi tuong
            if (request.getSession().getAttribute("admin") != null) {
                u = (User) request.getSession().getAttribute("admin");
            }
            if (u != null) {
                boolean check = true;
                String FullName = request.getParameter("Name");
                String Image = request.getParameter("Image");
                String Bio = request.getParameter("Bio");
                String BackGroundImage = request.getParameter("BackGroundImage");
                String UserID = request.getParameter("UserID");
                String uploadFolder = request.getServletContext().getRealPath("assets/images/userpicture");
                File imageFolder = new File(uploadFolder);
                if (!imageFolder.exists()) {
                    imageFolder.mkdirs();
                }

                Part imageAva = request.getPart("Image"); // tra ve doi tuong file 'image'
                Part bgPart = request.getPart("BackGroundImage");

                String imageFilename = Paths.get(imageAva.getSubmittedFileName()).getFileName().toString(); // lay ra ten cua file moi chon
                String bgFilename = Paths.get(bgPart.getSubmittedFileName()).getFileName().toString();

                InputStream inputStream = imageAva.getInputStream();
                InputStream inputStreamBg = bgPart.getInputStream();
                File targetFile = new File(imageFolder, imageFilename);
                File targetFileBg = new File(imageFolder, bgFilename);
                if (!targetFile.exists()) {
                    Files.copy(inputStream, targetFile.toPath());
                }
                if (!targetFileBg.exists()) {
                    Files.copy(inputStreamBg, targetFileBg.toPath());

                }
                //lay ra ten file anh vi du nhu la abc.jpg cha han sau do lay duoi sau dau cham se dc jpg là cái dinh dang anh  

                System.out.println(imageFilename);
                System.out.println(bgFilename);
                UserDAO userDAO = new UserDAO();

                String[] avatar = imageFilename.split("\\.");
                String[] background = bgFilename.split("\\.");
                System.out.println(imageFilename);
                if (imageFilename != null && !imageFilename.equals("") && !avatar[1].equals("jpg")) {
                    check = false;
                    request.setAttribute("errorUpload", "Ảnh chỉ được cho phép trong định dạng jpg");
                }
                if (bgFilename != null && !bgFilename.equals("") && !background[1].equals("jpg")) {
                    check = false;
                    request.setAttribute("errorUploadBg", "Ảnh chỉ được cho phép trong định dạng jpg");
                }
                if (imageFilename.isEmpty()) {
                    imageFilename = userDAO.getUser(Integer.parseInt(UserID)).getImage();
                }
                if (bgFilename.isEmpty()) {
                    bgFilename = userDAO.getUser(Integer.parseInt(UserID)).getImage();
                }
                if (check == true) { // Neu dung dinh dang anh

                    userDAO.UpdateLogin1(FullName, imageFilename, Bio, bgFilename,UserID);
                    request.setAttribute("messSuccess", "Update Profile success");
                    response.sendRedirect("updateProForAdmin?id=" + UserID + "&messSuccess=Update+Profile+success");
                } else {

                    User user = userDAO.getUserbyUserID(UserID);
                    request.setAttribute("user", user);
                    System.out.println("hi");
                    response.sendRedirect("list-user");
                }
            } else {
                request.getRequestDispatcher("/view/user/login_register/login.jsp").forward(request, response);
            }

        } catch (SQLException ex) {
            Logger.getLogger(UpdateCourse.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
