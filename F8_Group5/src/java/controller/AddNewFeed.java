/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.NewFeedDAO;
import dao.UserDAO;
import dto.NewFeed;
import dto.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hanh
 */
public class AddNewFeed extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            int adminid = Integer.parseInt(request.getParameter("AdminID"));
            request.setAttribute("AdminID", adminid);
            request.getRequestDispatcher("/view/admin/AddNewFeed.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String uploadFolder = request.getServletContext().getRealPath("assets/images/NewFeed");
        Path uploadPath = Paths.get(uploadFolder);
        if (!Files.exists(uploadPath)) {
            Files.createDirectory(uploadPath); // neu chua ton tai foder postimg thi tao
        }
        Part imagePart = request.getPart("Image"); // tra ve doi tuong file 'image'
        String imageFilename = Paths.get(imagePart.getSubmittedFileName()).getFileName().toString();
        String Title = request.getParameter("Title");
        String Description = request.getParameter("Description");
        String youtube = request.getParameter("Youtube");
        String UserID = request.getParameter("AdminID");
        User user = new User();
        UserDAO userdao = new UserDAO();
        try {
            user = userdao.getUserbyUserID(UserID);
        } catch (SQLException ex) {
            Logger.getLogger(AddNewFeed.class.getName()).log(Level.SEVERE, null, ex);
        }
        String fullname = user.getFullName();
        NewFeedDAO newfeeddao = new NewFeedDAO();
        newfeeddao.addnewfeed(Title,Description, imageFilename, youtube, fullname, LocalDateTime.now());
        System.out.println(Title + " " + Description + " " + imageFilename + " " + youtube + " " + fullname +" " + LocalDateTime.now());
        response.sendRedirect("newfeedmanager");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
