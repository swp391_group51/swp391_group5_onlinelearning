/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.course;

import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import dao.LessonDAO;
import dto.Lesson;
import jakarta.servlet.http.Part;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 *
 * @author Hanh
 */
public class UpdateLesson extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdateLesson</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UpdateLesson at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int lessonid = Integer.parseInt(request.getParameter("LessonID"));
        LessonDAO lessondao = new LessonDAO();
        Lesson lesson = new Lesson();
        lesson = lessondao.getLessonbyID(lessonid);
        request.setAttribute("lesson", lesson);
        request.getRequestDispatcher("view/user/lesson/UpdateLesson.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String videoNameWithoutExtension = "";
        try {
            String uploadFolder = request.getServletContext().getRealPath("assets/lesson");
            Path uploadPath = Paths.get(uploadFolder);

            if (!Files.exists(uploadPath)) {
                Files.createDirectory(uploadPath); // Create the directory if it doesn't exist
            }

            Part videoPart = request.getPart("Video"); // Get the video Part
            String videoFilename = Paths.get(videoPart.getSubmittedFileName()).getFileName().toString();

            if (!videoFilename.isEmpty()) {
                // Remove the ".mp4" extension from the filename
                videoNameWithoutExtension = videoFilename.replaceFirst("[.][^.]+$", "");

                // Save the video file to the upload directory
                videoPart.write(Paths.get(uploadPath.toString(), videoFilename).toString());

                // Print the video name without extension
                System.out.println("Video Name Without Extension: " + videoNameWithoutExtension);
            }
            int LessonID = Integer.parseInt(request.getParameter("LessonID"));
            String LessonName = request.getParameter("Name");
            System.out.println("LessonID: " + LessonID);
            System.out.println("LessonName: " + LessonName);
            LessonDAO lessonDAO = new LessonDAO();
            lessonDAO.UpdateLesson(LessonID, LessonName, videoNameWithoutExtension);
            int CourseID = lessonDAO.getLessonbyID(LessonID).getCourseID();
            System.out.println(CourseID);
            response.sendRedirect(request.getContextPath() + "/lessonManager?CourseId=" + CourseID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
