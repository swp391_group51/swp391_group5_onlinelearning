/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.course;

import dao.LessonDAO;
import dto.Lesson;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import dao.CourseDAO;
/**
 *
 * @author Hanh
 */
public class AddLesson extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddLesson</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddLesson at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CourseDAO course = new CourseDAO();
        request.setAttribute("course", course);
        request.getRequestDispatcher("view/user/lesson/AddLesson.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String videoNameWithoutExtension = "";
        try {
            String uploadFolder = request.getServletContext().getRealPath("assets/lesson");
            Path uploadPath = Paths.get(uploadFolder);

            if (!Files.exists(uploadPath)) {
                Files.createDirectory(uploadPath); // Create the directory if it doesn't exist
            }

            Part videoPart = request.getPart("Video"); // Get the video Part
            String videoFilename = Paths.get(videoPart.getSubmittedFileName()).getFileName().toString();

            if (!videoFilename.isEmpty()) {
                // Remove the ".mp4" extension from the filename
                videoNameWithoutExtension = videoFilename.replaceFirst("[.][^.]+$", "");

                // Save the video file to the upload directory
                videoPart.write(Paths.get(uploadPath.toString(), videoFilename).toString());

                // Print the video name without extension
                System.out.println("Video Name Without Extension: " + videoNameWithoutExtension);
            }
            int CourseID = Integer.parseInt(request.getParameter("CourseID"));
            String LessonName = request.getParameter("Name");
            LessonDAO lessonDAO = new LessonDAO();
            int LessonID = lessonDAO.SetNextLessonID();
            lessonDAO.AddLesson(LessonID, LessonName, videoNameWithoutExtension, CourseID);
            response.sendRedirect(request.getContextPath() + "/lessonManager");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
