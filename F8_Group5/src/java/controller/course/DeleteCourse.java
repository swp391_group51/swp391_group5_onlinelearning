/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.course;

import dao.CourseDAO;
import dto.Course;
import dto.User;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author admin
 */
public class DeleteCourse extends HttpServlet {

    User u = null;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        if (request.getSession().getAttribute("admin") != null) {
                u = (User) request.getSession().getAttribute("admin");
            } else if (request.getSession().getAttribute("customer") != null
                    || request.getSession().getAttribute("expert") != null
                    || request.getSession().getAttribute("sales") != null) {
                // Redirect admin, expert, and sales roles to home page
                response.sendRedirect("home");
                return;
            }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        try {

            if (u != null) {
                String courseID = request.getParameter("CourseID");
                CourseDAO dao = new CourseDAO();

                //delete image in directory
                Course c = dao.getCourseByID(courseID);

                if (c != null) {
                    String imageFilename = c.getImage();
                    String uploadFolder = request.getServletContext().getRealPath("/assets/images/course");
                    Path imagePath = Paths.get(uploadFolder, imageFilename);

                    if (Files.exists(imagePath)) {
//                        Files.delete(imagePath); //if delete course without image, it shows error 500
                        dao.deleteCourse(courseID);
                        response.sendRedirect("courseManager");
                    } else {
                        // Image doesn't exist
                        dao.deleteCourse(courseID);
                        response.sendRedirect("courseManager");
                    }
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(DeleteCourse.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
