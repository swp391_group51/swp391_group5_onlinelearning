/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.course;

import dao.CategoryDAO;
import dao.CourseDAO;
import dao.UserDAO;
import dto.Category;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import dto.Course;
import dto.User;
import jakarta.servlet.http.Part;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author admin
 */
public class UpdateCourse extends HttpServlet {

    User u = null;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        if (request.getSession().getAttribute("admin") != null) {
            u = (User) request.getSession().getAttribute("admin");
        } else if (request.getSession().getAttribute("customer") != null
                || request.getSession().getAttribute("expert") != null
                || request.getSession().getAttribute("sales") != null) {
            // Redirect admin, expert, and sales roles to home page
            response.sendRedirect("home");
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

        try {
            if (u != null) {
                String CourseID = request.getParameter("CourseID");
                CourseDAO CourseDAO = new CourseDAO();
                CategoryDAO CategoryDAO = new CategoryDAO();
                Course c = CourseDAO.getCourseByID(CourseID);
                UserDAO udao = new UserDAO();
                List<User> listUser = udao.getAllUser();
                List<Category> listCategory = CategoryDAO.ListAllCategory();
                request.setAttribute("listUser", listUser);
                request.setAttribute("course", c);
                request.setAttribute("listCategory", listCategory);
                request.getRequestDispatcher("view/user/course/updateCourse.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("/view/user/login_register/login.jsp").forward(request, response);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UpdateCourse.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);

        if (u != null) {
            try {
                String id = request.getParameter("id");
                String name = request.getParameter("name");

                String info = request.getParameter("info");
                String des = request.getParameter("des");
                String status = request.getParameter("Status");
                String uid = request.getParameter("uid");
                String cid = request.getParameter("cid");
                String price = request.getParameter("Price");

                String price_type = request.getParameter("price-type");

                if (price == null) {
                    price = "0";
                }

                CourseDAO CourseDAO = new CourseDAO();
                Course c = CourseDAO.getCourseByID(id);

                String uploadFolder = request.getServletContext().getRealPath("assets/images/course");
                Path uploadPath = Paths.get(uploadFolder);

                Part imagePart = request.getPart("image"); // Get the uploaded image part
                String imageFilename = Paths.get(imagePart.getSubmittedFileName()).getFileName().toString(); // Get the submitted image filename

                if (!imageFilename.isEmpty()) {
                    // Generate a new unique filename
                    String renameImage = price_type + "c" + id + ".jpg";
                    String newImageName = request.getServletContext().getRealPath("assets/images/course") + renameImage;
                    Path newImagePath = Paths.get(newImageName); // New image path to delete

                    if (!Files.exists(uploadPath)) {
                        Files.createDirectory(uploadPath); // Create the upload directory if it doesn't exist
                    } else {
                        if (Files.exists(newImagePath)) { // If a file with the new name already exists, delete it
                            Files.delete(newImagePath);
                        }
                        imageFilename = renameImage; // Set the new filename to the renamed version
                    }

                    imagePart.write(Paths.get(uploadPath.toString(), imageFilename).toString()); // Save the new image to the folder
                } else {
                    imageFilename = c.getImage(); // Keep the existing image filename
                }

                System.out.println(id);

                CourseDAO.updateCourse(name, imageFilename, info, des, status, uid, cid, price, id);
//                request.setAttribute("messSuccess", "Update Course success");
                response.sendRedirect("courseManager");
            } catch (SQLException ex) {
                Logger.getLogger(UpdateCourse.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            request.getRequestDispatcher("/view/user/login_register/login.jsp").forward(request, response);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
