/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.course;

import controller.blog.BlogManager;
import dao.CategoryDAO;
import dao.CourseDAO;
import dao.UserDAO;
import dto.Category;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import dto.Course;
import dto.User;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author admin
 */
public class CourseManager extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CourseManager</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CourseManager at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {

            User u = null;

            if (request.getSession().getAttribute("admin") != null) {
                u = (User) request.getSession().getAttribute("admin");
            } else if (request.getSession().getAttribute("customer") != null
                    || request.getSession().getAttribute("expert") != null
                    || request.getSession().getAttribute("sales") != null) {
                // Redirect admin, expert, and sales roles to home page
                response.sendRedirect("home");
                return;
            }
            if (u != null) {
                CourseDAO dao = new CourseDAO();
                UserDAO UserDAO = new UserDAO();
                CategoryDAO CategoryDAO = new CategoryDAO();

                List<Course> list = dao.getAllCourse();
                List<User> listUser = UserDAO.getAllUser();
                List<Category> listCategory = CategoryDAO.ListAllCategory();

                int index = 1;
                int totalPage = 0;  // Initialize totalPage to 0 initially

                if (!list.isEmpty()) {
                    totalPage = list.size() % 5 == 0 ? (list.size() / 5) : ((list.size() / 5) + 1);
                }

                if (request.getParameter("page") != null) {
                    index = Integer.parseInt(request.getParameter("page"));
                }

                if (index < 1) {
                    index = 1;
                } else if (totalPage > 0 && index > totalPage) {
                    index = totalPage;
                }

                int startIndex = (index - 1) * 5;
                int endIndex = Math.min(startIndex + 5, list.size());
                List<Course> paginatedList = list.subList(startIndex, endIndex);

                request.setAttribute("totalPage", totalPage);
                request.setAttribute("index", index);
                request.setAttribute("listCourse", paginatedList);

                request.setAttribute("listUser", listUser);
                request.setAttribute("listCategory", listCategory);

                request.getRequestDispatcher("/view/user/course/courseManager.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("/view/user/login_register/login.jsp").forward(request, response);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BlogManager.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String search = request.getParameter("search").trim();
            CourseDAO CourseDAO = new CourseDAO();
            List<Course> list = CourseDAO.getCourseBySearch(search);
            UserDAO UserDAO = new UserDAO();
            CategoryDAO CategoryDAO = new CategoryDAO();

            System.out.println(search);

            int index = 1;
            int totalPage = 0;  // Initialize totalPage to 0 initially

            if (!list.isEmpty()) {
                totalPage = list.size() % 5 == 0 ? (list.size() / 5) : ((list.size() / 5) + 1);
            }

            if (request.getParameter("page") != null) {
                index = Integer.parseInt(request.getParameter("page"));
            }

            if (index < 1) {
                index = 1;
            } else if (totalPage > 0 && index > totalPage) {
                index = totalPage;
            }

            int startIndex = (index - 1) * 5;
            int endIndex = Math.min(startIndex + 5, list.size());
            List<Course> paginatedList = list.subList(startIndex, endIndex);

            request.setAttribute("totalPage", totalPage);
            request.setAttribute("index", index);
            request.setAttribute("listCourse", paginatedList);
            request.setAttribute("search", search);

            User u = null;

            if (request.getSession().getAttribute("admin") != null) {
                u = (User) request.getSession().getAttribute("admin");
            }
            if (u != null) {
                List<Category> listCategory = CategoryDAO.ListAllCategory();
                List<User> listUser = UserDAO.getAllUser();

                request.setAttribute("listUser", listUser);
                request.setAttribute("listCategory", listCategory);

                request.getRequestDispatcher("/view/user/course/courseManager.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("/view/user/login_register/login.jsp").forward(request, response);
            }

        } catch (Exception ex) {
            Logger.getLogger(CourseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
