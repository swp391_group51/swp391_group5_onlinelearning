package controller.course;

import dao.CategoryDAO;
import dto.Category;
import dto.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hanh
 */
public class trainingManager extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servletspecific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF8");
        try ( PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet trainingManager</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet trainingManager at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editorfold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servletspecific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        User u = null;
        if (request.getSession().getAttribute("customer") != null) {
            u = (User) request.getSession().getAttribute("customer");
        } else if (request.getSession().getAttribute("admin") != null) {
            u = (User) request.getSession().getAttribute("admin");
        }
        if (u != null) {
            CategoryDAO cate = new CategoryDAO();
            ArrayList<Category> category = new ArrayList<>();
            try {
                category = cate.ListAllCategory();
            } catch (SQLException ex) {
                Logger.getLogger(trainingManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.setAttribute("cate", category);
            request.getRequestDispatcher("/view/admin/trainingManager.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("/view/user/login_register/login.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servletspecific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String categoryId = request.getParameter("categoryId");
        int categoryIdInt = Integer.parseInt(categoryId);
        CategoryDAO cate = new CategoryDAO();
        ArrayList<Category> category = new ArrayList<>();
        cate.toggleCategoryStatus(categoryIdInt);
        try {
            category = cate.ListAllCategory();
        } catch (SQLException ex) {
            Logger.getLogger(trainingManager.class.getName()).log(Level.SEVERE, null, ex);
        }

        request.setAttribute(
                "cate", category);
        request.getRequestDispatcher(
                "/view/admin/trainingManager.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editorfold>

}
