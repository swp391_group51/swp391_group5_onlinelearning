/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.course;

import dao.CategoryDAO;
import dao.CourseDAO;
import dto.Category;
import dto.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author trank
 */
public class AddProgramTraining extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            User u = null;

            if (request.getSession().getAttribute("admin") != null) {
                u = (User) request.getSession().getAttribute("admin");
            }

            if (u != null) {
                CategoryDAO dao = new CategoryDAO();
                List<Category> cateA = dao.ListAllCategory();

                String type = request.getParameter("type");
                request.setAttribute("Category", cateA);
                request.getRequestDispatcher("view/user/course/addProgram.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("/view/user/login_register/login.jsp").forward(request, response);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UpdateProgramTraining.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(AddProgramTraining.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String uploadFolder = request.getServletContext().getRealPath("assets/images/TrainingProgram");
        Path uploadPath = Paths.get(uploadFolder);
        if (!Files.exists(uploadPath)) {
            Files.createDirectory(uploadPath); // neu chua ton tai foder postimg thi tao
        }
        Part imagePart = request.getPart("Image"); // tra ve doi tuong file 'image'
        String imageFilename = Paths.get(imagePart.getSubmittedFileName()).getFileName().toString(); // lay ra ten cua file moi chon
        if (!imageFilename.isEmpty()) {
            imagePart.write(Paths.get(uploadPath.toString(), imageFilename).toString()); //save to foder 
        }
        CategoryDAO cate = new CategoryDAO();
        String Name = request.getParameter("Name");
        String Description = request.getParameter("Description");
        int id = 0;
        try {
            id = cate.CountID() + 1;
        } catch (SQLException ex) {
            Logger.getLogger(AddProgramTraining.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            cate.addCategory(id, Name, imageFilename, Description, "0");
        } catch (SQLException ex) {
            Logger.getLogger(AddProgramTraining.class.getName()).log(Level.SEVERE, null, ex);
        }
        response.sendRedirect(request.getContextPath() + "/trainingManager");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
