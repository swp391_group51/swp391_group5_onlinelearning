/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.course;

import dao.CategoryDAO;
import dao.CourseDAO;
import dao.UserDAO;
import dto.Category;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import dto.Course;
import dto.User;
import jakarta.servlet.http.Part;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author admin
 */
public class AddCourse extends HttpServlet {

    User u = null;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        if (request.getSession().getAttribute("admin") != null) {
            u = (User) request.getSession().getAttribute("admin");
        } else if (request.getSession().getAttribute("customer") != null
                || request.getSession().getAttribute("expert") != null
                || request.getSession().getAttribute("sales") != null) {
            // Redirect admin, expert, and sales roles to home page
            response.sendRedirect("home");
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

        try {
            if (u != null) {

                String UserID = request.getParameter("User_UserID");

                UserDAO UserDAO = new UserDAO();
                CategoryDAO CategoryDAO = new CategoryDAO();

                List<Category> listCategory = CategoryDAO.ListAllCategory();
                request.setAttribute("listCategory", listCategory);

                List<User> listUser = UserDAO.getAllUser();
                User user = UserDAO.getUserbyUserID(UserID);

                request.setAttribute("listUser", listUser);
                request.setAttribute("user", user);

                request.getRequestDispatcher("/view/user/course/addCourse.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("/view/user/login_register/login.jsp").forward(request, response);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AddCourse.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);
        CourseDAO CourseDAO = new CourseDAO();
        CategoryDAO categoryDAO = new CategoryDAO();
        if (u != null) {
            try {

                int id = CourseDAO.getLatestID() + 1;
                String name = request.getParameter("Name");
                String info = request.getParameter("CourseInfo");
                String description = request.getParameter("Description");
                String status = request.getParameter("Status");
                String User_UserID = request.getParameter("User_UserID");
                String Category_CategoryID = request.getParameter("Category_CategoryID");
                String price = request.getParameter("Price");
                if (price == null) {
                    price = "0";
                }

                List<Category> listCategory = categoryDAO.ListAllCategory();
                request.setAttribute("listCategory", listCategory);

                String price_type = request.getParameter("price-type");

                Course c = CourseDAO.checkExistCourse(name);
                if (c == null) {

                    String uploadFolder = request.getServletContext().getRealPath("assets/images/course");
                    Path uploadPath = Paths.get(uploadFolder);

                    Part imagePart = request.getPart("image"); // tra ve doi tuong file 'image'
                    String imageFilename = Paths.get(imagePart.getSubmittedFileName()).getFileName().toString(); // lay ra ten cua file moi chon
                    if (!imageFilename.isEmpty()) {
                        // Generate a new unique filename
                        String renameImage = price_type + "c" + id + ".jpg";

                        String newImageName = request.getServletContext().getRealPath("assets/images/course") + renameImage;
                        Path newImagePath = Paths.get(newImageName); //new image path to delete

                        if (!Files.exists(uploadPath)) {
                            Files.createDirectory(uploadPath); // neu chua ton tai foder postimg thi tao
                        } else {
                            if (Files.exists(newImagePath)) { //neu ton tai file giong ten file cu thi xoa
                                Files.delete(newImagePath);
                            }
                            imageFilename = renameImage;
                            imagePart.write(Paths.get(uploadPath.toString(), imageFilename).toString()); //save to foder 
                        }
                    } else {
                        // Set a default image filename
                        String defaultImageFilename = "default.jpg"; // Update this with your actual default image filename

                        // You might need to adjust the paths accordingly
                        String newImageName = request.getServletContext().getRealPath("assets/images/course") + defaultImageFilename;
                        Path newImagePath = Paths.get(newImageName);

                        imageFilename = defaultImageFilename;
                        System.out.println("Upload Path: " + uploadPath);
                        System.out.println("Image Filename: " + imageFilename);

                    }

                    CourseDAO.addCourse(id, name, imageFilename, info, description, status, User_UserID, Category_CategoryID, price);
                    request.setAttribute("messSuccess", "Add new Course success");
                    request.getRequestDispatcher("/view/user/course/addCourse.jsp").forward(request, response);

                } else {
                    request.setAttribute("mess", "Course name already exist");
                    request.getRequestDispatcher("/view/user/course/addCourse.jsp").forward(request, response); //error cant load data from database
                }

            } catch (SQLException ex) {
                Logger.getLogger(AddCourse.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            request.getRequestDispatcher("/view/user/login_register/login.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
