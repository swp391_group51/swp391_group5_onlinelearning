/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.course;

import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import dao.LessonDAO;
import java.util.ArrayList;
import dto.Lesson;
import dao.CourseDAO;
import java.awt.BorderLayout;
import java.util.List;
/**
 *
 * @author Hanh
 */
public class LessonManager extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LessonManager</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LessonManager at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        CourseDAO course = new CourseDAO();
        LessonDAO lesson = new LessonDAO();
        ArrayList<Lesson> lessonlist = new ArrayList<>();
        lessonlist = lesson.listallLesson();
        int index = 1;

            int totalPage = lessonlist.size() % 5 == 0 ? (lessonlist.size() / 5) : ((lessonlist.size() / 5) + 1);

            if (request.getParameter("page") != null) {
                index = Integer.parseInt(request.getParameter("page"));
            }

            if (index < 1) {
                index = 1;
            } else if (index > totalPage) {
                index = totalPage;
            }

            int startIndex = (index - 1) * 5;
            int endIndex = Math.min(startIndex + 5, lessonlist.size());
            List<Lesson> paginatedList = lessonlist.subList(startIndex, endIndex);

            request.setAttribute("totalPage", totalPage);
            request.setAttribute("index", index);
        request.setAttribute("course",course);
        request.setAttribute("list", lessonlist);
        request.setAttribute("listLesson", paginatedList);
        request.getRequestDispatcher("view/user/lesson/lesson.jsp").forward(request, response);
    } 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        request.getRequestDispatcher("view/lesson/lesson.jsp").forward(request, response);
    }
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
