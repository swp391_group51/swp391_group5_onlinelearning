/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package utils;

/**
 *
 * @author ADMIN
 */
public class Constants {

    // Google
    public static String GOOGLE_CLIENT_ID = "474696467188-912h3an6csb4sen30chpstfou05t4o77.apps.googleusercontent.com";
    public static String GOOGLE_CLIENT_SECRET = "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";
    public static String GOOGLE_REDIRECT_URI = "http://localhost:9999/F8_Group6/loginwithgoogle";
    public static String GOOGLE_LINK_GET_TOKEN = "https://accounts.google.com/o/oauth2/token";
    public static String GOOGLE_LINK_GET_USER_INFO = "https://www.googleapis.com/oauth2/v1/userinfo?access_token=";
    public static String GOOGLE_GRANT_TYPE = "authorization_code";
    // Facebook
    public static String FACEBOOK_APP_ID = "846580123743218";
    public static String FACEBOOK_APP_SECRET = "431767c2d772d41045a8a42999b95d00";
    public static String FACEBOOK_REDIRECT_URL = "http://localhost:9999/F8_Group6/login-facebook";
    public static String FACEBOOK_LINK_GET_TOKEN = "https://graph.facebook.com/oauth/access_token?client_id=%s&client_secret=%s&redirect_uri=%s&code=%s";
    // Github
    public static String GIT_CLIENT_ID = "7546e46ad9badbdb19fe";
    public static String GIT_CLIENT_SECRET = "9c72f494060a28b5e211d11e3cd3ecfc8be017ab";
    public static String GIT_REDIRECT_URI = "http://localhost:9999/F8_Group6/login-github";
    public static String GIT_LINK_GET_TOKEN = "https://github.com/login/oauth/access_token?";
    public static String GIT_LINK_GET_USER_INFO = "https://api.github.com/user";
}
