/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package utils;

import dao.UserDAO;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.List;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author ADMIN
 */
public class MD5 {

    public String getMD5Password(String password) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(password.getBytes());
        byte[] digest = md.digest();
        return DatatypeConverter
                .printHexBinary(digest).toUpperCase();
    }

    public String findEmail(String email) throws NoSuchAlgorithmException, SQLException {
         List<String> emails = new UserDAO().getAllEmails();
        for (int i = 0; i < emails.size(); i++) {
            String eHash = new MD5().getMD5Password(emails.get(i));
            if (eHash.equals(email)) {
                email = emails.get(i);
                break;
            }
        }
        return email;
    }

    public static void main(String[] args) throws NoSuchAlgorithmException, SQLException {
        System.out.println(new MD5().findEmail("phongcahaiduong@gmail.com"));
    }
}
