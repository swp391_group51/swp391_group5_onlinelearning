/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import dto.Lesson;
import java.sql.SQLException;
import utils.DBContext;

/**
 *
 * @author Hanh
 */
public class LessonDAO extends DBContext {

    int LessonID;
    String Name;
    String Video;
    int CourseID;

    public LessonDAO() {
    }

    public LessonDAO(int LessonID, String Name, String Video, int CourseID) {
        this.LessonID = LessonID;
        this.Name = Name;
        this.Video = Video;
        this.CourseID = CourseID;
    }

    public int getCourseID() {
        return CourseID;
    }

    public void setCourseID(int CourseID) {
        this.CourseID = CourseID;
    }

    public int getLessonID() {
        return LessonID;
    }

    public void setLessonID(int LessonID) {
        this.LessonID = LessonID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getVideo() {
        return Video;
    }

    public void setVideo(String Video) {
        this.Video = Video;
    }

    PreparedStatement ps = null;
    ResultSet rs = null;

    public ArrayList<Lesson> listallLesson(int CourseID) {
        ArrayList<Lesson> lessonlist = new ArrayList<>();
        String sql = "SELECT * FROM lesson where Course_CourseID = ?";
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, CourseID);
            rs = ps.executeQuery();
            while (rs.next()) {
                lessonlist.add(new Lesson(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4)));
            }
            return lessonlist;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lessonlist;
    }
    
    public ArrayList<Lesson> listallLesson() {
        ArrayList<Lesson> lessonlist = new ArrayList<>();
        String sql = "SELECT * FROM lesson";
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                lessonlist.add(new Lesson(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4)));
            }
            return lessonlist;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lessonlist;
    }

    public void DeleteLesson(int LessonID) {
        String sql = "Delete from lesson where lessonid = ?";
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, LessonID);
            int i = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Lesson getLessonbyID(int LessonID) {
        String sql = "select * from lesson where lessonid = ?";
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, LessonID);
            rs = ps.executeQuery();
            while (rs.next()) {
                return new Lesson(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void UpdateLesson(int LessonID, String LessonName, String Video) {
        String sql = "update lesson set name = ? , video = ? where lessonid = ?";
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, LessonName);
            ps.setString(2, Video);
            ps.setInt(3, LessonID);
            int i = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void AddLesson(int LessonID, String LessonName, String Video, int CourseID) {
        String sql = "INSERT INTO lesson VALUES (?, ?, ?, ?)";
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, LessonID);
            ps.setString(2, LessonName);
            ps.setString(3, Video);
            ps.setInt(4, CourseID); // Set the correct parameter for CourseID
            int rowsAffected = ps.executeUpdate();

            if (rowsAffected > 0) {
                System.out.println("Lesson added successfully.");
            } else {
                System.out.println("No rows added.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int SetNextLessonID() {
        String sql = "select max(LessonID) from lesson";
        int i = 0;
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                i = rs.getInt(1) + 1;
            }
            return i;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
