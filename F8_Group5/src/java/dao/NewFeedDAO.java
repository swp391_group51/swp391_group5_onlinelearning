/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import dto.NewFeed;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import utils.DBContext;

/**
 *
 * @author Hanh
 */
public class NewFeedDAO extends DBContext {

    int FeedID;
    String FeedTitle;
    String Description;
    String Image;
    String FeedVideo;
    String Author;
    private LocalDateTime timeCreated;
    
    public NewFeedDAO() {

    }

    public NewFeedDAO(int FeedID, String FeedTitle, String Description, String Image, String FeedVideo, String Author,LocalDateTime timeCreated) {
        this.FeedID = FeedID;
        this.FeedTitle = FeedTitle;
        this.Description = Description;
        this.Image = Image;
        this.FeedVideo = FeedVideo;
        this.Author = Author;
        this.timeCreated = timeCreated;
    }

    public LocalDateTime getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(LocalDateTime timeCreated) {
        this.timeCreated = timeCreated;
    }
    
    public String getImage() {
        return Image;
    }

    public void setImage(String Image) {
        this.Image = Image;
    }

    public int getFeedID() {
        return FeedID;
    }

    public void setFeedID(int FeedID) {
        this.FeedID = FeedID;
    }

    public String getFeedTitle() {
        return FeedTitle;
    }

    public void setFeedTitle(String FeedTitle) {
        this.FeedTitle = FeedTitle;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getFeedVideo() {
        return FeedVideo;
    }

    public void setFeedVideo(String FeedVideo) {
        this.FeedVideo = FeedVideo;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String Author) {
        this.Author = Author;
    }

    PreparedStatement ps = null;
    ResultSet rs = null;

    public ArrayList<NewFeed> listNewFeed() {
        ArrayList<NewFeed> feedlist = new ArrayList<>();
        String sql = "SELECT * FROM NewFeed";
        try {
            ps = connection.prepareCall(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                feedlist.add(new NewFeed(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getTimestamp(7).toLocalDateTime()));
            }
            return feedlist;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return feedlist;
    }
    public void addnewfeed(String Title, String Description, String Image, String FeedVideo, String Author, LocalDateTime CreateTime){
        String sql = "INSERT INTO newfeed (FeedTitle, Description, Image, FeedVideo, Author, CreateTime) VALUES (?,?,?,?,?,?)";
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, Title);
            ps.setString(2, Description);
            ps.setString(3, Image);
            ps.setString(4, FeedVideo);
            ps.setString(5, Author);
            ps.setTimestamp(6, Timestamp.valueOf(CreateTime));
            int i = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
