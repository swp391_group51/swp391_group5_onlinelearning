/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import dto.Invoice;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
//import java.util.Date;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import utils.DBContext;

/**
 *
 * @author pmt
 */
public class InvoiceDAO extends DBContext {

    PreparedStatement ps = null;
    ResultSet rs = null;

    public void addInvoice(String Invoice_ID, int User_UserID, String price, String Status, String Description, String Course_CourseID) throws SQLException {
        String sql = "insert into f8db.invoice values (?, ?, ?, CURRENT_DATE, ?, ?,?)";
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, Invoice_ID);
            ps.setInt(2, User_UserID);
            ps.setString(3, price);
            ps.setString(4, Status);
            ps.setString(5, Description);
            ps.setString(6, Course_CourseID);
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("Loi: " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
    }

    public List<Invoice> getListInvoice() throws SQLException {
        List<Invoice> listInvoice = new ArrayList<>();
        String sql = "SELECT * FROM f8db.invoice";
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                listInvoice.add(new Invoice(rs.getInt(1),
                        rs.getInt(2),
                        rs.getInt(3),
                        rs.getDate(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getInt(7)));
            }
        } catch (Exception e) {
            System.out.println("Loi: " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
        return listInvoice;
    }

}
