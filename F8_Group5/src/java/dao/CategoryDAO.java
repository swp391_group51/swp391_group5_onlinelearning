/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import dto.Category;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DBContext;

/**
 *
 * @author Hanh
 */
public class CategoryDAO extends DBContext {

    int CategoryID;
    String Name;
    String Image;
    String Description;
    int Status;

    PreparedStatement ps = null;
    ResultSet rs = null;

    public CategoryDAO(int CategoryID, String Name, String Description, String Image, int Status) {
        this.CategoryID = CategoryID;
        this.Name = Name;
        this.Description = Description;
        this.Image = Image;
        this.Status = Status;
    }

    public CategoryDAO() {
    }

    public int getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(int CategoryID) {
        this.CategoryID = CategoryID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String Image) {
        this.Image = Image;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    public ArrayList<Category> ListAllCategory() throws SQLException {
        ArrayList<Category> category = new ArrayList<>();
        String sql = "SELECT * FROM category;";
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                category.add(new Category(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return category;
    }

    public Category addCategory(int id, String name, String image, String des, String status) throws SQLException {
        System.out.println("ADD CATEGORY: " + id + " " + name);
        String sql = "insert into f8db.category(CategoryID,Name, image, Description, Status) values(?,?,?,?,?)";
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ps.setString(2, name);
            ps.setString(3, image);
            ps.setString(4, des);
            ps.setString(5, status);
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("Loi: " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
        return null;
    }

    public int CountID() throws SQLException {
        String sql = "SELECT COUNT(categoryid) FROM category";
        int count = 0;

        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery(); // Use executeQuery() here
            if (rs.next()) {
                count = rs.getInt(1);
                System.out.println(count);
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }

        return count;
    }

    public void toggleCategoryStatus(int categoryId) {
        try {
            String query = "SELECT Status FROM f8db.category WHERE CategoryID = ?";
            ps = connection.prepareStatement(query);
            ps.setInt(1, categoryId);

            rs = ps.executeQuery();
            if (rs.next()) {
                int currentStatus = rs.getInt("Status");
                int newStatus = (currentStatus == 1) ? 0 : 1;
                String updateSql = "UPDATE f8db.category SET Status = ? WHERE CategoryID = ?";
                ps = connection.prepareStatement(updateSql);
                ps.setInt(1, newStatus);
                ps.setInt(2, categoryId);
                ps.executeUpdate();

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public Category getCateByID(int id) throws SQLException {
        String sql = "select * from f8db.category where CategoryID =?";
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                return new Category(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5));
            }
        } catch (Exception e) {
            System.out.println("loi: " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }

        return null;
    }

    public void updateCategory(int id, String name, String image, String des) throws SQLException {
        System.out.println(id + " " + name + " " + des);
        String sql = "update category set Name = ?,image = ?,Description = ? where CategoryID = ?";
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, name);
            ps.setString(2, image);
            ps.setString(3, des);
            ps.setInt(4, id);
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("loi: " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
    }

    public void deleteCategory(int CategoryID) {
        String sql = "delete from category where categoryid = ?";
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, CategoryID);
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("loi: " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    Logger.getLogger(CategoryDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                    Logger.getLogger(CategoryDAO.class.getName()).log(Level.SEVERE, null, ex);
//                }
                }
            }
        }
    }

    public ArrayList<Category> getCateBySearch(String search) throws SQLException {
        ArrayList<Category> category = new ArrayList<>();
        String sql = "SELECT * FROM category where name like ?";
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1,"%" + search + "%");         
            rs = ps.executeQuery();
            while (rs.next()) {
                System.out.println(rs.getString(1));
                category.add(new Category(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return category;
    }
}
