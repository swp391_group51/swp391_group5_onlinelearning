/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import dto.Category;
import dto.Course;
import dto.CourseEnroll;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.sql.SQLException;
import utils.DBContext;

/**
 *
 * @author admin
 */
public class CourseDAO extends DBContext {

    PreparedStatement ps = null;
    ResultSet rs = null;

    public void addCourseEnroll(int userid, String courseid, int status) throws SQLException {
        String sql = "insert into f8db.courseenroll values (?,?,?,current_date());";
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, userid);
            ps.setString(2, courseid);
            ps.setInt(3, status);
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("Loi: " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
    }

    public boolean isEnrolled(int userid, String courseid) throws SQLException {
        boolean isEnrolled = false;

        try {
            String sql = "SELECT COUNT(*) AS count\n"
                    + "FROM f8db.courseenroll ce\n"
                    + "JOIN f8db.course c ON ce.Course_CourseID = c.CourseID\n"
                    + "WHERE ce.User_UserID = ? AND ce.Course_CourseID = ? AND c.Status = 1";
            ps = connection.prepareStatement(sql);
            ps.setInt(1, userid);
            ps.setString(2, courseid);

            rs = ps.executeQuery(); // Use executeQuery() to retrieve results

            if (rs.next()) {
                int count = rs.getInt("count");
                isEnrolled = count > 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // Handle exception
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return isEnrolled;
    }

    public List<CourseEnroll> getEnrollCourseList(int userid) throws SQLException {
        List<CourseEnroll> list = new ArrayList<>();
        String sql = "SELECT ce.User_UserID, ce.Course_CourseID, ce.Status, ce.EnrollDate\n"
                + "FROM f8db.courseenroll ce\n"
                + "JOIN f8db.course c ON ce.Course_CourseID = c.CourseID\n"
                + "WHERE ce.User_UserID = ? AND c.Status = 1;";
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, userid);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new CourseEnroll(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getDate(4)));
            }
        } catch (Exception e) {
            System.out.println("Loi: " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
        return list;
    }

    public List<Course> getAllCourse() throws SQLException {
        List<Course> list = new ArrayList<>();
        String sql = "select * from f8db.course";
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Course(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getInt(6),
                        rs.getInt(7),
                        rs.getInt(8),
                        rs.getInt(9)));
            }
        } catch (Exception e) {
            System.out.println("Loi: " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
        return list;
    }

    public Course checkExistCourse(String name) throws SQLException {
        String sql = "select *  from Course where Name LIKE ?";
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, "%" + name + "%");

            rs = ps.executeQuery();
            while (rs.next()) {
                return new Course(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getInt(6),
                        rs.getInt(7),
                        rs.getInt(8),
                        rs.getInt(9));
            }
        } catch (Exception e) {
            System.out.println("loi: " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
        return null;
    }

    public void addCourse(int id, String name, String image, String info, String des, String status, String uid, String cid, String price) throws SQLException {
        String sql = "insert into f8db.course(CourseID, Name, Image, CourseInfo, Description, Status, User_UserID, Category_CategoryID, price)\n"
                + "values(?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ps.setString(2, name);
            ps.setString(3, image);
            ps.setString(4, info);
            ps.setString(5, des);
            ps.setString(6, status);
            ps.setString(7, uid);
            ps.setString(8, cid);
            ps.setString(9, price);
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("Loi: " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
    }

    public void deleteCourse(String id) throws SQLException {
        String sql = "delete from f8db.course where CourseID =?";
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("Loi: " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }

    }

    public Course getCourseByID(String id) throws SQLException {
        String sql = "select * from f8db.course where CourseID =?";
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                return new Course(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getInt(6),
                        rs.getInt(7),
                        rs.getInt(8),
                        rs.getInt(9));
            }
        } catch (Exception e) {
            System.out.println("loi: " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }

        return null;
    }

    // update 
    public void updateCourse(String name, String image, String info, String des, String status, String uid, String cid, String price, String id) throws SQLException {
        String sql = "update f8db.course set Name = ?,\n"
                + "               Image = ?,\n"
                + "               CourseInfo = ?,\n"
                + "                Description = ?,\n"
                + "               Status = ?,\n"
                + "               User_UserID = ?,\n"
                + "                Category_CategoryID = ?, \n"
                + "                price = ?\n"
                + "                where CourseID = ?";
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, name);
            ps.setString(2, image);
            ps.setString(3, info);
            ps.setString(4, des);
            ps.setString(5, status);
            ps.setString(6, uid);
            ps.setString(7, cid);
            ps.setString(8, price);
            ps.setString(9, id);
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("loi: " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
    }

    public void toggleCategoryStatus(int categoryId) {
        try {

            // Tạo câu lệnh SQL để truy vấn trạng thái hiện tại
            String query = "SELECT Status FROM f8db.category WHERE CategoryID = ?";

            // Chuẩn bị câu lệnh SQL
            ps = connection.prepareStatement(query);
            ps.setInt(1, categoryId);

            // Thực hiện truy vấn
            rs = ps.executeQuery();

            if (rs.next()) {
                int currentStatus = rs.getInt("Status");

                // Chuyển đổi trạng thái
                int newStatus = (currentStatus == 1) ? 0 : 1;

                // Tạo câu lệnh SQL để cập nhật trạng thái mới
                String updateSql = "UPDATE f8db.category SET Status = ? WHERE CategoryID = ?";

                // Chuẩn bị câu lệnh SQL
                ps = connection.prepareStatement(updateSql);
                ps.setInt(1, newStatus);
                ps.setInt(2, categoryId);

                // Thực thi câu lệnh SQL
                ps.executeUpdate();

                System.out.println("Trạng thái đã được chuyển đổi thành công.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // Xử lý ngoại lệ hoặc thông báo lỗi
        } finally {
            // Đóng kết nối và tài nguyên
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public List<Course> getCourseBySearch(String search) throws Exception {
        List<Course> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM f8db.course c where c.Name like ? and c.Status = 1";
            ps = connection.prepareStatement(sql);
            ps.setString(1, "%" + search + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Course(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getInt(6),
                        rs.getInt(7),
                        rs.getInt(8),
                        rs.getInt(9)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return list;
    }

    public List<Course> getFreeCourse() throws Exception {
        List<Course> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM course where price = 0 and status = 1";
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Course c = new Course(rs.getInt(1), rs.getString(3), rs.getString(2), rs.getString(4), 0);
                list.add(c);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return list;
    }

    public List<Course> getProCourse() throws Exception {
        List<Course> list = new ArrayList<>();
        try {
            String sql = "select c.CourseID, c.Name, c.Image, c.CourseInfo, c.price from course c where price > 0 and status = 1";
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Course c = new Course(rs.getInt(1), rs.getString(3), rs.getString(2), rs.getString(4), rs.getInt(5));
                list.add(c);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return list;
    }

    //limit 5 course on homepage
    public List<Course> getProCourse5() throws Exception {
        List<Course> list = new ArrayList<>();
        try {
            String sql = "select c.CourseID, c.Name, c.Image, c.CourseInfo, c.price from course c where price > 0 and status = 1 limit 5";
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Course c = new Course(rs.getInt(1), rs.getString(3), rs.getString(2), rs.getString(4), rs.getInt(5));
                list.add(c);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return list;
    }

    public List<Course> getFreeCourse5() throws Exception {
        List<Course> list = new ArrayList<>();
        try {
            String sql = "select c.CourseID, c.Name, c.Image, c.CourseInfo, c.price from course c where price = 0 and status = 1 limit 5";
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Course c = new Course(rs.getInt(1), rs.getString(3), rs.getString(2), rs.getString(4), 0);
                list.add(c);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return list;
    }

    public int getLatestID() {
        int id;
        try {
            String sql = "select max(CourseID) from Course";
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                id = rs.getInt(1);
                return id;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
}
