/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dto;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author pmt
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class Invoice {

    private int Invoice_ID;
    private int User_UserID;
    private int price;
    private Date CreateDate;
    private int Status;
    private String Description;
    private int Course_CourseID;

}
