/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dto;

/**
 *
 * @author Hanh
 */
import java.time.Duration;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NewFeed {

    private int FeedID;
    private String FeedTitle;
    private String Description;
    private String Image;
    private String FeedVideo;
    private String Author;
    private LocalDateTime timeCreated;

    public long getDay() {
        LocalDateTime now = LocalDateTime.now();
        Duration duration = Duration.between(timeCreated, now);
        return duration.toDays();
    }
}
