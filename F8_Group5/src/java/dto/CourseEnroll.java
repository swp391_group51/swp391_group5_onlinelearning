/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dto;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author pmt
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CourseEnroll {
    private int UserID;
    private int CourseID;
    private int status;
    private Date date;
}
